# ecoPlatform

ecoPlatform is a part of the [re-empowered tool suite](https://reempowered-h2020.com/).
The official description reads:

> ecoPlatform will be a lightweight, cloud-based platform with the primary
  objective of providing the RE-EMPOWERED tools with a secure and reliable
  interface to the deployed distributed energy infrastructure. In addition,
  ecoPlatform will be capable of managing, processing and handling the
  heterogeneous data and command stream from the RE-EMPOWERED tools, metering
  infrastructure, SCADA systems, MGCC and selected controllable assets.
  ecoPlatform will provide a Platform as a Service (PaaS) that can integrate all
  the solutions in one software structure.

The platform consists of three core services:
- Message broker ([RabbitMQ](https://www.rabbitmq.com/))
- Database ([Timescale](https://www.timescale.com/))
- Engine (web application and data processor - source code in the [src](./src/)
  directory)

This repository contains documentation, scripts for the installing and running
the services, and source code for the application.

Documentation for the project is stored in the [docs](./docs/) subdirectory.
Start with the [Concepts](./docs/concepts.md) document.

## Development setup

The platform services read configuration from a file named .env in the src
directory. This file is not checked in to the repository, so you must first
create it. You can do this by copying the file file .env.example:
`cp src/.env.example src/.env`. The configuration does not need to be modified
for development setup.

Running the application requires a running instance of Timescale and RabbitMQ.
The repository contains a [docker-compose.yml](./docker-compose.yml) file for
running these services. To run them, make sure you have
[Docker](https://www.docker.com/) installed, and then run `docker compose up`
from the project root directory.

When the services are running, RabbitMQ must be bootstrapped. This is done with
the script rabbitmq_bootstrap.sh, which lives in the scripts directory. Run it
with bash: `bash ./scripts/rabbitmq_bootstrap.sh`.

As the application is written in Elixir, it requires you to have Erlang and Elixir
installed to run it on your host. The [.tool-versions](./.tool-versions) file
lists the versions of Erlang and Elixir that the application is tested with. If you
have [asdf](https://asdf-vm.com/) installed, you can install these versions
simply by running `asdf install` from the project root directory.

Then:

- Move into the src directory by running `cd src/`
- Run `mix setup` to install and setup dependencies and migrate and seed the database
- Start the application with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

The database is seeded with two user accounts:

- Instance administrator account:
  - Username: `admin@example.com`
  - Password: `admin`
- Standard account:
  - Username: `standard@example.com`
  - Password: `standard`

The database is also seeded with a default set of providers, datasets,
datastreams, datapoints and API credentials. See the file
[seeds.exs](./src/priv/repo/seeds.exs) for more details.

The seeded API credentials are:

- API key: `provider1`
- API secret: `secret`

These credentials are for the provider named `Provider1`, which has the MQTT
namespace `provider1`. They only work for the HTTP API by default. To make them
work for the MQTT API, run this command from the root of the repository:
`bash ./scripts/create_mqtt_credentials.sh provider1 provider1 secret`.

See the [API documentation](./docs/api.md) to learn how the API works.

## License

This project is released under the MIT License.
