#! /usr/bin/env bash
# This script is used to create accounts for instance administrators

EMAIL=$1
NAME=$2

docker compose exec -e EMAIL="$EMAIL" -e NAME="$NAME" engine /bin/bash -c '/app/bin/ecoplatform rpc "Ecoplatform.CLI.create_admin_user(\"$EMAIL\", \"$NAME\")"'
