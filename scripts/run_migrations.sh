#! /usr/bin/env bash
# This script runs migrations for the Timescale database

docker run --rm --network=ecoplatform_default --env-file=src/.env engine /app/bin/migrate
