#! /usr/bin/env bash
# This script is used to create API credentials for MQTT and HTTP API integrations

MQTT_NAMESPACE=$1
NAME=$2

CREDENTIALS_OR_ERROR=$(docker compose exec -e MQTT_NAMESPACE=$MQTT_NAMESPACE -e NAME="$NAME" engine /bin/bash -c '/app/bin/ecoplatform rpc "Ecoplatform.CLI.create_api_credentials(\"$MQTT_NAMESPACE\", \"$NAME\")"')

if [[ $(echo $CREDENTIALS_OR_ERROR | cut -c 1-7) != "SUCCESS" ]]; then
  echo "$CREDENTIALS_OR_ERROR"
  exit 1
fi

API_KEY=$(echo $CREDENTIALS_OR_ERROR | cut -d ':' -f 2)
API_SECRET=$(echo $CREDENTIALS_OR_ERROR | cut -d ':' -f 3)

bash ./scripts/create_mqtt_credentials.sh "$MQTT_NAMESPACE" "$API_KEY" "$API_SECRET"

echo CREDENTIALS:
echo API KEY: $API_KEY
echo API SECRET: $API_SECRET
