#! /usr/bin/env bash
# This script bootstraps the Timescale database

docker compose exec timescale /bin/bash -c 'psql -U postgres -c "create database $EP_DB_DATABASE;"'
docker compose exec timescale /bin/bash -c 'psql -U postgres -c "create user $EP_DB_USERNAME with encrypted password '\''$EP_DB_PASSWORD'\'';"'
docker compose exec timescale /bin/bash -c 'psql -U postgres -c "grant all privileges on database $EP_DB_DATABASE to $EP_DB_USERNAME;"'
docker compose exec timescale /bin/bash -c 'psql -U postgres $EP_DB_DATABASE -c "grant all on schema public TO $EP_DB_USERNAME;"'

bash ./scripts/run_migrations.sh
