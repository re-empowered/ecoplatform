#! /usr/bin/env bash
# This script bootstraps the RabbitMQ broker with necessary configuration to run the pipeline

# Create admin user and assign permissions
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl add_user $EP_RABBITMQ_ADMIN_USERNAME $EP_RABBITMQ_ADMIN_PASSWORD'
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl set_permissions -p "/" $EP_RABBITMQ_ADMIN_USERNAME ".*" ".*" ".*"'
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl set_user_tags $EP_RABBITMQ_ADMIN_USERNAME administrator'
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl delete_user guest'

# Create vhost
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl add_vhost $EP_RABBITMQ_VHOST'
# Create app user and assign permissions
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl add_user $EP_RABBITMQ_USERNAME $EP_RABBITMQ_PASSWORD'
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl set_permissions -p $EP_RABBITMQ_VHOST $EP_RABBITMQ_USERNAME ".*" ".*" ".*"'
docker compose exec rabbitmq /bin/bash -c 'rabbitmqctl set_permissions -p $EP_RABBITMQ_VHOST $EP_RABBITMQ_ADMIN_USERNAME ".*" ".*" ".*"'

# Enable MQTT plugin
docker compose exec rabbitmq /bin/bash -c 'rabbitmq-plugins enable rabbitmq_mqtt'
