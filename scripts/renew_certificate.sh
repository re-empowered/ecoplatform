#! /usr/bin/env bash
# This script is used renew TLS certificates
# It should be run daily by cron

docker compose up cert-renew
docker compose kill -s SIGHUP nginx
