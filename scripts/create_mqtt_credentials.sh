#! /usr/bin/env bash
# This script is used to create API credentials for the MQTT API

MQTT_NAMESPACE=$1
API_KEY=$2
API_SECRET=$3

docker compose exec -e API_KEY="$API_KEY" -e API_SECRET="$API_SECRET" rabbitmq /bin/bash -c 'rabbitmqctl add_user $API_KEY $API_SECRET'
docker compose exec -e API_KEY="$API_KEY" -e API_SECRET="$API_SECRET" rabbitmq /bin/bash -c 'rabbitmqctl set_permissions -p ep $API_KEY "mqtt-subscription-.*" "mqtt-subscription-.*|$EP_RABBITMQ_MQTT_EXCHANGE" "mqtt-subscription-.*|$EP_RABBITMQ_MQTT_EXCHANGE"'
docker compose exec -e API_KEY="$API_KEY" -e API_SECRET="$API_SECRET" -e MQTT_NAMESPACE="$MQTT_NAMESPACE" rabbitmq /bin/bash -c 'rabbitmqctl set_topic_permissions -p ep $API_KEY "$EP_RABBITMQ_MQTT_EXCHANGE" "^$MQTT_NAMESPACE\..+" ".*"'
