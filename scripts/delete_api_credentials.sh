#! /usr/bin/env bash
# This script is used to delete API credentials for MQTT and HTTP API integrations

API_KEY=$1

RESULT=$(docker compose exec -e API_KEY=$API_KEY engine /bin/bash -c '/app/bin/ecoplatform rpc "Ecoplatform.CLI.delete_api_credentials(\"$API_KEY\")"')

if [[ $(echo $RESULT | cut -c 1-7) != "SUCCESS" ]]; then
  echo "$RESULT"
  exit 1
fi

docker compose exec -e API_KEY="$API_KEY" rabbitmq /bin/bash -c 'rabbitmqctl delete_user "$API_KEY"'
