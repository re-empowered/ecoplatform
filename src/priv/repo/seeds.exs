# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ecoplatform.Repo.insert!(%Ecoplatform.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Ecoplatform.Repo.insert!(%Ecoplatform.Accounts.User{
  email: "admin@example.com",
  name: "Admin",
  admin: true,
  hashed_password: Bcrypt.hash_pwd_salt("admin")
})

Ecoplatform.Repo.insert!(%Ecoplatform.Accounts.User{
  email: "standard@example.com",
  name: "Standard",
  admin: false,
  hashed_password: Bcrypt.hash_pwd_salt("standard")
})

alias Ecoplatform.Repo
alias Ecoplatform.Datasets.Provider
alias Ecoplatform.Datasets.Dataset
alias Ecoplatform.Datasets.Datastream
alias Ecoplatform.Datasets.Datapoint
alias Ecoplatform.Datasets.DatastreamValueAlarm

now =
  NaiveDateTime.utc_now()
  |> NaiveDateTime.truncate(:second)

insert_and_return = fn records, schema ->
  Repo.insert_all(schema, records, placeholders: %{now: now})
  Repo.all(schema)
end

providers =
  for index <- 1..10 do
    %{
      name: "Provider#{index}",
      mqtt_namespace: "provider#{index}",
      inserted_at: {:placeholder, :now},
      updated_at: {:placeholder, :now}
    }
  end
  |> insert_and_return.(Provider)

datasets =
  for provider <- providers,
      index <- 1..10 do
    %{
      provider_id: provider.id,
      name: "Dataset#{provider.id}.#{index}",
      description:
        Stream.repeatedly(fn -> "Description for dataset #{index}" end)
        |> Enum.take(10)
        |> Enum.join(" "),
      mqtt_subtopic: "dataset#{index}",
      inserted_at: {:placeholder, :now},
      updated_at: {:placeholder, :now}
    }
  end
  |> insert_and_return.(Dataset)

[datastream | _] =
  for dataset <- datasets,
      index <- 1..10 do
    %{
      name: "datastream-#{index}",
      dataset_id: dataset.id,
      inserted_at: {:placeholder, :now},
      updated_at: {:placeholder, :now}
    }
  end
  |> insert_and_return.(Datastream)

_datapoints =
  for index <- 1..80000 do
    %{
      ts: DateTime.utc_now() |> DateTime.add(index * -10),
      datastream_id: datastream.id,
      value: index * :rand.uniform()
    }
  end
  |> Enum.chunk_every(trunc(65535 / 3))
  |> Enum.each(fn datapoints ->
    Repo.insert_all(Datapoint, datapoints, placeholders: %{now: now})
  end)

_alarms =
  Stream.unfold(DateTime.utc_now(), fn dt -> {dt, DateTime.add(dt, -17, :minute)} end)
  |> Enum.take(300)
  |> Enum.map(fn dt ->
    %{
      datastream_id: datastream.id,
      raised_at: dt,
      ts: DateTime.add(dt, -10, :second),
      value: :rand.uniform() * 200
    }
  end)
  |> insert_and_return.(DatastreamValueAlarm)

Ecoplatform.Repo.insert!(%Ecoplatform.Accounts.APICredentials{
  provider_id: 1,
  name: "Default",
  key: "provider1",
  hashed_secret: Bcrypt.hash_pwd_salt("secret")
})
