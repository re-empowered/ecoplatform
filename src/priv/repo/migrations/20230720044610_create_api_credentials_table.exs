defmodule Ecoplatform.Repo.Migrations.CreateAPICredentialsTable do
  use Ecto.Migration

  def change do
    create table(:api_credentials) do
      add :provider_id, references(:providers, on_delete: :delete_all), null: false
      add :name, :string, null: false
      add :key, :string, null: false
      add :hashed_secret, :string, null: false

      timestamps()
    end

    create unique_index(:api_credentials, [:key])
    create unique_index(:api_credentials, [:provider_id, :name])
  end
end
