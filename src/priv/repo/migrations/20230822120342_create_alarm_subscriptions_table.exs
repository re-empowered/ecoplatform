defmodule Ecoplatform.Repo.Migrations.CreateAlarmSubscriptionsTable do
  use Ecto.Migration

  def change do
    create table(:alarm_subscriptions, primary_key: false) do
      add :dataset_id, references(:datasets, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
      add :notified_at, :naive_datetime
    end

    create unique_index(:alarm_subscriptions, [:dataset_id, :user_id])
  end
end
