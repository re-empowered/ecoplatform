defmodule Ecoplatform.Repo.Migrations.CreateDatastreamValueAlarmsTables do
  use Ecto.Migration

  def change do
    create table(:datastream_value_alarms, primary_key: false) do
      add :datastream_id, :int8, null: false
      # Timescale documentation recommends using timestamptz for timestamp fields
      add :raised_at, :timestamptz, null: false
      add :ts, :timestamptz, null: false
      add :value, :float, null: false
    end

    create index(:datastream_value_alarms, [:datastream_id, :raised_at])

    execute(
      "SELECT create_hypertable('datastream_value_alarms', 'raised_at', create_default_indexes => FALSE);",
      ""
    )

    execute(
      "SELECT set_chunk_time_interval('datastream_value_alarms', INTERVAL '1 day');",
      ""
    )

    execute(
      "SELECT add_retention_policy('datastream_value_alarms', INTERVAL '7 days');",
      ""
    )

    execute(
      """
      CREATE MATERIALIZED VIEW datastream_value_alarms_hourly
      WITH (timescaledb.continuous)
      AS
        SELECT
          datastream_id,
          time_bucket('1 hour'::interval, raised_at) AS raised_during,
          count(*) AS alarms,
          min(ts) as earliest,
          max(ts) as latest,
          min(value) as value_min,
          max(value) as value_max,
          percentile_cont(0.05) within group (order by value asc) as value_p05,
          percentile_cont(0.1) within group (order by value asc) as value_p10,
          percentile_cont(0.2) within group (order by value asc) as value_p20,
          percentile_cont(0.5) within group (order by value asc) as value_p50,
          percentile_cont(0.8) within group (order by value asc) as value_p80,
          percentile_cont(0.9) within group (order by value asc) as value_p90,
          percentile_cont(0.95) within group (order by value asc) as value_p95
        FROM datastream_value_alarms
        GROUP BY datastream_id, raised_during
      WITH NO DATA;
      """,
      "DROP MATERIALIZED VIEW datastream_value_alarms_hourly CASCADE;"
    )

    execute(
      "SELECT add_continuous_aggregate_policy('datastream_value_alarms_hourly', '1 day', '1 hour', '1 hour');",
      ""
    )

    execute(
      "SELECT add_retention_policy('datastream_value_alarms_hourly', INTERVAL '90 days');",
      ""
    )
  end
end
