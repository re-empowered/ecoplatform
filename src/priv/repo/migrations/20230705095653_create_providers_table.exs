defmodule Ecoplatform.Repo.Migrations.CreateProvidersTable do
  use Ecto.Migration

  def change do
    create table(:providers) do
      add :name, :string, null: false
      add :mqtt_namespace, :string, null: false

      timestamps()
    end

    create unique_index(:providers, [:name])
    create unique_index(:providers, [:mqtt_namespace])
  end
end
