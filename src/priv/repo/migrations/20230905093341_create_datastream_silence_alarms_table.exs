defmodule Ecoplatform.Repo.Migrations.AddDatastreamSilenceAlarms do
  use Ecto.Migration

  def change do
    create table(:datastream_silence_alarms, primary_key: false) do
      add :datastream_id, references(:datastreams, on_delete: :delete_all), null: false
      # Timescale documentation recommends using timestamptz for timestamp fields
      add :raised_at, :timestamptz, null: false
    end

    create index(:datastream_silence_alarms, [:datastream_id, :raised_at])

    execute(
      "SELECT create_hypertable('datastream_silence_alarms', 'raised_at', create_default_indexes => FALSE);",
      ""
    )

    execute(
      "SELECT set_chunk_time_interval('datastream_silence_alarms', INTERVAL '1 day');",
      ""
    )

    execute(
      "SELECT add_retention_policy('datastream_silence_alarms', INTERVAL '7 days');",
      ""
    )
  end
end
