defmodule Ecoplatform.Repo.Migrations.CreateDatasetsTable do
  use Ecto.Migration

  def change do
    create table(:datasets) do
      add :provider_id, references(:providers, on_delete: :delete_all), null: false
      add :name, :string
      add :description, :text
      add :mqtt_subtopic, :string

      timestamps()
    end
  end
end
