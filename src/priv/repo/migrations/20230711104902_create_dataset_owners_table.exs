defmodule Ecoplatform.Repo.Migrations.CreateDatasetOwnersTable do
  use Ecto.Migration

  def change do
    create table(:dataset_owners, primary_key: false) do
      add :dataset_id, references(:datasets, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
    end

    create unique_index(:dataset_owners, [:dataset_id, :user_id])
  end
end
