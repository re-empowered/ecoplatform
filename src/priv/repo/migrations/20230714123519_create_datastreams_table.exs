defmodule Ecoplatform.Repo.Migrations.CreateDatastreamsTable do
  use Ecto.Migration

  def change do
    create table(:datastreams) do
      add :dataset_id, references(:datasets, on_delete: :delete_all), null: false
      add :name, :string, null: false
      add :description, :text
      add :min_value, :float
      add :max_value, :float
      add :max_silence_seconds, :integer
      add :latest_datapoint_ts, :timestamptz
      add :physical_unit, :string
      add :sensor_type, :string
      add :address, :string
      add :geo_tag, :string
      add :project_tag, :string
      add :sensor_owner, :string
      add :data_owner, :string
      add :acquisition_responsible, :string
      add :confidentiality_tag, :string
      add :gdpr_classification, :string
      add :other_comments, :string

      timestamps()
    end

    create unique_index(:datastreams, [:dataset_id, :name])
  end
end
