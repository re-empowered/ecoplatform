defmodule Ecoplatform.Repo.Migrations.CreateDatapointsTable do
  use Ecto.Migration

  def change do
    create table(:datapoints, primary_key: false) do
      add :datastream_id, references(:datastreams, on_delete: :delete_all),
        null: false,
        primary_key: true

      # Timescale documentation recommends using timestamptz for timestamp fields
      add :ts, :timestamptz, null: false, primary_key: true
      add :value, :float
    end

    execute("SELECT create_hypertable('datapoints', 'ts', create_default_indexes => FALSE);", "")

    execute(
      """
      ALTER TABLE datapoints SET (
        timescaledb.compress,
        timescaledb.compress_segmentby = 'datastream_id',
        timescaledb.compress_orderby = 'ts ASC'
      );
      """,
      ""
    )

    execute(
      "SELECT add_compression_policy('datapoints', INTERVAL '14 days')",
      "SELECT remove_compression_policy('datapoints');"
    )
  end
end
