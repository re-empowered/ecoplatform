defmodule Ecoplatform.Config do
  @moduledoc """
  Reads configuration from .env file and environment variables.
  """

  alias Vapor.Provider.Group
  alias Vapor.Provider.Dotenv
  alias Vapor.Provider.Env

  def load!() do
    providers = [
      %Group{
        name: :mailer,
        providers: [
          %Dotenv{},
          %Env{
            bindings: [
              {:host, "EP_SMTP_HOST"},
              {:port, "EP_SMTP_PORT"},
              {:username, "EP_SMTP_USERNAME", default: ""},
              {:password, "EP_SMTP_PASSWORD", default: ""},
              {:mail_from_name, "EP_SMTP_MAIL_FROM_NAME", default: "ecoPlatform"},
              {:mail_from_email, "EP_SMTP_MAIL_FROM_EMAIL"}
            ]
          }
        ]
      },
      %Group{
        name: :rabbitmq,
        providers: [
          %Dotenv{},
          %Env{
            bindings: [
              {:host, "EP_RABBITMQ_HOST"},
              {:vhost, "EP_RABBITMQ_VHOST"},
              {:username, "EP_RABBITMQ_USERNAME"},
              {:password, "EP_RABBITMQ_PASSWORD"}
            ]
          }
        ]
      },
      %Group{
        name: :pipeline,
        providers: [
          %Dotenv{},
          %Env{
            bindings: [
              {:sink_queue, "EP_PIPELINE_SINK_QUEUE", default: "ep.pipeline.sink"},
              {:sink_dlx, "EP_PIPELINE_SINK_DLX", default: "ep.pipeline-dlx"},
              {:sink_queue_sources, "EP_PIPELINE_SINK_QUEUE_SOURCES",
               default: [{"ep.pipeline", "validated"}], map: &parse_queue_sources/1},
              {:validate_queue, "EP_PIPELINE_VALIDATE_QUEUE", default: "ep.pipeline.validate"},
              {:validate_dlx, "EP_PIPELINE_VALIDATE_DLX", default: "ep.pipeline-dlx"},
              {:validate_queue_sources, "EP_PIPELINE_VALIDATE_QUEUE_SOURCES",
               default: [{"ep.pipeline", "unpacked"}], map: &parse_queue_sources/1},
              {:validate_publish_target, "EP_PIPELINE_VALIDATE_PUBLISH_TARGET",
               default: {"ep.pipeline", "validated"}, map: &parse_publish_target/1},
              {:unpack_queue, "EP_PIPELINE_UNPACK_QUEUE", default: "ep.pipeline.unpack"},
              {:unpack_dlx, "EP_PIPELINE_UNPACK_DLX", default: "ep.mqtt-dlx"},
              {:unpack_queue_sources, "EP_PIPELINE_UNPACK_QUEUE_SOURCES",
               default: [{"ep.mqtt", "#"}], map: &parse_queue_sources/1},
              {:unpack_publish_target, "EP_PIPELINE_UNPACK_PUBLISH_TARGET",
               default: {"ep.pipeline", "unpacked"}, map: &parse_publish_target/1}
            ]
          }
        ]
      }
    ]

    Vapor.load!(providers)
  end

  defp parse_queue_sources(queue_sources) do
    String.split(queue_sources, ",")
    |> Enum.map(fn source_pair ->
      [exchange, routing_key] = String.split(source_pair, ":")
      {exchange, routing_key}
    end)
  end

  defp parse_publish_target(publish_target) do
    [exchange, routing_key] = String.split(publish_target, ":")
    {exchange, routing_key}
  end
end
