defmodule Ecoplatform.Datasets do
  @moduledoc """
  The Datasets context.
  """

  import Ecto.Query, warn: false
  alias Ecoplatform.Repo

  alias Ecoplatform.Accounts.User
  alias Ecoplatform.Datasets.AlarmSubscription
  alias Ecoplatform.Datasets.Dataset
  alias Ecoplatform.Datasets.Datastream
  alias Ecoplatform.Datasets.DatastreamValueAlarm
  alias Ecoplatform.Datasets.DatastreamValueAlarmHourly
  alias Ecoplatform.Datasets.DatastreamSilenceAlarm
  alias Ecoplatform.Datasets.Datapoint
  alias Ecoplatform.Datasets.Owner
  alias Ecoplatform.Datasets.Provider

  @doc """
  Returns the list of providers.

  ## Examples

      iex> list_providers()
      [%Provider{}, ...]

  """
  def list_providers do
    Repo.all(Provider)
  end

  @doc """
  Gets a single provider.

  Raises `Ecto.NoResultsError` if the Provider does not exist.

  ## Examples

      iex> get_provider!(123)
      %Provider{}

      iex> get_provider!(456)
      ** (Ecto.NoResultsError)

  """
  def get_provider!(id), do: Repo.get!(Provider, id)

  @doc "Returns the provider for the given MQTT namespace"
  def get_provider_by_mqtt_namespace(mqtt_namespace) do
    Repo.get_by(Provider, mqtt_namespace: mqtt_namespace)
  end

  @doc """
  Creates a provider.

  ## Examples

      iex> create_provider("a-name", "an-mqtt-namespace")
      {:ok, %Provider{}}

      iex> create_provider("a-name", "an/invalid/mqtt/namespace")
      {:error, %Ecto.Changeset{}}

  """
  def create_provider(name, mqtt_namespace) do
    %Provider{}
    |> Provider.changeset(name, mqtt_namespace)
    |> Repo.insert()
  end

  @doc """
  Updates a provider.

  ## Examples

      iex> update_provider(provider, "b-name", "a-new-mqtt-namespace")
      {:ok, %Provider{}}

      iex> update_provider(provider, "b-name", "a/new/invalid/mqtt/namespace")
      {:error, %Ecto.Changeset{}}

  """
  def update_provider(%Provider{} = provider, name, mqtt_namespace) do
    provider
    |> Provider.changeset(name, mqtt_namespace)
    |> Repo.update()
  end

  @doc """
  Deletes a provider.

  ## Examples

      iex> delete_provider(provider)
      {:ok, %Provider{}}

      iex> delete_provider(provider)
      {:error, %Ecto.Changeset{}}

  """
  def delete_provider(%Provider{} = provider) do
    Repo.delete(provider)
  end

  @doc """
  Returns the list of datasets.

  The datasests have the provider preloaded.

  ## Examples

      iex> list_datasets()
      [%Dataset{}, ...]

  """
  def list_datasets() do
    Repo.all(
      from d in Dataset,
        join: p in assoc(d, :provider),
        as: :provider,
        preload: [provider: p]
    )
  end

  @doc """
  Returns a list of datasets after applying filtering and sorting with Flop.

  The datasests have the provider preloaded.
  """
  def flop_datasets(params) do
    query =
      from d in Dataset,
        join: p in assoc(d, :provider),
        as: :provider,
        preload: [provider: p]

    Flop.validate_and_run(query, params, for: Dataset)
  end

  @doc """
  Gets a single dataset with the provider preloaded.

  Raises `Ecto.NoResultsError` if the Dataset does not exist.

  ## Examples

      iex> get_dataset!(123)
      %Dataset{}

      iex> get_dataset!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dataset!(id) do
    Repo.one!(
      from d in Dataset,
        where: d.id == ^id,
        join: p in assoc(d, :provider),
        preload: [provider: p]
    )
  end

  @doc """
  Creates a dataset.

  ## Examples

      iex> create_dataset(42, "a name", "a description", "a/subtopic")
      {:ok, %Dataset{}}

      iex> create_dataset(-1, "a name", "", "$invalid.topic")
      {:error, %Ecto.Changeset{}}

  """
  def create_dataset(provider_id, name, description, mqtt_subtopic) do
    %Dataset{}
    |> Dataset.changeset(provider_id, name, description, mqtt_subtopic)
    |> Repo.insert()
  end

  @doc """
  Updates a dataset.

  ## Examples

      iex> update_dataset(dataset, 42, "a name", "a description", "a/subtopic")
      {:ok, %Dataset{}}

      iex> update_dataset(dataset, -1, "a name", "", "$invalid.topic")
      {:error, %Ecto.Changeset{}}

  """
  def update_dataset(
        %Dataset{} = dataset,
        provider_id,
        name,
        description,
        mqtt_subtopic
      ) do
    dataset
    |> Dataset.changeset(provider_id, name, description, mqtt_subtopic)
    |> Repo.update()
  end

  @doc """
  Deletes a dataset.

  ## Examples

      iex> delete_dataset(dataset)
      {:ok, %Dataset{}}

      iex> delete_dataset(dataset)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dataset(%Dataset{} = dataset) do
    Repo.delete(dataset)
  end

  @doc """
  Returns the list of datasets owned by the user.

  ## Examples

      iex> list_own_datasets(user_id)
      [%Dataset{}, ...]

  """
  def list_own_datasets(user_id) do
    Repo.all(
      from d in Dataset,
        join: o in assoc(d, :owners),
        where: o.id == ^user_id
    )
  end

  @doc """
  Returns a list of datasets after applying filtering and sorting with Flop.

  The datasests have the provider preloaded.
  """
  def flop_own_datasets(user_id, params) do
    query =
      from d in Dataset,
        join: p in assoc(d, :provider),
        as: :provider,
        preload: [provider: p],
        join: o in assoc(d, :owners),
        where: o.id == ^user_id

    Flop.validate_and_run(query, params, for: Dataset)
  end

  def add_dataset_owner(%Dataset{} = dataset, user_id) do
    Owner.changeset(dataset.id, user_id)
    |> Repo.insert()
  end

  def remove_dataset_owner(%Dataset{} = dataset, user_id) do
    query =
      from owners in "dataset_owners",
        where: owners.dataset_id == ^dataset.id and owners.user_id == ^user_id

    case Repo.delete_all(query) do
      {number, nil} when number in [0, 1] -> :ok
      error -> error
    end
  end

  def list_dataset_owners(%Dataset{} = dataset) do
    Repo.all(
      from owners in "dataset_owners",
        where: owners.dataset_id == ^dataset.id,
        join: u in "users",
        on: u.id == owners.user_id,
        select: %{id: u.id, name: u.name}
    )
  end

  def user_can_access?(%Dataset{} = dataset, %User{} = user) do
    user.admin or owner?(dataset, user)
  end

  defp owner?(dataset, user) do
    Repo.exists?(
      from owners in "dataset_owners",
        where: owners.dataset_id == ^dataset.id,
        where: owners.user_id == ^user.id
    )
  end

  ## Datastreams

  @doc """
  Returns the list of datastreams.

  ## Examples

      iex> list_datastreams()
      [%Datastream{}, ...]

  """
  def list_datastreams() do
    Repo.all(Datastream)
  end

  @doc """
  Returns a list of datastreams for a dataset after applying filtering and sorting with Flop.
  """
  def flop_datastreams(dataset_id, params) do
    query =
      from(
        d in Datastream,
        where: d.dataset_id == ^dataset_id
      )

    Flop.validate_and_run(query, params, for: Datastream)
  end

  @doc """
  Gets a single datastream.

  Raises `Ecto.NoResultsError` if the Datastream does not exist.

  ## Examples

      iex> get_datastream!(123)
      %Datastream{}

      iex> get_datastream!(456)
      ** (Ecto.NoResultsError)

  """
  def get_datastream!(id) do
    Repo.get!(Datastream, id)
  end

  @doc """
  Creates a datastream.

  ## Examples

      iex> create_datastream(42, "a-name", %{description: "a description"})
      {:ok, %Datastream{}}

      iex> create_datastream(-1, "invalid name", %{})
      {:error, %Ecto.Changeset{}}

  """
  def create_datastream(dataset_id, name, params) do
    %Datastream{}
    |> Datastream.changeset(dataset_id, name, params)
    |> Repo.insert()
  end

  @doc """
  Updates a datastream.

  ## Examples

      iex> update_datastream(datastream, 42, "a-name", %{description: "a description"})
      {:ok, %Datastream{}}

      iex> update_datastream(datastream, -1, "invalid name", %{})
      {:error, %Ecto.Changeset{}}

  """
  def update_datastream(datastream, dataset_id, name, params) do
    datastream
    |> Datastream.changeset(dataset_id, name, params)
    |> Repo.update()
  end

  @doc """
  Deletes a datastream.

  ## Examples

      iex> delete_datastream(datastream)
      {:ok, %Datastream{}}

      iex> delete_datastream(datastream)
      {:error, %Ecto.Changeset{}}

  """
  def delete_datastream(%Datastream{} = datastream) do
    Repo.delete(datastream)
  end

  @doc """
  Inserts the given datapoints in a single batch.

  The `datapoints` paramter must be a list of maps, where every
  map has following the keys:
  - `datastream_id`: the ID of a Datastream
  - `ts`: a UTC timestamp with microsecond precision, e.g. `~U[2020-01-01 13:00:00.000000Z]`
  - `value`: a floating point number, e.g. `12.34`

  Note: the list of datapoints must have a max length of 21845 (as the
  postgresql protocol can not handle more than 65535 parameters).

  ## Examples:

      iex> Datasets.insert_datapoints([
      ...>   %{datastream_id: 1, ts: ~U[2020-01-01 13:00:00.000000Z], value: 12.34},
      ...>   %{datastream_id: 2, ts: ~U[2020-01-01 14:15:16.171819Z], value: 56.78}
      ...> ])
      :ok
  """
  def insert_datapoints(datapoints) do
    case Repo.insert_all(Datapoint, datapoints,
           on_conflict: :replace_all,
           conflict_target: [:datastream_id, :ts]
         ) do
      {number, nil} when is_integer(number) ->
        update_latest_datapoint_ts(datapoints)
        :ok

      error ->
        error
    end
  end

  defp update_latest_datapoint_ts(datapoints) do
    datapoints
    |> Enum.reduce(%{}, &latest_timestamps/2)
    |> Enum.group_by(fn {_, ts} -> ts end, fn {datastream_id, _} -> datastream_id end)
    |> Enum.each(fn {latest_datapoint_ts, datastream_ids} ->
      q =
        from d in Datastream,
          where: d.id in ^datastream_ids,
          where: d.latest_datapoint_ts < ^latest_datapoint_ts or is_nil(d.latest_datapoint_ts)

      Repo.update_all(q, set: [latest_datapoint_ts: latest_datapoint_ts])
    end)
  end

  defp latest_timestamps(datapoint, latest_timestamps_per_datastream) do
    Map.update(
      latest_timestamps_per_datastream,
      datapoint.datastream_id,
      datapoint.ts,
      fn current_ts -> max_ts(datapoint.ts, current_ts) end
    )
  end

  defp max_ts(new, current) do
    case DateTime.compare(new, current) do
      :gt -> new
      _ -> current
    end
  end

  @doc """
  Returns a stream of datapoints for the given datastream.

  The stream will return all datapoints for the given datastream whose timestamp
  `ts` is within the bounds given by `start_ts` and `end_ts`, both inclusive.
  The datapoints will be sorted by `ts` ascending.

  Any calls that enumerate the stream must be wrapped in a transaction.

  ## Examples:

      iex> stream = Datastreams.stream_datapoints(1, ~U[2020-01-01 13:00:00.000000Z], ~U[2020-01-01 16:00:00.000000Z])
      iex> Repo.transaction(fn -> Enum.to_list(stream) end)
      {:ok, [%Datapoint{}, ...]}
  """
  def stream_datapoints(datastream_id, start_ts, end_ts, ordering \\ :asc) do
    Repo.stream(
      from d in Datapoint,
        where: d.datastream_id == ^datastream_id,
        where: d.ts >= ^start_ts,
        where: d.ts <= ^end_ts,
        order_by: [{^ordering, d.ts}]
    )
  end

  ## Alarms

  def flop_dataset_value_alarm_hourly(%Dataset{} = dataset, params) do
    query =
      from dvah in DatastreamValueAlarmHourly,
        join: d in assoc(dvah, :datastream),
        as: :datastream,
        where: d.dataset_id == ^dataset.id,
        preload: [datastream: d]

    Flop.validate_and_run(query, params, for: DatastreamValueAlarmHourly)
  end

  def get_value_alarm_summary(datastream_id, raised_during) do
    hourly = get_value_alarm_hourly(datastream_id, raised_during)
    alarms = get_value_alarms(datastream_id, raised_during)

    {hourly, alarms}
  end

  def get_value_alarm_hourly(datastream_id, raised_during) do
    Repo.one(
      from dvah in DatastreamValueAlarmHourly,
        where: dvah.datastream_id == ^datastream_id,
        where: dvah.raised_during == ^raised_during,
        order_by: [{:desc, dvah.raised_during}]
    )
  end

  def get_value_alarms(datastream_id, raised_during) do
    Repo.all(
      from dva in DatastreamValueAlarm,
        where: dva.datastream_id == ^datastream_id,
        where:
          fragment("date_trunc('hour', ?::timestamp)", dva.raised_at) ==
            fragment("date_trunc('hour', ?::timestamp)", ^raised_during),
        order_by: [{:desc, dva.raised_at}]
    )
  end

  def insert_value_alarms(alarms) do
    case Repo.insert_all(DatastreamValueAlarm, alarms) do
      {number, nil} when is_integer(number) -> :ok
      error -> error
    end
  end

  def has_alarm_subscription?(dataset, user) do
    Repo.exists?(
      from a in AlarmSubscription,
        where: a.dataset_id == ^dataset.id,
        where: a.user_id == ^user.id
    )
  end

  def create_alarm_subscription(%Dataset{} = dataset, %User{} = user) do
    AlarmSubscription.changeset(dataset.id, user.id)
    |> Repo.insert()
  end

  def delete_alarm_subscription(%Dataset{} = dataset, %User{} = user) do
    query =
      from a in AlarmSubscription,
        where: a.dataset_id == ^dataset.id,
        where: a.user_id == ^user.id

    case Repo.delete_all(query) do
      {number, nil} when number in [0, 1] -> :ok
      error -> error
    end
  end

  def mark_alarm_subscriptions_notified(dataset_ids, user_id) do
    query =
      from a in AlarmSubscription,
        where: a.user_id == ^user_id,
        where: a.dataset_id in ^dataset_ids

    Repo.update_all(query, set: [notified_at: DateTime.utc_now()])
  end

  def alarm_subscriptions_dormant_since(watermark) do
    Repo.all(
      from a in AlarmSubscription,
        where: a.notified_at < ^watermark or is_nil(a.notified_at),
        join: u in assoc(a, :user),
        select: %{email: u.email, name: u.name, dataset_id: a.dataset_id, user_id: a.user_id}
    )
  end

  def value_alarms_since(watermark, dataset_ids) do
    Repo.all(
      from dva in DatastreamValueAlarm,
        join: datastream in assoc(dva, :datastream),
        join: dataset in assoc(datastream, :dataset),
        where: dva.raised_at > ^watermark,
        where: datastream.dataset_id in ^dataset_ids,
        select: %{
          dataset_id: dataset.id,
          dataset_name: dataset.name,
          datastream_name: datastream.name,
          alarm: dva
        }
    )
  end

  def silence_alarms_since(watermark, dataset_ids) do
    Repo.all(
      from dsa in DatastreamSilenceAlarm,
        join: datastream in assoc(dsa, :datastream),
        join: dataset in assoc(datastream, :dataset),
        where: dsa.raised_at > ^watermark,
        where: datastream.dataset_id in ^dataset_ids,
        select: %{
          dataset_id: dataset.id,
          dataset_name: dataset.name,
          datastream_name: datastream.name,
          alarm: dsa
        }
    )
  end
end
