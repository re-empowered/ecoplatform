defmodule Ecoplatform.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @env Mix.env()

  @impl true
  def start(_type, _args) do
    config = Ecoplatform.Config.load!()
    :ok = Ecoplatform.Mailer.put_config(config)

    children =
      [
        # Start the Telemetry supervisor
        EcoplatformWeb.Telemetry,
        # Start the Ecto repository
        Ecoplatform.Repo,
        # Start the PubSub system
        {Phoenix.PubSub, name: Ecoplatform.PubSub},
        # Start Finch
        {Finch, name: Ecoplatform.Finch},
        # Start the Endpoint (http/https)
        EcoplatformWeb.Endpoint,
        Ecoplatform.Datasets.AlarmNotifier,
        Ecoplatform.Datasets.SilenceChecker
      ] ++ pipeline(@env, config)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Ecoplatform.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def pipeline(:test, _config) do
    []
  end

  def pipeline(_, config) do
    :ok = Ecoplatform.Pipeline.setup(config)

    sink_opts =
      Keyword.merge(
        [producer_module: BroadwayRabbitMQ.Producer],
        Ecoplatform.Pipeline.Sink.opts(config)
      )

    validate_opts =
      Keyword.merge(
        [producer_module: BroadwayRabbitMQ.Producer],
        Ecoplatform.Pipeline.Validate.opts(config)
      )

    unpack_opts =
      Keyword.merge(
        [producer_module: BroadwayRabbitMQ.Producer],
        Ecoplatform.Pipeline.Unpack.opts(config)
      )

    [
      Ecoplatform.Pipeline.Publisher.child_spec_for(Ecoplatform.Pipeline.Validate, config),
      Ecoplatform.Pipeline.Publisher.child_spec_for(Ecoplatform.Pipeline.Unpack, config),
      Ecoplatform.Pipeline.Cache.child_spec_for(:dataset_id),
      Ecoplatform.Pipeline.Cache.child_spec_for(:datastream_id),
      Ecoplatform.Pipeline.Cache.child_spec_for(:datastream_range),
      {Ecoplatform.Pipeline.Sink, sink_opts},
      {Ecoplatform.Pipeline.Validate, validate_opts},
      {Ecoplatform.Pipeline.Unpack, unpack_opts}
    ]
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    EcoplatformWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
