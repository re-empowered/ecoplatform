defmodule Ecoplatform.Mailer do
  @moduledoc false
  use Swoosh.Mailer, otp_app: :ecoplatform

  def put_config(config) do
    current_adapter_config = Application.get_env(:ecoplatform, __MODULE__)

    new_adapter_config =
      case Keyword.fetch!(current_adapter_config, :adapter) do
        Swoosh.Adapters.SMTP ->
          :ok =
            Application.put_env(
              :ecoplatform,
              :mail_from,
              {config.mailer.mail_from_name, config.mailer.mail_from_email}
            )

          current_adapter_config
          |> Keyword.merge(auth(config.mailer))
          |> Keyword.merge(
            relay: config.mailer.host,
            port: config.mailer.port,
            tls_options: [
              verify: :verify_peer,
              cacertfile: CAStore.file_path(),
              server_name_indication: String.to_charlist(config.mailer.host),
              depth: 99
            ]
          )

        _ ->
          current_adapter_config
      end

    Application.put_env(:ecoplatform, __MODULE__, new_adapter_config)
  end

  defp auth(config) do
    case config do
      %{username: "", password: ""} -> []
      _ -> [username: config.username, password: config.password]
    end
  end
end
