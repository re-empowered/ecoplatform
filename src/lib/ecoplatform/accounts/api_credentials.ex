defmodule Ecoplatform.Accounts.APICredentials do
  @moduledoc """
  API credentials schema and associated changesets.
  """

  use Ecto.Schema

  import Ecto.Changeset

  schema "api_credentials" do
    belongs_to :provider, Ecoplatform.Datasets.Provider
    field :name, :string
    field :key, :string
    field :secret, :string, virtual: true, redact: true
    field :hashed_secret, :string, redact: true

    timestamps()
  end

  def changeset(provider_id, name, key_prefix \\ "") do
    key_prefix =
      case key_prefix do
        "" -> ""
        key_prefix -> "#{key_prefix}-"
      end

    key_suffix =
      max(32 - byte_size(key_prefix), 0)
      |> random_characters()

    key = "#{key_prefix}#{key_suffix}"

    secret = random_characters(32)
    hashed_secret = Bcrypt.hash_pwd_salt(secret)

    %__MODULE__{}
    |> change(%{
      provider_id: provider_id,
      name: name,
      key: key,
      secret: secret,
      hashed_secret: hashed_secret
    })
    |> validate_provider()
    |> validate_name()
    |> unique_constraint([:provider_id, :name], error_key: :name)
    |> unique_constraint(:key)
  end

  defp validate_provider(changeset) do
    changeset
    |> validate_required(:provider_id)
    |> foreign_key_constraint(:provider_id)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_required(:name)
    |> validate_length(:name, min: 2)
  end

  def valid_secret?(%__MODULE__{hashed_secret: hashed_secret}, secret)
      when is_binary(hashed_secret) and byte_size(secret) > 0 do
    Bcrypt.verify_pass(secret, hashed_secret)
  end

  def valid_secret?(_, _) do
    Bcrypt.no_user_verify()
    false
  end

  defp random_characters(0) do
    ""
  end

  @characters Enum.flat_map([?0..?9, ?A..?Z, ?a..?z], &Enum.to_list/1)
  @characters_count Enum.count(@characters)
  defp random_characters(length) do
    for _ <- 1..length, into: "" do
      index = :rand.uniform(@characters_count) - 1
      <<Enum.at(@characters, index)>>
    end
  end
end
