defmodule Ecoplatform.Accounts.UserNotifier do
  @moduledoc """
  Helpers for delivering email notifications to users.
  """

  import Swoosh.Email

  require EEx

  alias Ecoplatform.Mailer

  # Delivers the email using the application mailer.
  defp deliver(recipient, subject, body) do
    from =
      Application.get_env(
        :ecoplatform,
        :mail_from,
        {"ecoPlatform", "no-reply@ecoplatform.example.com"}
      )

    email =
      new()
      |> to(recipient)
      |> from(from)
      |> subject(subject)
      |> text_body(body)

    with {:ok, _metadata} <- Mailer.deliver(email) do
      {:ok, email}
    end
  end

  @doc """
  Deliver instructions to new user to reset password.
  """
  def deliver_new_user_instructions(user, url) do
    deliver(user.email, "New ecoPlatform user", """

    ==============================

    Hi #{user.name},

    A new account has been created for you on an ecoPlatform instance.

    You can choose a password for your account by visiting the URL below:

    #{url}

    ==============================
    """)
  end

  @doc """
  Deliver instructions to reset a user password.
  """
  def deliver_reset_password_instructions(user, url) do
    deliver(user.email, "Reset password instructions", """

    ==============================

    Hi #{user.name},

    You can reset your password by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to update a user email.
  """
  def deliver_update_email_instructions(user, url) do
    deliver(user.email, "Update email instructions", """

    ==============================

    Hi #{user.name},

    You can change your email by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  def deliver_alarm_notification(to, dataset_alarm_groups) do
    deliver(
      to.email,
      "ecoPlatform alarms",
      alarm_notification_text(to.name, dataset_alarm_groups)
    )
  end

  alias Ecoplatform.Datasets.DatastreamSilenceAlarm
  alias Ecoplatform.Datasets.DatastreamValueAlarm

  EEx.function_from_string(
    :defp,
    :alarm_notification_text,
    """
    Hi <%= name %>,

    Ecoplatform has observed the following alarms:

    <%= for dataset_alarm_group <- dataset_alarm_groups do %>
      Dataset: <%= dataset_alarm_group.dataset_name %> (<%= dataset_alarm_group.dataset_url %>)
      <%= for datastream_alarm_group <- dataset_alarm_group.datastream_alarm_groups do %>
        Datastream: <%= datastream_alarm_group.datastream_name %>
        <%= for alarm <- datastream_alarm_group.alarms do %>
          <%= format_alarm(alarm) %>
        <% end %>
      <% end %>
    <% end %>
    """,
    [:name, :dataset_alarm_groups]
  )

  defp format_alarm(alarm) do
    case alarm do
      %DatastreamValueAlarm{} ->
        "Value alarm | Raised at #{alarm.raised_at} | Timestamp: #{alarm.ts} | Value: #{alarm.value}"

      %DatastreamSilenceAlarm{} ->
        "Silence alarm | Raised at #{alarm.raised_at}"
    end
  end
end
