defmodule Ecoplatform.Pipeline.Import do
  @moduledoc """
  Import datapoints from CSV format into the pipeline.
  """

  alias NimbleCSV.RFC4180, as: CSV

  alias Ecoplatform.Repo

  @max_chunk_size 500

  def from_csv(csv_path, dataset) do
    dataset = Repo.preload(dataset, :datastreams)

    datastreams =
      Map.new(dataset.datastreams, fn datastream -> {datastream.name, datastream.id} end)

    File.stream!(csv_path, read_ahead: 100_000)
    |> CSV.parse_stream(skip_headers: false)
    |> Stream.with_index(1)
    |> Stream.map(fn {line, line_number} ->
      with [datastream_name, ts, value] <- line,
           {:ok, datastream_id} <- Map.fetch(datastreams, datastream_name),
           {ts, _} <- Integer.parse(ts),
           {value, _} <- Float.parse(value) do
        %{datastream_id: datastream_id, ts: ts, value: value}
      else
        _ ->
          {:error, {:invalid_line, {line_number, line}}}
      end
    end)
    |> chunk_until_error()
    |> Stream.map(fn
      {:error, _error} = error -> error
      datapoints -> publish(datapoints)
    end)
    |> Stream.filter(fn
      {:error, _error} -> true
      _ -> false
    end)
    |> Enum.take(1)
    |> case do
      [] -> :ok
      [error] -> error
    end
  end

  defp chunk_until_error(stream) do
    Stream.chunk_while(stream, {[], 0}, &chunk_fun/2, &after_fun/1)
  end

  defp chunk_fun(_el, {:error, _error} = acc) do
    {:halt, acc}
  end

  defp chunk_fun({:error, _} = error, {_acc_buffer, 0}) do
    {:cont, error}
  end

  defp chunk_fun({:error, _} = error, {acc_buffer, _acc_count}) do
    {:cont, Enum.reverse(acc_buffer), error}
  end

  defp chunk_fun(el, {acc_buffer, @max_chunk_size}) do
    {:cont, Enum.reverse(acc_buffer), {[el], 1}}
  end

  defp chunk_fun(el, {acc_buffer, acc_count}) do
    {:cont, {[el | acc_buffer], acc_count + 1}}
  end

  defp after_fun({:error, _error} = acc) do
    {:cont, acc, []}
  end

  defp after_fun({_acc_buffer, 0}) do
    {:cont, []}
  end

  defp after_fun({acc_buffer, _acc_count}) do
    {:cont, Enum.reverse(acc_buffer), []}
  end

  defp publish(datapoints) do
    payload = Jason.encode!(datapoints)
    publish_fn(payload)
  end

  if Mix.env() == :test do
    defp publish_fn(payload) do
      send(self(), {:publish, payload})
      :ok
    end
  else
    defp publish_fn(payload) do
      Ecoplatform.Pipeline.Publisher.publish(Ecoplatform.Pipeline.Unpack.Publisher, payload)
    end
  end
end
