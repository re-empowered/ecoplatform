defmodule Ecoplatform.Pipeline.Cache do
  @moduledoc """
  Cache used by pipeline processors to lookup data necessary for processing.

  The cache is loaded eagerly at system startup, and then reloaded every 10 minutes.
  """

  use Cachex.Warmer

  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Dataset

  @doc false
  def child_spec_for(name) do
    Supervisor.child_spec(
      {Cachex,
       name: name,
       warmers: [Cachex.Spec.warmer(module: __MODULE__, state: name)],
       expiration: Cachex.Spec.expiration(default: :timer.minutes(12))},
      id: {__MODULE__, name}
    )
  end

  @doc "Lookup the id of the dataset for the given routing key"
  def dataset_id(routing_key) do
    Cachex.get(:dataset_id, routing_key)
  end

  @doc "Lookup the id of the datastream for the given dataset id and datastream name"
  def datastream_id(dataset_id, datastream_name) do
    Cachex.get(:datastream_id, {dataset_id, datastream_name})
  end

  @doc "Lookup the min and max values for a datastream by datastream id"
  def datastream_range(datastream_id) do
    Cachex.get(:datastream_range, datastream_id)
  end

  @impl Cachex.Warmer
  def interval() do
    :timer.minutes(10)
  end

  @impl Cachex.Warmer
  def execute(:dataset_id) do
    datasets =
      Datasets.list_datasets()
      |> Enum.map(fn dataset ->
        {Dataset.routing_key(dataset), dataset.id}
      end)

    {:ok, datasets}
  end

  def execute(:datastream_id) do
    datastreams =
      Datasets.list_datastreams()
      |> Enum.map(fn datastream ->
        {{datastream.dataset_id, datastream.name}, datastream.id}
      end)

    {:ok, datastreams}
  end

  def execute(:datastream_range) do
    datastream_ranges =
      Datasets.list_datastreams()
      |> Enum.map(fn datastream ->
        {datastream.id, {datastream.min_value, datastream.max_value}}
      end)

    {:ok, datastream_ranges}
  end
end
