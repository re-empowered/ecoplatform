defmodule Ecoplatform.Pipeline.Validate do
  @moduledoc """
  Validates that received data is inside bounds dictated by datastream minimum
  and maximum values.
  """

  use Broadway

  alias Broadway.Message
  alias Ecoplatform.Datasets
  alias Ecoplatform.Pipeline
  alias Ecoplatform.Pipeline.Cache
  alias Ecoplatform.Pipeline.Schema
  alias Ecoplatform.Pipeline.Publisher

  def opts(config) do
    [
      producer_opts: [
        queue: config.pipeline.validate_queue,
        connection: Pipeline.rabbitmq_uri(config.rabbitmq),
        on_failure: :reject,
        qos: [prefetch_count: 100]
      ]
    ]
  end

  def start_link(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    producer_module = Keyword.fetch!(opts, :producer_module)
    producer_opts = Keyword.get(opts, :producer_opts, [])

    datastream_range_fn = Keyword.get(opts, :datastream_range_fn, &Cache.datastream_range/1)
    publish_fn = Keyword.get(opts, :publish_fn, &publish/1)

    Broadway.start_link(__MODULE__,
      name: name,
      context: %{
        datastream_range_fn: datastream_range_fn,
        publish_fn: publish_fn
      },
      producer: [
        module: {producer_module, producer_opts},
        concurrency: 1
      ],
      processors: [
        default: []
      ],
      batchers: [
        value_alarm_sink: [
          concurrency: System.schedulers_online() * 2,
          batch_size: 10,
          batch_timeout: 1000
        ]
      ]
    )
  end

  defp publish([]) do
    :ok
  end

  defp publish(payload) do
    payload = Jason.encode!(payload)
    Publisher.publish(__MODULE__.Publisher, payload)
  end

  @impl Broadway
  def handle_message(_processor, message, context) do
    case normalize_payload(message.data) do
      {:ok, payload} ->
        %{true: valid, false: invalid} =
          payload
          |> Enum.group_by(fn payload -> valid_value?(payload, context) end)
          |> Enum.into(%{true: [], false: []})

        context.publish_fn.(valid)

        # Any invalid values must be turned into value alarms by the batcher
        message
        |> Message.put_batcher(:value_alarm_sink)
        |> Message.put_data(invalid)

      error ->
        Message.failed(message, error)
    end
  end

  defp normalize_payload(data) do
    with {:ok, data} <- Jason.decode(data),
         :ok <- Schema.validate_unpacked(data) do
      {:ok, data}
    end
  end

  defp valid_value?(payload, context) do
    case context.datastream_range_fn.(payload["datastream_id"]) do
      {:ok, nil} ->
        # If there was no entry in the cache for the given datastream_id,
        # the value is rejected as invalid
        false

      {:ok, {nil, nil}} ->
        # If the given datastream does not have a minimum and maximum defined
        # all values are considered valid
        true

      {:ok, {min, nil}} ->
        payload["value"] >= min

      {:ok, {nil, max}} ->
        payload["value"] <= max

      {:ok, {min, max}} ->
        payload["value"] >= min and payload["value"] <= max
    end
  end

  @impl true
  def handle_batch(:value_alarm_sink, messages, _, _) do
    now = DateTime.utc_now()

    alarms =
      Enum.flat_map(messages, fn message ->
        Enum.map(message.data, fn payload ->
          %{
            datastream_id: payload["datastream_id"],
            raised_at: now,
            ts: DateTime.from_unix!(payload["ts"] * 1000, :microsecond),
            value: :erlang.float(payload["value"])
          }
        end)
      end)

    :ok = Datasets.insert_value_alarms(alarms)

    messages
  end
end
