defmodule Ecoplatform.Pipeline.Schema do
  @moduledoc false

  require Exonerate

  Exonerate.function_from_string(:def, :validate_packed, """
  {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "object",
    "properties": {
      "ts": {"type": "integer", "minimum": 0, "exclusiveMaximum": 32503680000000},
      "values": {
        "type": "object",
        "additionalProperties": {"type": "number"}
      }
    },
    "required": ["ts", "values"]
  }
  """)

  Exonerate.function_from_string(:def, :validate_unpacked, """
  {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "type": "array",
    "items": {
      "type": "object",
      "properties": {
        "datastream_id": {"type": "integer", "minimum": 1},
        "ts": {"type": "integer", "minimum": 0, "exclusiveMaximum": 32503680000000},
        "value": {"type": "number"}
      },
      "required": ["ts", "datastream_id", "value"]
    }
  }
  """)
end
