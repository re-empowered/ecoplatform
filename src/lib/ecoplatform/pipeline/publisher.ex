defmodule Ecoplatform.Pipeline.Publisher do
  @moduledoc """
  Manages a connection to RabbitMQ and exposes an API for publishing messages.
  """

  alias Ecoplatform.Pipeline

  @behaviour :gen_statem

  defstruct [:uri, :exchange, :routing_key, :connect_timeout, :conn, :channel]

  def child_spec_for(processor, config) do
    uri = Pipeline.rabbitmq_uri(config.rabbitmq)

    {exchange, routing_key} =
      case processor do
        Ecoplatform.Pipeline.Validate ->
          config.pipeline.validate_publish_target

        Ecoplatform.Pipeline.Unpack ->
          config.pipeline.unpack_publish_target
      end

    Supervisor.child_spec(
      {__MODULE__,
       [
         name: Module.concat(processor, Publisher),
         uri: uri,
         exchange: exchange,
         routing_key: routing_key
       ]},
      id: {__MODULE__, processor}
    )
  end

  @doc false
  def child_spec(opts) do
    %{
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  @doc """
  Starts a Publisher with the options given in `opts`.

  ## Options:
  - `:uri` - A RabbitMQ connection URI. Required.
  - `:exchange` - The name of the exchange that messages should be published to.
     Required.
  - `:connect_timeout` - The number of milliseconds to wait after a failed
     connection attempt before trying to connect again. Optional, defaults to 5000.
  """
  def start_link(opts) do
    name = Keyword.fetch!(opts, :name)

    :gen_statem.start_link({:local, name}, __MODULE__, opts, [])
  end

  @doc """
  Publishes a message to RabbitMQ, using the client identified by `processor`.
  """
  def publish(processor, message) do
    :gen_statem.call(processor, {:publish, message})
  end

  @impl :gen_statem
  def callback_mode() do
    :state_functions
  end

  @impl :gen_statem
  def init(opts) do
    data = %__MODULE__{
      uri: Keyword.fetch!(opts, :uri),
      exchange: Keyword.fetch!(opts, :exchange),
      routing_key: Keyword.fetch!(opts, :routing_key),
      connect_timeout: Keyword.get(opts, :connect_timeout, 5000),
      conn: nil,
      channel: nil
    }

    actions = [{:next_event, :internal, :connect}]

    {:ok, :disconnected, data, actions}
  end

  @doc false
  def disconnected(event_type, :connect, data) when event_type in [:internal, :state_timeout] do
    case AMQP.Connection.open(data.uri) do
      {:ok, conn} ->
        Process.monitor(conn.pid)
        {:ok, channel} = AMQP.Channel.open(conn)
        :ok = AMQP.Exchange.declare(channel, data.exchange, :topic, durable: true)
        {:next_state, :connected, %{data | conn: conn, channel: channel}}

      {:error, :timeout} ->
        actions = [{:next_event, :internal, :connect}]
        {:keep_state_and_data, actions}

      {:error, _error} ->
        actions = [{:state_timeout, data.connect_timeout, :connect}]
        {:keep_state_and_data, actions}
    end
  end

  def disconnected({:call, from}, {:publish, _payload}, _data) do
    actions = [{:reply, from, {:error, :disconnected}}]
    {:keep_state_and_data, actions}
  end

  @doc false
  def connected({:call, from}, {:publish, payload}, data) do
    case do_publish(data, payload) do
      :ok ->
        actions = [{:reply, from, :ok}]
        {:keep_state_and_data, actions}
    end
  end

  def connected(:info, {:DOWN, _ref, :process, _pid, _reason}, data) do
    actions = [{:next_event, :internal, :connect}]
    {:next_state, :disconnected, data, actions}
  end

  defp do_publish(data, payload) do
    AMQP.Basic.publish(data.channel, data.exchange, data.routing_key, payload, persistent: true)
  end
end
