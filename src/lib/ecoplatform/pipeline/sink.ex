defmodule Ecoplatform.Pipeline.Sink do
  @moduledoc """
  Sinks data from RabbitMQ into the database.
  """

  use Broadway

  alias Broadway.Message
  alias Ecoplatform.Datasets
  alias Ecoplatform.Pipeline
  alias Ecoplatform.Pipeline.Schema

  def opts(config) do
    [
      producer_opts: [
        queue: config.pipeline.sink_queue,
        connection: Pipeline.rabbitmq_uri(config.rabbitmq),
        on_failure: :reject,
        qos: [prefetch_count: 100],
        metadata: [
          :timestamp
        ]
      ]
    ]
  end

  def start_link(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    producer_module = Keyword.fetch!(opts, :producer_module)
    producer_opts = Keyword.get(opts, :producer_opts, [])

    Broadway.start_link(__MODULE__,
      name: name,
      producer: [
        module: {producer_module, producer_opts},
        concurrency: 1
      ],
      processors: [
        default: []
      ],
      batchers: [
        default: [
          concurrency: System.schedulers_online() * 2,
          batch_size: 10,
          batch_timeout: 1000
        ]
      ]
    )
  end

  @impl Broadway
  def handle_message(_processor, message, _) do
    case normalize_payload(message.data) do
      {:ok, payload} -> Message.put_data(message, payload)
      error -> Message.failed(message, error)
    end
  end

  defp normalize_payload(data) do
    with {:ok, data} <- Jason.decode(data),
         :ok <- Schema.validate_unpacked(data) do
      data =
        Enum.map(data, fn data ->
          %{
            ts: DateTime.from_unix!(data["ts"] * 1000, :microsecond),
            datastream_id: data["datastream_id"],
            value: :erlang.float(data["value"])
          }
        end)

      {:ok, data}
    end
  end

  @impl Broadway
  def handle_batch(_batcher, messages, _, _) do
    datapoints = Enum.flat_map(messages, fn message -> message.data end)
    :ok = Datasets.insert_datapoints(datapoints)
    messages
  end
end
