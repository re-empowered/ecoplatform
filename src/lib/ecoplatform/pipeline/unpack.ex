defmodule Ecoplatform.Pipeline.Unpack do
  @moduledoc """
  Unpacks the received data from the format published on the MQTT interface to
  the format used by the downstream pipeline processors.
  """

  use Broadway

  alias Broadway.Message
  alias Ecoplatform.Pipeline
  alias Ecoplatform.Pipeline.Cache
  alias Ecoplatform.Pipeline.Schema
  alias Ecoplatform.Pipeline.Publisher

  def opts(config) do
    [
      producer_opts: [
        queue: config.pipeline.unpack_queue,
        connection: Pipeline.rabbitmq_uri(config.rabbitmq),
        on_failure: :reject,
        qos: [prefetch_count: 50],
        metadata: [
          :routing_key
        ]
      ]
    ]
  end

  def start_link(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    producer_module = Keyword.fetch!(opts, :producer_module)
    producer_opts = Keyword.get(opts, :producer_opts, [])

    dataset_id_fn = Keyword.get(opts, :dataset_id_fn, &Cache.dataset_id/1)
    datastream_id_fn = Keyword.get(opts, :datastream_id_fn, &Cache.datastream_id/2)
    publish_fn = Keyword.get(opts, :publish_fn, &publish/1)

    Broadway.start_link(__MODULE__,
      name: name,
      context: %{
        dataset_id_fn: dataset_id_fn,
        datastream_id_fn: datastream_id_fn,
        publish_fn: publish_fn
      },
      producer: [
        module: {producer_module, producer_opts},
        concurrency: 1
      ],
      processors: [
        default: []
      ]
    )
  end

  defp publish([]) do
    :ok
  end

  defp publish(payload) do
    payload = Jason.encode!(payload)
    Publisher.publish(__MODULE__.Publisher, payload)
  end

  @impl Broadway
  def handle_message(_processor, message, context) do
    with {:ok, payload} <- normalize_payload(message.data),
         {:ok, dataset_id} <- lookup_dataset_id(message.metadata.routing_key, context) do
      payload
      |> unpack_values(dataset_id, context)
      |> context.publish_fn.()

      message
    else
      error ->
        Message.failed(message, error)
    end
  end

  defp normalize_payload(data) do
    with {:ok, data} <- Jason.decode(data),
         :ok <- Schema.validate_packed(data) do
      {:ok, data}
    end
  end

  defp lookup_dataset_id(routing_key, context) do
    case context.dataset_id_fn.(routing_key) do
      {:ok, nil} -> {:error, :unknown_routing_key}
      response -> response
    end
  end

  defp lookup_datastream_id(dataset_id, datastream_name, context) do
    case context.datastream_id_fn.(dataset_id, datastream_name) do
      {:ok, nil} -> {:error, :unknown_datastream}
      response -> response
    end
  end

  defp unpack_values(payload, dataset_id, context) do
    Enum.reduce(payload["values"], [], fn {datastream_name, value}, acc ->
      case lookup_datastream_id(dataset_id, datastream_name, context) do
        {:ok, datastream_id} ->
          unpacked = %{datastream_id: datastream_id, ts: payload["ts"], value: value}
          [unpacked | acc]

        {:error, _error} ->
          acc
      end
    end)
  end
end
