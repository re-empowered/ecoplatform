defmodule Ecoplatform.CLI do
  @moduledoc """
  Functions to be called from shell by system administrators.
  """

  use EcoplatformWeb, :verified_routes

  alias Ecoplatform.Accounts
  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Provider

  def create_api_credentials(mqtt_namespace, name) do
    with %Provider{} = provider <- Datasets.get_provider_by_mqtt_namespace(mqtt_namespace),
         prefix = "P" <> String.pad_leading("#{provider.id}", 3, "0"),
         {:ok, credentials} <- Accounts.create_api_credentials(provider.id, name, prefix) do
      IO.puts("SUCCESS: #{credentials.key}:#{credentials.secret}")
    else
      nil ->
        IO.puts("ERROR: Provider not found.")

      {:error, changeset} ->
        Enum.each(changeset.errors, fn {field, {message, _}} ->
          IO.puts("ERROR: #{field}: #{message}")
        end)
    end
  end

  def delete_api_credentials(key) do
    case Accounts.delete_api_credentials_by_key(key) do
      :ok ->
        IO.puts("SUCCESS: Credentials were deleted")

      {:error, :not_found} ->
        IO.puts("ERROR: Credentials not found.")
    end
  end

  def create_admin_user(email, name) do
    case Accounts.create_user(email, name, true) do
      {:ok, user} ->
        Accounts.deliver_new_user_instructions(user, &url(~p"/users/reset_password/#{&1}"))
        IO.puts("SUCCESS: User created")

      {:error, changeset} ->
        Enum.each(changeset.errors, fn {field, {message, _}} ->
          IO.puts("ERROR: #{field}: #{message}")
        end)
    end
  end
end
