defmodule Ecoplatform.Repo do
  use Ecto.Repo,
    otp_app: :ecoplatform,
    adapter: Ecto.Adapters.Postgres
end
