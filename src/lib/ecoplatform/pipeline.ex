defmodule Ecoplatform.Pipeline do
  @moduledoc """
  Common pipeline functions and setup configuration.
  """

  def rabbitmq_uri(rabbitmq_config) do
    "amqp://#{rabbitmq_config.username}:#{rabbitmq_config.password}@#{rabbitmq_config.host}:5672/#{rabbitmq_config.vhost}"
  end

  @doc "Create RabbitMQ exchange, queues and bindings for pipeline"
  def setup(config) do
    {:ok, connection} = AMQP.Connection.open(rabbitmq_uri(config.rabbitmq))
    {:ok, channel} = AMQP.Channel.open(connection)

    Enum.each(exchanges(config.pipeline), fn exchange ->
      AMQP.Exchange.declare(channel, exchange, :topic, durable: true)
    end)

    Enum.each(queues(config.pipeline), fn {queue, dlx} ->
      AMQP.Queue.declare(channel, queue,
        durable: true,
        arguments: [{"x-dead-letter-exchange", :longstr, dlx}]
      )
    end)

    Enum.each(bindings(config.pipeline), fn {queue, exchange, routing_key} ->
      AMQP.Queue.bind(channel, queue, exchange, routing_key: routing_key)
    end)
  end

  defp exchanges(pipeline_config) do
    source_exchanges =
      [
        pipeline_config.sink_queue_sources,
        pipeline_config.validate_queue_sources,
        pipeline_config.unpack_queue_sources
      ]
      |> List.flatten()
      |> Enum.map(fn {exchange, _routing_key} ->
        exchange
      end)

    publish_target_exchanges =
      [pipeline_config.validate_queue_sources, pipeline_config.unpack_queue_sources]
      |> List.flatten()
      |> Enum.map(fn {exchange, _routing_key} -> exchange end)

    dlx_exchanges = [
      pipeline_config.sink_dlx,
      pipeline_config.validate_dlx,
      pipeline_config.unpack_dlx
    ]

    Enum.dedup(source_exchanges ++ publish_target_exchanges ++ dlx_exchanges)
  end

  defp queues(pipeline_config) do
    [
      {pipeline_config.sink_queue, pipeline_config.sink_dlx},
      {pipeline_config.validate_queue, pipeline_config.validate_dlx},
      {pipeline_config.unpack_queue, pipeline_config.unpack_dlx}
    ]
  end

  defp bindings(pipeline_config) do
    sink_bindings =
      Enum.map(pipeline_config.sink_queue_sources, fn {exchange, routing_key} ->
        {pipeline_config.sink_queue, exchange, routing_key}
      end)

    validate_bindings =
      Enum.map(pipeline_config.validate_queue_sources, fn {exchange, routing_key} ->
        {pipeline_config.validate_queue, exchange, routing_key}
      end)

    unpack_bindings =
      Enum.map(pipeline_config.unpack_queue_sources, fn {exchange, routing_key} ->
        {pipeline_config.unpack_queue, exchange, routing_key}
      end)

    sink_bindings ++ validate_bindings ++ unpack_bindings
  end
end
