defmodule Ecoplatform.Datasets.SilenceChecker do
  @moduledoc "Checks regularly if new silence alarms should be created"
  use GenServer

  import Ecto.Query

  alias Ecoplatform.Repo
  alias Ecoplatform.Datasets.Datastream
  alias Ecoplatform.Datasets.DatastreamSilenceAlarm

  # last_processed_message_ts is the latest timestamp seen by a pipeline processor (integer, unix epoch)
  defstruct [:last_processed_message_ts, :last_checked]

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  defp schedule_silence_check() do
    Process.send_after(self(), :check_silence, :timer.minutes(5))
  end

  @impl GenServer
  def init(_opts) do
    schedule_silence_check()
    :telemetry.attach(__MODULE__, [:broadway, :batcher, :start], &__MODULE__.handle_event/4, %{})
    {:ok, %__MODULE__{}}
  end

  @doc false
  def handle_event([:broadway, :batcher, :start], _event_meta, batch, _config) do
    case batch.messages do
      [%Broadway.Message{metadata: %{timestamp: timestamp}} | _] ->
        GenServer.cast(__MODULE__, {:notify_pipeline_progress, timestamp})

      _ ->
        nil
    end
  end

  @impl GenServer
  def handle_cast({:notify_pipeline_progress, timestamp}, state) do
    if state.last_processed_message_ts == nil or timestamp > state.last_processed_message_ts do
      {:noreply, %{state | last_processed_message_ts: timestamp}}
    else
      {:noreply, state}
    end
  end

  @impl GenServer
  def handle_info(:check_silence, state) do
    schedule_silence_check()
    state = check_silence(state)
    {:noreply, state}
  end

  def check_silence(%{last_processed_message_ts: nil} = state) do
    # If the pipeline has not progressed at all since boot we do not check any
    # silence alarms
    state
  end

  def check_silence(state) do
    check_time = DateTime.from_unix!(state.last_processed_message_ts)

    # Only check silence if the pipeline has progressed since last check
    if state.last_checked == nil or check_time > state.last_checked do
      datastreams = Repo.all(from d in Datastream, where: not is_nil(d.max_silence_seconds))

      alarms =
        datastreams
        |> Enum.filter(fn datastream ->
          datastream.latest_datapoint_ts == nil or
            DateTime.diff(check_time, datastream.latest_datapoint_ts) >=
              datastream.max_silence_seconds
        end)
        |> Enum.map(fn datastream ->
          %{datastream_id: datastream.id, raised_at: DateTime.utc_now()}
        end)

      Repo.insert_all(DatastreamSilenceAlarm, alarms)

      %{state | last_checked: check_time}
    else
      state
    end
  end
end
