defmodule Ecoplatform.Datasets.AlarmNotifier do
  @moduledoc "Sends alarm notifications periodically"

  use GenServer

  alias Ecoplatform.Datasets
  alias Ecoplatform.Accounts.UserNotifier

  defmodule DatasetAlarmGroup do
    @moduledoc false
    use EcoplatformWeb, :verified_routes

    defstruct [:dataset_id, :dataset_name, :dataset_url, :datastream_alarm_groups]

    def from_alarm(alarm) do
      %DatasetAlarmGroup{
        dataset_id: alarm.dataset_id,
        dataset_name: alarm.dataset_name,
        dataset_url: url(~p"/datasets/#{alarm.dataset_id}/alarms")
      }
    end
  end

  defmodule DatastreamAlarmGroup do
    @moduledoc false
    defstruct [:datastream_id, :datastream_name, :alarms]

    def from_alarm(alarm) do
      %DatastreamAlarmGroup{
        datastream_id: alarm.alarm.datastream_id,
        datastream_name: alarm.datastream_name
      }
    end
  end

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init(_opts) do
    schedule_alarm_notifications()
    {:ok, %{}}
  end

  defp schedule_alarm_notifications() do
    Process.send_after(self(), :send_alarm_notifications, :timer.minutes(5))
  end

  @impl GenServer
  def handle_info(:send_alarm_notifications, state) do
    schedule_alarm_notifications()
    send_alarm_notifications()
    {:noreply, state}
  end

  defp send_alarm_notifications() do
    one_hour_ago = DateTime.add(DateTime.utc_now(), -1, :hour)
    alarm_subscriptions = Datasets.alarm_subscriptions_dormant_since(one_hour_ago)

    subscribed_dataset_ids = unique_dataset_ids(alarm_subscriptions)

    dataset_alarm_groups =
      (Datasets.value_alarms_since(one_hour_ago, subscribed_dataset_ids) ++
         Datasets.silence_alarms_since(one_hour_ago, subscribed_dataset_ids))
      |> group_alarms_by_dataset()

    alarm_subscriptions
    |> group_by_subscriber()
    |> notify_subscriptions(dataset_alarm_groups)
  end

  defp unique_dataset_ids(alarm_subscriptions) do
    alarm_subscriptions
    |> MapSet.new(fn s -> s.dataset_id end)
    |> MapSet.to_list()
  end

  defp group_alarms_by_dataset(alarms) do
    alarms
    |> Enum.group_by(&DatasetAlarmGroup.from_alarm/1)
    |> Enum.map(&group_alarms_by_datastream/1)
    |> Enum.sort_by(fn dataset_alarm_group -> dataset_alarm_group.dataset_id end)
  end

  defp group_alarms_by_datastream({dataset_alarm_group, alarms}) do
    datastream_alarm_groups =
      alarms
      |> Enum.group_by(&DatastreamAlarmGroup.from_alarm/1)
      |> Enum.map(&sort_alarms/1)
      |> Enum.sort_by(fn datastream_alarm_group -> datastream_alarm_group.datastream_id end)

    %{dataset_alarm_group | datastream_alarm_groups: datastream_alarm_groups}
  end

  defp sort_alarms({datastream_alarm_group, alarms}) do
    alarms =
      alarms
      |> Enum.map(fn %{alarm: alarm} -> alarm end)
      |> Enum.sort_by(fn alarm -> alarm.raised_at end, DateTime)

    %{datastream_alarm_group | alarms: alarms}
  end

  defp group_by_subscriber(subscriptions) do
    subscriptions
    |> Enum.group_by(
      fn subscription -> Map.take(subscription, [:email, :name, :user_id]) end,
      fn %{dataset_id: dataset_id} -> dataset_id end
    )
  end

  defp notify_subscriptions(subscriptions, dataset_alarm_groups) do
    Enum.each(subscriptions, fn {subscriber, subscribed_dataset_ids} ->
      dataset_alarm_groups
      |> Enum.filter(fn dataset_alarm_group ->
        dataset_alarm_group.dataset_id in subscribed_dataset_ids
      end)
      |> notify_subscriber(subscriber)
    end)
  end

  defp notify_subscriber([], _subscriber) do
    :ok
  end

  defp notify_subscriber(dataset_alarm_groups, subscriber) do
    UserNotifier.deliver_alarm_notification(subscriber, dataset_alarm_groups)

    notified_dataset_ids =
      Enum.map(dataset_alarm_groups, fn dataset_alarm_group -> dataset_alarm_group.dataset_id end)

    Datasets.mark_alarm_subscriptions_notified(notified_dataset_ids, subscriber.user_id)
  end
end
