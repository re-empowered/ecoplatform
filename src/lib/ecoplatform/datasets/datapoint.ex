defmodule Ecoplatform.Datasets.Datapoint do
  @moduledoc """
  Datapoint schema.
  """

  use Ecto.Schema

  alias Ecoplatform.Datasets.Datastream

  @primary_key false
  schema "datapoints" do
    belongs_to :datastream, Datastream
    field :ts, :utc_datetime_usec
    field :value, :float
  end
end
