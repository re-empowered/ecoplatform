defmodule Ecoplatform.Datasets.Datastream do
  @moduledoc """
  Datastream schema and related changesets.
  """

  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Ecoplatform.Datasets.Dataset

  @derive {
    Flop.Schema,
    filterable: [:name, :description, :features],
    sortable: [:id, :name, :description],
    default_order: %{
      order_by: [:id],
      order_directions: [:asc]
    },
    default_limit: 20,
    custom_fields: [
      features: [
        filter: {__MODULE__, :filter_by_feature, []},
        ecto_type: :string,
        operators: [:ilike]
      ]
    ]
  }

  schema "datastreams" do
    field :name, :string
    field :description, :string
    field :min_value, :float
    field :max_value, :float
    field :max_silence_seconds, :integer
    field :latest_datapoint_ts, :utc_datetime_usec
    field :physical_unit, :string
    field :sensor_type, :string
    field :address, :string
    field :geo_tag, :string
    field :project_tag, :string
    field :sensor_owner, :string
    field :data_owner, :string
    field :acquisition_responsible, :string
    field :confidentiality_tag, :string
    field :gdpr_classification, :string
    field :other_comments, :string
    belongs_to :dataset, Dataset
    timestamps()
  end

  def changeset(datastream, dataset_id, name, params) do
    params = Map.merge(%{dataset_id: dataset_id, name: name}, params)

    datastream
    |> change(params)
    |> validate_dataset()
    |> validate_name()
    |> Ecto.Changeset.validate_number(:max_silence_seconds, greater_than_or_equal_to: 300)
  end

  def validate_dataset(changeset) do
    changeset
    |> validate_required([:dataset_id])
    |> foreign_key_constraint(:dataset_id)
  end

  def validate_name(changeset) do
    changeset
    |> validate_required([:name])
    |> validate_length(:name, min: 2, max: 160)
    |> validate_format(:name, ~r|^[[:alnum:]/_-]+$|,
      message:
        "must contain only alphanumeric characters (letters and numbers), slashes (/), dashes (-) and underscores (_)"
    )
    |> unique_constraint([:dataset_id, :name], error_key: :name)
  end

  # credo:disable-for-next-line
  def filter_by_feature(query, %Flop.Filter{value: value}, _opts) do
    filter = "%#{value}%"

    from d in query,
      where:
        ilike(d.physical_unit, ^filter) or
          ilike(d.sensor_type, ^filter) or
          ilike(d.address, ^filter) or
          ilike(d.geo_tag, ^filter) or
          ilike(d.project_tag, ^filter) or
          ilike(d.sensor_owner, ^filter) or
          ilike(d.data_owner, ^filter) or
          ilike(d.acquisition_responsible, ^filter) or
          ilike(d.confidentiality_tag, ^filter) or
          ilike(d.gdpr_classification, ^filter)
  end
end
