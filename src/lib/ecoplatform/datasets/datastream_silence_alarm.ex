defmodule Ecoplatform.Datasets.DatastreamSilenceAlarm do
  @moduledoc """
  Datastream silence alarm schema.
  """

  use Ecto.Schema

  alias Ecoplatform.Datasets.Datastream

  @primary_key false
  schema "datastream_silence_alarms" do
    belongs_to :datastream, Datastream
    field :raised_at, :utc_datetime_usec
  end
end
