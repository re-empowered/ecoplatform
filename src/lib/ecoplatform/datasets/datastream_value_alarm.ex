defmodule Ecoplatform.Datasets.DatastreamValueAlarm do
  @moduledoc """
  Datastream value alarm schema.
  """

  use Ecto.Schema

  alias Ecoplatform.Datasets.Datastream

  @primary_key false
  schema "datastream_value_alarms" do
    belongs_to :datastream, Datastream
    field :raised_at, :utc_datetime_usec
    field :ts, :utc_datetime_usec
    field :value, :float
  end
end
