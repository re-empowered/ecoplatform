defmodule Ecoplatform.Datasets.Dataset do
  @moduledoc """
  Dataset schema and related changesets.
  """

  use Ecto.Schema

  import Ecto.Changeset

  alias Ecoplatform.Datasets.Provider
  alias Ecoplatform.Datasets.Datastream
  alias Ecoplatform.Accounts.User

  @derive {
    Flop.Schema,
    filterable: [:name, :description, :mqtt_subtopic, :provider_name, :provider_mqtt_namespace],
    sortable: [:name, :description, :mqtt_subtopic, :provider_name, :provider_mqtt_namespace],
    default_limit: 20,
    join_fields: [
      provider_name: [
        binding: :provider,
        field: :name,
        ecto_type: :string
      ],
      provider_mqtt_namespace: [
        binding: :provider,
        field: :mqtt_namespace,
        ecto_type: :string
      ]
    ]
  }

  schema "datasets" do
    belongs_to :provider, Provider
    field :name, :string
    field :description, :string
    field :mqtt_subtopic, :string
    many_to_many :owners, User, join_through: "dataset_owners", on_delete: :delete_all
    has_many :datastreams, Datastream

    timestamps()
  end

  def changeset(dataset, provider_id, name, description, mqtt_subtopic) do
    dataset
    |> change(
      provider_id: provider_id,
      name: name,
      description: description,
      mqtt_subtopic: mqtt_subtopic
    )
    |> validate_provider()
    |> validate_name()
    |> validate_mqtt_subtopic()
  end

  def routing_key(dataset) do
    [
      String.replace(dataset.provider.mqtt_namespace, "/", "."),
      String.replace(dataset.mqtt_subtopic, "/", ".")
    ]
    |> Enum.join(".")
  end

  defp validate_provider(changeset) do
    changeset
    |> validate_required([:provider_id])
    |> foreign_key_constraint(:provider_id)
  end

  defp validate_name(changeset) do
    changeset
    |> validate_required([:name])
    |> validate_length(:name, min: 2, max: 160)
    |> unique_constraint(:name)
  end

  defp validate_mqtt_subtopic(changeset) do
    changeset
    |> validate_required([:mqtt_subtopic])
    |> validate_format(:mqtt_subtopic, ~r|^[[:alnum:]/_-]+$|,
      message:
        "must contain only alphanumeric characters (letters and numbers), slashes (/), dashes (-) and underscores (_)"
    )
    |> validate_change(:mqtt_subtopic, fn :mqtt_subtopic, mqtt_subtopic ->
      cond do
        String.starts_with?(mqtt_subtopic, "/") ->
          [mqtt_subtopic: "must not start with a slash (/)"]

        String.ends_with?(mqtt_subtopic, "/") ->
          [mqtt_subtopic: "must not end with a slash (/)"]

        String.contains?(mqtt_subtopic, "//") ->
          [mqtt_subtopic: "must not contain double slashes (//)"]

        true ->
          []
      end
    end)
  end
end
