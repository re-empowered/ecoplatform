defmodule Ecoplatform.Datasets.AlarmSubscription do
  @moduledoc """
  Alarm subscription schema and related changeset.
  """

  use Ecto.Schema
  import Ecto.Changeset

  alias Ecoplatform.Accounts.User
  alias Ecoplatform.Datasets.Dataset

  @primary_key false
  schema "alarm_subscriptions" do
    belongs_to :dataset, Dataset
    belongs_to :user, User
    field :notified_at, :naive_datetime
  end

  def changeset(dataset_id, user_id) do
    %__MODULE__{}
    |> change(dataset_id: dataset_id, user_id: user_id)
    |> validate_required([:dataset_id, :user_id])
    |> foreign_key_constraint(:dataset_id)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:user_id,
      name: :alarm_subscriptions_dataset_id_user_id_index,
      message: "is already subscribed"
    )
  end
end
