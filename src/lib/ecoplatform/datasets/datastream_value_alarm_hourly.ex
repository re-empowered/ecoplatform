defmodule Ecoplatform.Datasets.DatastreamValueAlarmHourly do
  @moduledoc """
  Datastream value alarm hourly schema.

  The underlying table is a Timescale continous aggregate (and is not a table,
  but a materialized view).
  """

  use Ecto.Schema

  alias Ecoplatform.Datasets.Datastream

  @derive {
    Flop.Schema,
    filterable: [:datastream_name],
    sortable: [:raised_during],
    default_order: %{
      order_by: [:raised_during],
      order_directions: [:desc]
    },
    default_limit: 20,
    join_fields: [
      datastream_name: [
        binding: :datastream,
        field: :name,
        ecto_type: :string
      ]
    ]
  }

  @primary_key false

  schema "datastream_value_alarms_hourly" do
    belongs_to :datastream, Datastream, primary_key: true
    field :raised_during, :utc_datetime_usec, primary_key: true
    field :earliest, :utc_datetime_usec
    field :latest, :utc_datetime_usec
    field :alarms, :integer
    field :value_min, :float
    field :value_max, :float
    field :value_p05, :float
    field :value_p10, :float
    field :value_p20, :float
    field :value_p50, :float
    field :value_p80, :float
    field :value_p90, :float
    field :value_p95, :float
  end
end
