defmodule Ecoplatform.Datasets.Provider do
  @moduledoc """
  Provider schema and related changesets.
  """

  use Ecto.Schema

  import Ecto.Changeset

  schema "providers" do
    field :mqtt_namespace, :string
    field :name, :string

    timestamps()
  end

  def changeset(provider, name, mqtt_namespace) do
    provider
    |> change(name: name, mqtt_namespace: mqtt_namespace)
    |> validate_name()
    |> validate_topic_namespace()
  end

  defp validate_name(changeset) do
    changeset
    |> Ecto.Changeset.validate_required([:name])
    |> Ecto.Changeset.validate_length(:name, min: 2, max: 160)
    |> Ecto.Changeset.unique_constraint(:name)
  end

  defp validate_topic_namespace(changeset) do
    changeset
    |> Ecto.Changeset.validate_required([:mqtt_namespace])
    |> Ecto.Changeset.validate_length(:mqtt_namespace, min: 2, max: 60)
    |> Ecto.Changeset.validate_format(:mqtt_namespace, ~r/^[[:alnum:]_-]+$/,
      message:
        "must contain only alphanumeric characters (letters and numbers), dashes (-) and underscores (_)"
    )
    |> Ecto.Changeset.unique_constraint(:mqtt_namespace)
  end
end
