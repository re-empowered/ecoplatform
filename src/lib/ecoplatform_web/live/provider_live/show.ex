defmodule EcoplatformWeb.ProviderLive.Show do
  use EcoplatformWeb, :live_view

  alias Ecoplatform.Datasets

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    {:noreply,
     socket
     |> assign(:page_title, page_title(socket.assigns.live_action))
     |> assign(:provider, Datasets.get_provider!(id))}
  end

  defp page_title(:show), do: "Show provider"
  defp page_title(:edit), do: "Edit provider"
end
