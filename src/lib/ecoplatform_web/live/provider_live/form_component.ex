defmodule EcoplatformWeb.ProviderLive.FormComponent do
  use EcoplatformWeb, :live_component

  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="provider-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:mqtt_namespace]} type="text" label="MQTT namespace" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Provider</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{provider: provider} = assigns, socket) do
    changeset = changeset(provider)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  defp changeset(provider, params \\ %{}) do
    types = %{name: :string, mqtt_namespace: :string}
    data = Map.take(provider, Map.keys(types))

    {data, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:name, :mqtt_namespace])
    |> Ecto.Changeset.validate_length(:name, min: 2, max: 160)
    |> Ecto.Changeset.validate_length(:mqtt_namespace, min: 2, max: 60)
    |> Ecto.Changeset.validate_format(:mqtt_namespace, ~r/^[[:alnum:]_-]+$/,
      message:
        "must contain only alphanumeric characters (letters and numbers), dashes (-) and underscores (_)"
    )
  end

  @impl true
  def handle_event("validate", %{"provider" => provider_params}, socket) do
    changeset =
      changeset(socket.assigns.provider, provider_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"provider" => provider_params}, socket) do
    with changeset <- changeset(socket.assigns.provider, provider_params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate),
         {:ok, socket} <- save_provider(socket, socket.assigns.action, params) do
      {:noreply, socket}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_provider(socket, :edit, params) do
    case Datasets.update_provider(socket.assigns.provider, params.name, params.mqtt_namespace) do
      {:ok, provider} ->
        notify_parent({:saved, provider})

        {:ok,
         socket
         |> put_flash(:info, "Provider updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp save_provider(socket, :new, params) do
    case Datasets.create_provider(params.name, params.mqtt_namespace) do
      {:ok, provider} ->
        notify_parent({:saved, provider})

        {:ok,
         socket
         |> put_flash(:info, "Provider created successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :provider))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
