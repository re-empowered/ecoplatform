defmodule EcoplatformWeb.DatasetLive.FormComponent do
  use EcoplatformWeb, :live_component

  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="dataset-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:provider_id]} type="select" label="Provider" options={@providers} />
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:description]} type="textarea" label="Description" />
        <.input field={@form[:mqtt_subtopic]} type="text" label="MQTT subtopic" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Dataset</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{dataset: dataset} = assigns, socket) do
    providers =
      Datasets.list_providers() |> Enum.map(fn provider -> {provider.name, provider.id} end)

    changeset = changeset(dataset)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(providers: providers)
     |> assign_form(changeset)}
  end

  defp changeset(dataset, params \\ %{}) do
    types = %{provider_id: :integer, name: :string, description: :string, mqtt_subtopic: :string}
    data = Map.take(dataset, Map.keys(types))

    {data, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:provider_id, :name, :description, :mqtt_subtopic])
    |> Ecto.Changeset.validate_length(:name, min: 2, max: 160)
    |> Ecto.Changeset.validate_format(:mqtt_subtopic, ~r|^[[:alnum:]/_-]+$|,
      message:
        "must contain only alphanumeric characters (letters and numbers), slashes (/), dashes (-) and underscores (_)"
    )
    |> Ecto.Changeset.validate_change(:mqtt_subtopic, fn :mqtt_subtopic, mqtt_subtopic ->
      cond do
        String.starts_with?(mqtt_subtopic, "/") ->
          [mqtt_subtopic: "must not start with a slash (/)"]

        String.ends_with?(mqtt_subtopic, "/") ->
          [mqtt_subtopic: "must not end with a slash (/)"]

        String.contains?(mqtt_subtopic, "//") ->
          [mqtt_subtopic: "must not contain double slashes (//)"]

        true ->
          []
      end
    end)
  end

  @impl true
  def handle_event("validate", %{"dataset" => dataset_params}, socket) do
    changeset =
      changeset(socket.assigns.dataset, dataset_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"dataset" => dataset_params}, socket) do
    with changeset <- changeset(socket.assigns.dataset, dataset_params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate),
         {:ok, socket} <- save_dataset(socket, socket.assigns.action, params) do
      {:noreply, socket}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_dataset(socket, :edit, params) do
    case Datasets.update_dataset(
           socket.assigns.dataset,
           params.provider_id,
           params.name,
           params.description,
           params.mqtt_subtopic
         ) do
      {:ok, dataset} ->
        notify_parent({:saved, :edit, dataset})

        {:ok,
         socket
         |> put_flash(:info, "Dataset updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp save_dataset(socket, :new, params) do
    case Datasets.create_dataset(
           params.provider_id,
           params.name,
           params.description,
           params.mqtt_subtopic
         ) do
      {:ok, dataset} ->
        notify_parent({:saved, :new, dataset})

        {:ok,
         socket
         |> put_flash(:info, "Dataset created successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :dataset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
