defmodule EcoplatformWeb.DatasetLive.Show do
  use EcoplatformWeb, :live_view

  import EcoplatformWeb.DatasetLive.Show.DatasetTabMenu

  alias Ecoplatform.Datasets

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    dataset = Datasets.get_dataset!(id)

    if Datasets.user_can_access?(dataset, socket.assigns.current_user) do
      {:noreply,
       socket
       |> assign(:page_title, page_title(socket.assigns.live_action))
       |> assign(:dataset, dataset)}
    else
      socket =
        socket
        |> Phoenix.LiveView.put_flash(:error, "You do not have access to this dataset.")
        |> Phoenix.LiveView.redirect(to: ~p"/")

      {:noreply, socket}
    end
  end

  defp page_title(:show), do: "Show dataset"
  defp page_title(:edit), do: "Edit dataset"
end
