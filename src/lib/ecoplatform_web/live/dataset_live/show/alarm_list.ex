defmodule EcoplatformWeb.DatasetLive.Show.AlarmList do
  @moduledoc false

  use EcoplatformWeb, :live_view

  alias Ecoplatform.Datasets
  import EcoplatformWeb.DatasetLive.Show.DatasetTabMenu

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     stream_configure(socket, :alarm_summaries,
       dom_id: fn a -> "#{a.datastream_id}-#{DateTime.to_unix(a.raised_during)}" end
     )}
  end

  @impl true
  def handle_params(%{"id" => id} = params, _, socket) do
    dataset = Datasets.get_dataset!(id)

    subscribed_to_alarms? = Datasets.has_alarm_subscription?(dataset, socket.assigns.current_user)

    if Datasets.user_can_access?(dataset, socket.assigns.current_user) do
      {:noreply,
       socket
       |> assign(:page_title, page_title(socket.assigns.live_action))
       |> assign(:dataset, dataset)
       |> assign_flop(params)
       |> assign(alarm_summary_id: params["alarm_summary_id"])
       |> assign(subscribed_to_alarms?: subscribed_to_alarms?)}
    else
      socket =
        socket
        |> Phoenix.LiveView.put_flash(:error, "You do not have access to this dataset.")
        |> Phoenix.LiveView.redirect(to: ~p"/")

      socket
    end
  end

  defp assign_flop(socket, params) do
    flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])

    {:ok, {alarm_summaries, meta}} =
      Datasets.flop_dataset_value_alarm_hourly(socket.assigns.dataset, flop_params)

    socket
    |> stream(:alarm_summaries, alarm_summaries, reset: true)
    |> assign(:flop_meta, meta)
  end

  defp page_title(:index), do: "Listing alarms"
  defp page_title(:show), do: "Show alarm"

  @impl true
  def handle_event("update-filter", params, socket) do
    flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])

    {:noreply,
     push_patch(socket, to: ~p"/datasets/#{socket.assigns.dataset}/alarms?#{flop_params}")}
  end

  def handle_event("reset-filter", _, socket) do
    flop =
      Flop.set_page(socket.assigns.flop_meta.flop, 1)
      |> Flop.reset_filters()

    path =
      Flop.Phoenix.build_path(~p"/datasets/#{socket.assigns.dataset}/alarms", flop,
        backend: socket.assigns.flop_meta.backend
      )

    {:noreply, push_patch(socket, to: path)}
  end

  def handle_event("subscribe-alarm-notifications", _params, socket) do
    case Datasets.create_alarm_subscription(socket.assigns.dataset, socket.assigns.current_user) do
      {:ok, _subscription} ->
        {:noreply,
         socket
         |> assign(subscribed_to_alarms?: true)
         |> put_flash(:info, "Subscribed")}

      {:error, %Ecto.Changeset{}} ->
        {:noreply,
         socket
         |> assign(subscribed_to_alarms?: true)
         |> put_flash(:error, "Already subscribed")}
    end
  end

  def handle_event("unsubscribe-alarm-notifications", _params, socket) do
    case Datasets.delete_alarm_subscription(socket.assigns.dataset, socket.assigns.current_user) do
      :ok ->
        {:noreply,
         socket
         |> assign(subscribed_to_alarms?: false)
         |> put_flash(:info, "Unsubscribed")}

      _error ->
        {:noreply, put_flash(socket, :error, "Unexpected error occured")}
    end
  end
end
