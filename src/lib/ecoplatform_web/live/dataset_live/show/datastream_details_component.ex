defmodule EcoplatformWeb.DatasetLive.Show.DatastreamDetailsComponent do
  @moduledoc false

  use EcoplatformWeb, :live_component

  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Preview of the datastream data in your database.</:subtitle>
      </.header>

      <div
        style="width:600px; height: 600px"
        id="graph"
        phx-hook="VegaLite"
        phx-update="ignore"
        data-id={@id}
      />
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    data = get_data(assigns.datastream)

    spec =
      VegaLite.new(width: :container, height: :container, padding: 5)
      # Load values. Values are a map with the attributes to be used by Vegalite
      |> VegaLite.data_from_values(data)
      # Defines the type of mark to be used
      |> VegaLite.mark(:line)
      # Sets the axis, the key for the data and the type of data
      |> VegaLite.encode_field(:x, "ts",
        type: :nominal,
        axis: [label_overlap: true]
      )
      |> VegaLite.encode_field(:y, "value", type: :quantitative)
      # Output the specifcation
      |> VegaLite.to_spec()

    {:ok,
     socket
     |> assign(assigns)
     |> assign(id: socket.id)
     |> push_event("vega_lite:#{socket.id}:init", %{"spec" => spec})}
  end

  defp get_data(datastream) do
    end_ts = DateTime.utc_now()
    start_ts = DateTime.add(end_ts, -7, :day)

    {:ok, data} =
      Ecoplatform.Repo.transaction(fn ->
        Datasets.stream_datapoints(datastream.id, start_ts, end_ts, :desc)
        |> Stream.map(fn m -> %{ts: m.ts, value: m.value} end)
        |> Enum.take(1000)
      end)

    data
  end
end
