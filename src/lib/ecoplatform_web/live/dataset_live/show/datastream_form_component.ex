defmodule EcoplatformWeb.DatasetLive.Show.DatastreamFormComponent do
  @moduledoc false

  use EcoplatformWeb, :live_component

  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="datastream-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:name]} type="text" label="Name" />
        <.input field={@form[:description]} type="textarea" label="Description" />
        <.input field={@form[:min_value]} type="number" label="Min value" />
        <.input field={@form[:max_value]} type="number" label="Max value" />
        <.input field={@form[:max_silence_seconds]} type="number" label="Max silence (seconds)" />
        <.input field={@form[:physical_unit]} type="text" label="Physical unit" />
        <.input field={@form[:sensor_type]} type="text" label="Sensor type" />
        <.input field={@form[:address]} type="text" label="Address" />
        <.input field={@form[:geo_tag]} type="text" label="Geo tag" />
        <.input field={@form[:project_tag]} type="text" label="Project tag" />
        <.input field={@form[:sensor_owner]} type="text" label="Sensor owner" />
        <.input field={@form[:data_owner]} type="text" label="Data owner" />
        <.input field={@form[:acquisition_responsible]} type="text" label="Acquisition responsible" />
        <.input field={@form[:confidentiality_tag]} type="text" label="Confidentiality tag" />
        <.input field={@form[:gdpr_classification]} type="text" label="GDPR classification" />
        <.input field={@form[:other_comments]} type="textarea" label="Other comments" />
        <:actions>
          <.button phx-disable-with="Saving...">Save datastream</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    changeset = changeset(assigns.datastream)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  defp changeset(datastream, params \\ %{}) do
    types = %{
      name: :string,
      description: :string,
      min_value: :float,
      max_value: :float,
      max_silence_seconds: :integer,
      physical_unit: :string,
      sensor_type: :string,
      address: :string,
      geo_tag: :string,
      project_tag: :string,
      sensor_owner: :string,
      data_owner: :string,
      acquisition_responsible: :string,
      confidentiality_tag: :string,
      gdpr_classification: :string,
      other_comments: :string
    }

    data = Map.take(datastream, Map.keys(types))

    {data, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:name])
    |> Ecto.Changeset.validate_number(:max_silence_seconds, greater_than_or_equal_to: 300)
  end

  @impl true
  def handle_event("validate", %{"new_datastream" => add_datastream_params}, socket) do
    changeset =
      changeset(socket.assigns.datastream, add_datastream_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"new_datastream" => add_datastream_params}, socket) do
    with changeset <- changeset(socket.assigns.datastream, add_datastream_params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate),
         {:ok, socket} <- save_datastream(socket, socket.assigns.action, params) do
      {:noreply, socket}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_datastream(socket, :edit, params) do
    case Datasets.update_datastream(
           socket.assigns.datastream,
           socket.assigns.dataset.id,
           params.name,
           Map.drop(params, [:name])
         ) do
      {:ok, _} ->
        {:ok,
         socket
         |> put_flash(:info, "Datastream updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp save_datastream(socket, :new, params) do
    case Datasets.create_datastream(
           socket.assigns.dataset.id,
           params.name,
           Map.drop(params, [:name])
         ) do
      {:ok, _} ->
        {:ok,
         socket
         |> put_flash(:info, "Datastream created successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :new_datastream))
  end
end
