defmodule EcoplatformWeb.DatasetLive.Show.DatastreamList do
  @moduledoc false

  use EcoplatformWeb, :live_view

  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Datastream
  import EcoplatformWeb.DatasetLive.Show.DatasetTabMenu

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id} = params, _, socket) do
    dataset = Datasets.get_dataset!(id)

    if Datasets.user_can_access?(dataset, socket.assigns.current_user) do
      flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])
      {:ok, {datastreams, meta}} = Datasets.flop_datastreams(dataset.id, flop_params)

      datastream =
        if datastream_id = Map.get(params, "datastream_id") do
          Datasets.get_datastream!(datastream_id)
        else
          %Datastream{}
        end

      {:noreply,
       socket
       |> assign(:page_title, page_title(socket.assigns.live_action))
       |> assign(:flop_meta, meta)
       |> assign(:dataset, dataset)
       |> assign(:datastream, datastream)
       |> assign(:show_features, Enum.any?(datastreams, fn d -> features(d) != [] end))
       |> assign(
         :show_other_comments,
         Enum.any?(datastreams, fn d -> d.other_comments != nil end)
       )
       |> stream(:flop_datastreams, datastreams, reset: true)}
    else
      socket =
        socket
        |> Phoenix.LiveView.put_flash(:error, "You do not have access to this dataset.")
        |> Phoenix.LiveView.redirect(to: ~p"/")

      {:noreply, socket}
    end
  end

  defp page_title(:index), do: "Listing datastreams"
  defp page_title(:new), do: "New datastream"
  defp page_title(:edit), do: "Edit datastream"
  defp page_title(:show), do: "Show datastream"
  defp page_title(:export), do: "Export datapoints"

  @impl true
  def handle_info(_, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("update-filter", params, socket) do
    flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])

    {:noreply,
     push_patch(socket, to: ~p"/datasets/#{socket.assigns.dataset}/datastreams?#{flop_params}")}
  end

  def handle_event("reset-filter", _, socket) do
    flop =
      Flop.set_page(socket.assigns.flop_meta.flop, 1)
      |> Flop.reset_filters()

    path =
      Flop.Phoenix.build_path(~p"/datasets/#{socket.assigns.dataset}/datastreams", flop,
        backend: socket.assigns.flop_meta.backend
      )

    {:noreply, push_patch(socket, to: path)}
  end

  def handle_event("delete", %{"id" => id}, socket) do
    datastream = Datasets.get_datastream!(id)
    {:ok, _} = Datasets.delete_datastream(datastream)

    {:noreply, stream_delete(socket, :flop_datastreams, datastream)}
  end

  defp format_latest_datapoint_ts(nil) do
    ""
  end

  defp format_latest_datapoint_ts(ts) do
    Calendar.strftime(ts, "%Y-%m-%d %H:%M:%S")
  end

  defp features(datastream) do
    Map.take(datastream, [
      :physical_unit,
      :sensor_type,
      :address,
      :geo_tag,
      :project_tag,
      :sensor_owner,
      :data_owner,
      :acquisition_responsible,
      :confidentiality_tag,
      :gdpr_classification
    ])
    |> Enum.reject(fn {_k, v} -> v == nil or v == "" end)
    |> Enum.sort_by(fn {k, _v} -> k end)
  end

  defp tag(assigns) do
    ~H"""
    <span class="m-1 text-xs inline-flex items-center font-bold leading-sm px-2 py-1 bg-blue-200 text-blue-700 rounded-full">
      <%= @key %>:<%= @value %>
    </span>
    """
  end
end
