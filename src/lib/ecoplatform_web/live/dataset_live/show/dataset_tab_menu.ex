defmodule EcoplatformWeb.DatasetLive.Show.DatasetTabMenu do
  @moduledoc false

  use EcoplatformWeb, :html

  attr :dataset, :map, required: true
  attr :current_path, :string, required: true

  def tab_menu(assigns) do
    ~H"""
    <.header_tab_menu_item navigate={~p"/datasets/#{@dataset}"} current_path={@current_path}>
      Details
    </.header_tab_menu_item>
    <.header_tab_menu_item
      navigate={~p"/datasets/#{@dataset}/datastreams"}
      current_path={@current_path}
    >
      Datastreams
    </.header_tab_menu_item>
    <.header_tab_menu_item navigate={~p"/datasets/#{@dataset}/alarms"} current_path={@current_path}>
      Alarms
    </.header_tab_menu_item>
    <.header_tab_menu_item navigate={~p"/datasets/#{@dataset}/owners"} current_path={@current_path}>
      Owners
    </.header_tab_menu_item>
    """
  end
end
