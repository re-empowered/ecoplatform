defmodule EcoplatformWeb.DatasetLive.Show.AlarmDetailsComponent do
  @moduledoc false

  use EcoplatformWeb, :live_component

  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        Value alarm details
        <:subtitle>
          Datastream: <%= @datastream.name %> | Min: <%= @datastream.min_value %> | Max: <%= @datastream.max_value %>
        </:subtitle>
      </.header>

      <.list>
        <:item title="Alarms"><%= @hourly_summary.alarms %></:item>
        <:item title="Earliest alarm"><%= @hourly_summary.earliest %></:item>
        <:item title="Latest alarm"><%= @hourly_summary.latest %></:item>
        <:item title="Mininum value"><%= Float.round(@hourly_summary.value_min, 3) %></:item>
        <:item title="Maximum value"><%= Float.round(@hourly_summary.value_max, 3) %></:item>
        <:item title="5th percentile"><%= Float.round(@hourly_summary.value_p05, 3) %></:item>
        <:item title="10th percentile"><%= Float.round(@hourly_summary.value_p10, 3) %></:item>
        <:item title="20th percentile"><%= Float.round(@hourly_summary.value_p20, 3) %></:item>
        <:item title="50th percentile"><%= Float.round(@hourly_summary.value_p50, 3) %></:item>
        <:item title="80th percentile"><%= Float.round(@hourly_summary.value_p80, 3) %></:item>
        <:item title="90th percentile"><%= Float.round(@hourly_summary.value_p90, 3) %></:item>
        <:item title="95th percentile"><%= Float.round(@hourly_summary.value_p95, 3) %></:item>
      </.list>

      <.header class="mt-20">Alarms</.header>
      <.table id="alarms" rows={@alarms}>
        <:col :let={alarm} label="Raised at"><%= alarm.raised_at %></:col>
        <:col :let={alarm} label="Timestamp"><%= alarm.ts %></:col>
        <:col :let={alarm} label="Value"><%= alarm.value %></:col>
      </.table>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    [datastream_id, raised_during] = String.split(assigns.id, "-")

    datastream = Datasets.get_datastream!(datastream_id)

    raised_during =
      raised_during
      |> String.to_integer()
      |> DateTime.from_unix!()

    {hourly_summary, alarms} = Datasets.get_value_alarm_summary(datastream_id, raised_during)

    {:ok,
     socket
     |> assign(
       assigns: assigns,
       datastream: datastream,
       hourly_summary: hourly_summary,
       alarms: alarms
     )}
  end
end
