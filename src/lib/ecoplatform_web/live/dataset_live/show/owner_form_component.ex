defmodule EcoplatformWeb.DatasetLive.Show.OwnerFormComponent do
  @moduledoc false

  use EcoplatformWeb, :live_component

  alias Ecoplatform.Accounts
  alias Ecoplatform.Datasets

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Add an owner to the dataset.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="dataset-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:user_id]} type="select" label="User" options={@users} />
        <:actions>
          <.button phx-disable-with="Saving...">Add owner</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    users =
      Accounts.list_users()
      |> Enum.map(fn user -> {user.name, user.id} end)

    changeset = changeset()

    {:ok,
     socket
     |> assign(assigns)
     |> assign(users: users)
     |> assign_form(changeset)}
  end

  defp changeset(params \\ %{}) do
    types = %{user_id: :integer}

    {%{}, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:user_id])
  end

  @impl true
  def handle_event("validate", %{"add_owner" => add_owner_params}, socket) do
    changeset =
      changeset(add_owner_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"add_owner" => add_owner_params}, socket) do
    with changeset <- changeset(add_owner_params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate),
         {:ok, _owner} <- Datasets.add_dataset_owner(socket.assigns.dataset, params.user_id) do
      {:noreply,
       socket
       |> put_flash(:info, "Dataset owner added successfully")
       |> push_patch(to: socket.assigns.patch)}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :add_owner))
  end
end
