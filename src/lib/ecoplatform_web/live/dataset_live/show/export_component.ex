defmodule EcoplatformWeb.DatasetLive.Show.ExportComponent do
  @moduledoc false

  use EcoplatformWeb, :live_component

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form for={@form} id="export-form" phx-target={@myself} phx-submit="submit">
        <.input field={@form[:start_ts]} type="datetime-local" label="Start" />
        <.input field={@form[:end_ts]} type="datetime-local" label="End" />
        <:actions>
          <.button phx-disable-with="Generating...">Generate download link</.button>
        </:actions>
      </.simple_form>

      <.button :if={@download_link} class="mt-4">
        <.link href={@download_link} target="_blank">
          Download export from <%= @start_ts %> to <%= @end_ts %>
        </.link>
      </.button>
    </div>
    """
  end

  @impl true
  def update(assigns, socket) do
    changeset = changeset()

    {:ok,
     socket
     |> assign(download_link: nil)
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  defp changeset(params \\ %{}) do
    types = %{
      start_ts: :string,
      end_ts: :string
    }

    changeset =
      {%{}, types}
      |> Ecto.Changeset.cast(params, Map.keys(types))
      |> Ecto.Changeset.validate_required(Map.keys(types))
      |> validate_datetime(:start_ts)
      |> validate_datetime(:end_ts)

    if changeset.valid? and
         DateTime.compare(
           Ecto.Changeset.get_change(changeset, :end_ts),
           Ecto.Changeset.get_change(changeset, :start_ts)
         ) ==
           :lt do
      Ecto.Changeset.add_error(changeset, :end_ts, "must be greater than or equal to start")
    else
      changeset
    end
  end

  defp validate_datetime(changeset, param) do
    datetime = Ecto.Changeset.get_change(changeset, param, "")

    with [_, year, month, day, hour, minute] <-
           Regex.run(~r/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2})$/, datetime),
         {:ok, date} <-
           Date.new(String.to_integer(year), String.to_integer(month), String.to_integer(day)),
         {:ok, time} <- Time.new(String.to_integer(hour), String.to_integer(minute), 0),
         {:ok, datetime} <- DateTime.new(date, time) do
      Ecto.Changeset.put_change(changeset, param, datetime)
    else
      _ -> Ecto.Changeset.add_error(changeset, param, "is invalid")
    end
  end

  @impl true
  def handle_event("submit", %{"export" => params}, socket) do
    with changeset <- changeset(params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate) do
      query_params = [
        datastream_ids: socket.assigns.datastream.id,
        start_ts: DateTime.to_unix(params.start_ts, :millisecond),
        end_ts: DateTime.to_unix(params.end_ts, :millisecond)
      ]

      {:noreply,
       socket
       |> assign(
         download_link: url(~p"/datasets/#{socket.assigns.dataset.id}/export?#{query_params}"),
         start_ts: params.start_ts,
         end_ts: params.end_ts
       )
       |> assign_form(changeset)}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :export))
  end
end
