defmodule EcoplatformWeb.DatasetLive.OwnerList do
  use EcoplatformWeb, :live_view

  alias Ecoplatform.Datasets
  import EcoplatformWeb.DatasetLive.Show.DatasetTabMenu

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    dataset =
      Datasets.get_dataset!(id)
      |> Ecoplatform.Repo.preload(:owners)

    if Datasets.user_can_access?(dataset, socket.assigns.current_user) do
      {:noreply,
       socket
       |> assign(:page_title, page_title(socket.assigns.live_action))
       |> assign(:dataset, dataset)}
    else
      socket =
        socket
        |> Phoenix.LiveView.put_flash(:error, "You do not have access to this dataset.")
        |> Phoenix.LiveView.redirect(to: ~p"/")

      {:noreply, socket}
    end
  end

  defp page_title(:index), do: "Listing owners"
  defp page_title(:new), do: "Add owner"

  @impl true
  def handle_event("remove-owner", params, socket) do
    Datasets.remove_dataset_owner(socket.assigns.dataset, params["user_id"])

    dataset =
      Datasets.get_dataset!(socket.assigns.dataset.id)
      |> Ecoplatform.Repo.preload(:owners)

    socket = assign(socket, :dataset, dataset)
    {:noreply, socket}
  end
end
