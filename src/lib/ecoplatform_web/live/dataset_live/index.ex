defmodule EcoplatformWeb.DatasetLive.Index do
  use EcoplatformWeb, :live_view

  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Dataset

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, list_all: false)}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit dataset")
    |> assign(:dataset, Datasets.get_dataset!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New dataset")
    |> assign(:dataset, %Dataset{})
  end

  defp apply_action(socket, :index, params) do
    flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])

    {:ok, {datasets, meta}} =
      if socket.assigns.list_all do
        Datasets.flop_datasets(flop_params)
      else
        Datasets.flop_own_datasets(socket.assigns.current_user.id, flop_params)
      end

    socket
    |> assign(page_title: "Listing Datasets", dataset: nil, flop_meta: meta)
    |> stream(:flop_datasets, datasets, reset: true)
  end

  @impl true
  def handle_info({EcoplatformWeb.DatasetLive.FormComponent, {:saved, action, dataset}}, socket) do
    if action == :new do
      Datasets.add_dataset_owner(dataset, socket.assigns.current_user.id)
    end

    {:noreply, stream_insert(socket, :flop_datasets, Datasets.get_dataset!(dataset.id), at: 0)}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    dataset = Datasets.get_dataset!(id)
    {:ok, _} = Datasets.delete_dataset(dataset)

    {:noreply, stream_delete(socket, :flop_datasets, dataset)}
  end

  def handle_event("toggle-all", _params, socket) do
    %{current_user: current_user, list_all: list_all, flop_meta: flop_meta} = socket.assigns

    if current_user.admin and not list_all do
      {:ok, {datasets, meta}} = Datasets.flop_datasets(flop_meta.flop)

      {:noreply,
       socket
       |> assign(list_all: true, flop_meta: meta)
       |> stream(:flop_datasets, datasets, reset: true)}
    else
      {:ok, {datasets, meta}} = Datasets.flop_own_datasets(current_user.id, flop_meta.flop)

      {:noreply,
       socket
       |> assign(list_all: false, flop_meta: meta)
       |> stream(:flop_datasets, datasets, reset: true)}
    end
  end

  def handle_event("update-filter", params, socket) do
    flop_params = Map.take(params, ["filters", "order_by", "order_directions", "page"])
    {:noreply, push_patch(socket, to: ~p"/datasets?#{flop_params}")}
  end

  def handle_event("reset-filter", _, socket) do
    flop =
      Flop.set_page(socket.assigns.flop_meta.flop, 1)
      |> Flop.reset_filters()

    path = Flop.Phoenix.build_path(~p"/datasets", flop, backend: socket.assigns.flop_meta.backend)
    {:noreply, push_patch(socket, to: path)}
  end
end
