defmodule EcoplatformWeb.UserLive.Index do
  use EcoplatformWeb, :live_view

  alias Ecoplatform.Accounts
  alias Ecoplatform.Accounts.User

  @impl true
  def mount(_params, _session, socket) do
    {:ok, stream(socket, :users, Accounts.list_users())}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit user")
    |> assign(:user, Accounts.get_user!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New user")
    |> assign(:user, %User{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing users")
    |> assign(:user, nil)
  end

  @impl true
  def handle_info({EcoplatformWeb.UserLive.FormComponent, {:saved, action, user}}, socket) do
    if action == :new do
      Accounts.deliver_new_user_instructions(
        user,
        &url(~p"/users/reset_password/#{&1}")
      )
    end

    {:noreply, stream_insert(socket, :users, user)}
  end

  # This function was added to avoid crashes in tests from receiving a message
  # from the Swoosh test adapter.
  if Mix.env() == :test do
    def handle_info({:email, _email}, socket) do
      {:noreply, socket}
    end
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    user = Accounts.get_user!(id)
    {:ok, _} = Accounts.delete_user(user)

    {:noreply, stream_delete(socket, :users, user)}
  end
end
