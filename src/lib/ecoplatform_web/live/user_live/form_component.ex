defmodule EcoplatformWeb.UserLive.FormComponent do
  use EcoplatformWeb, :live_component

  alias Ecoplatform.Accounts

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
      </.header>

      <.simple_form
        for={@form}
        id="user-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:email]} type="text" label="Email" phx-debounce="300" />
        <.input field={@form[:name]} type="text" label="Name" phx-debounce="300" />
        <.input field={@form[:admin]} type="checkbox" label="Administrator" />
        <:actions>
          <.button phx-disable-with="Saving...">Save User</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{user: user} = assigns, socket) do
    changeset = changeset(user)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  defp changeset(user, params \\ %{}) do
    data = Map.take(user, [:email, :name, :admin])
    types = %{email: :string, name: :string, admin: :boolean}

    {data, types}
    |> Ecto.Changeset.cast(params, Map.keys(types))
    |> Ecto.Changeset.validate_required([:email, :name])
    |> Ecto.Changeset.validate_format(:email, ~r/^[^\s]+@[^\s]+$/,
      message: "must have the @ sign and no spaces"
    )
  end

  @impl true
  def handle_event("validate", %{"user" => user_params}, socket) do
    changeset =
      changeset(socket.assigns.user, user_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"user" => user_params}, socket) do
    with changeset <- changeset(socket.assigns.user, user_params),
         {:ok, params} <- Ecto.Changeset.apply_action(changeset, :validate),
         {:ok, socket} <- save_user(socket, socket.assigns.action, params) do
      {:noreply, socket}
    else
      {:error, changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_user(socket, :edit, params) do
    case Accounts.update_user(socket.assigns.user, params.email, params.name, params.admin) do
      {:ok, user} ->
        notify_parent({:saved, :edit, user})

        {:ok,
         socket
         |> put_flash(:info, "User updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp save_user(socket, :new, params) do
    case Accounts.create_user(params.email, params.name, params.admin) do
      {:ok, user} ->
        notify_parent({:saved, :new, user})

        {:ok,
         socket
         |> put_flash(:info, "User created successfully")
         |> push_patch(to: socket.assigns.patch)}

      error ->
        error
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset, as: :user))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
