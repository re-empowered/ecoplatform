defmodule EcoplatformWeb.Layouts do
  @moduledoc false

  use EcoplatformWeb, :html

  embed_templates "layouts/*"
end
