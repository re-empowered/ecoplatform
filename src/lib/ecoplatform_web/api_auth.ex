defmodule EcoplatformWeb.APIAuth do
  @moduledoc """
  API authentication plugs and live_session hooks.
  """

  use EcoplatformWeb, :verified_routes

  import Plug.Conn
  import Phoenix.Controller

  alias Ecoplatform.Accounts
  alias Ecoplatform.Accounts.APICredentials
  alias Ecoplatform.Datasets

  def require_authenticated_provider(conn, _opts) do
    with [key | _] <- get_req_header(conn, "x-api-key"),
         [secret | _] <- get_req_header(conn, "x-api-secret"),
         %APICredentials{} = api_credentials <- get_api_credentials(key, secret) do
      provider = Datasets.get_provider!(api_credentials.provider_id)
      assign(conn, :current_provider, provider)
    else
      _ ->
        conn
        |> put_status(401)
        |> json(%{errors: %{detail: ["invalid API credentials"]}})
        |> halt()
    end
  end

  defp get_api_credentials(key, secret) do
    Accounts.get_api_credential_by_key_and_secret(key, secret)
  end
end
