defmodule EcoplatformWeb.DatasetController do
  use EcoplatformWeb, :controller

  alias Ecoplatform.Datasets
  alias Ecoplatform.Pipeline.Import

  plug :get_dataset
  plug :authorize_export when action in [:export]
  plug :authorize_import when action in [:import]

  action_fallback EcoplatformWeb.FallbackController

  def import(conn, params) do
    with %Plug.Upload{path: path} <- params["import"],
         :ok <- Import.from_csv(path, conn.assigns.dataset) do
      json(conn, 200)
    else
      {:error, {:invalid_line, {line_number, line}}} ->
        conn
        |> put_status(422)
        |> json(%{
          errors: %{
            import: ["invalid line ##{line_number}: #{line}"],
            meta: %{line_number: line_number}
          }
        })
        |> halt()

      _ ->
        conn
        |> put_status(422)
        |> json(%{errors: %{import: ["must be a csv file"]}})
        |> halt()
    end
  end

  def export(conn, params) do
    case validate_export_params(params) do
      {:ok, params} ->
        stream_export(conn, params)

      {:error, _changeset} = error ->
        error
    end
  end

  defp validate_export_params(params) do
    types = %{start_ts: :integer, end_ts: :integer, datastream_ids: :string}
    valid_unix_epoch = [greater_than_or_equal_to: 0, less_than: 32_503_680_000_000]

    changeset =
      {%{}, types}
      |> Ecto.Changeset.cast(params, Map.keys(types))
      |> Ecto.Changeset.validate_required([:start_ts, :end_ts])
      |> Ecto.Changeset.validate_number(:start_ts, valid_unix_epoch)
      |> Ecto.Changeset.validate_number(:end_ts, valid_unix_epoch)
      |> validate_datastream_ids()

    changeset =
      if changeset.valid? and
           Ecto.Changeset.get_change(changeset, :end_ts) <
             Ecto.Changeset.get_change(changeset, :start_ts) do
        Ecto.Changeset.add_error(changeset, :end_ts, "must be greater than or equal to start_ts")
      else
        changeset
      end

    Ecto.Changeset.apply_action(changeset, :validate)
  end

  defp validate_datastream_ids(changeset) do
    ids =
      changeset
      |> Ecto.Changeset.get_change(:datastream_ids, "")
      |> String.split(",", trim: true)
      |> Enum.map(fn id ->
        case Integer.parse(id) do
          {number, ""} -> number
          _ -> nil
        end
      end)

    cond do
      ids == [] ->
        changeset

      Enum.any?(ids, fn id -> id == nil end) ->
        Ecto.Changeset.add_error(changeset, :datastream_ids, "is invalid")

      true ->
        Ecto.Changeset.put_change(changeset, :datastream_ids, ids)
    end
  end

  defp stream_export(conn, params) do
    start_ts = DateTime.from_unix!(params.start_ts * 1000, :microsecond)
    end_ts = DateTime.from_unix!(params.end_ts * 1000, :microsecond)

    conn =
      conn
      |> put_resp_content_type("application/csv")
      |> put_resp_header("content-disposition", "attachment; filename=datapoints.csv")
      |> send_chunked(200)

    dataset = Ecoplatform.Repo.preload(conn.assigns.dataset, :datastreams)

    {:ok, conn} =
      Ecoplatform.Repo.transaction(fn ->
        dataset.datastreams
        |> maybe_filter_datastreams(params)
        |> Enum.sort_by(fn datastream -> datastream.id end)
        |> Enum.reduce_while(conn, fn datastream, conn ->
          stream_export_datapoints(conn, datastream, start_ts, end_ts)
        end)
      end)

    conn
  end

  defp maybe_filter_datastreams(datastreams, params) do
    if params[:datastream_ids] do
      Enum.filter(datastreams, fn datastream -> datastream.id in params.datastream_ids end)
    else
      datastreams
    end
  end

  defp stream_export_datapoints(conn, datastream, start_ts, end_ts) do
    result =
      Datasets.stream_datapoints(datastream.id, start_ts, end_ts)
      |> Stream.chunk_every(10_000)
      |> Enum.reduce_while(conn, fn datapoints, conn ->
        datapoints_csv =
          Enum.map(datapoints, fn datapoint ->
            ts = "#{DateTime.to_unix(datapoint.ts, :millisecond)}"
            value = Float.to_string(datapoint.value)
            [datastream.name, ",", ts, ",", value, "\r\n"]
          end)

        case chunk(conn, datapoints_csv) do
          {:ok, conn} -> {:cont, conn}
          {:error, _error} -> {:halt, {:error, conn}}
        end
      end)

    case result do
      {:error, conn} -> {:halt, conn}
      conn -> {:cont, conn}
    end
  end

  defp get_dataset(conn, _opts) do
    with %{"id" => dataset_id} when is_binary(dataset_id) <- conn.path_params,
         {dataset_id, _} <- Integer.parse(dataset_id) do
      dataset = Datasets.get_dataset!(dataset_id)
      assign(conn, :dataset, dataset)
    else
      _ ->
        conn
        |> put_status(404)
        |> json(%{errors: %{detail: ["dataset not found"]}})
        |> halt()
    end
  end

  defp authorize_export(conn, _opts) do
    case conn.assigns do
      %{current_provider: _} ->
        conn

      %{current_user: user} ->
        if Datasets.user_can_access?(conn.assigns.dataset, user) do
          conn
        else
          conn
          |> put_flash(:error, "You must be dataset owner to export data.")
          |> redirect(to: ~p"/")
          |> halt()
        end

      _ ->
        conn
        |> put_status(403)
        |> json(%{errors: %{detail: ["dataset access denied"]}})
        |> halt()
    end
  end

  defp authorize_import(conn, _opts) do
    if conn.assigns.current_provider.id == conn.assigns.dataset.provider_id do
      conn
    else
      conn
      |> put_status(403)
      |> json(%{errors: %{detail: ["dataset access denied"]}})
      |> halt()
    end
  end
end
