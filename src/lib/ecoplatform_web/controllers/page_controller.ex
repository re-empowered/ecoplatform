defmodule EcoplatformWeb.PageController do
  use EcoplatformWeb, :controller

  def home(conn, _params) do
    render(conn, :home)
  end
end
