defmodule EcoplatformWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use EcoplatformWeb, :controller

  # This clause handles errors returned by Ecto changesets.
  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    conn
    |> put_status(:unprocessable_entity)
    |> put_view(json: EcoplatformWeb.ChangesetJSON)
    |> render(:error, changeset: changeset)
  end
end
