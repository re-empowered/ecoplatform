defmodule EcoplatformWeb.PageHTML do
  use EcoplatformWeb, :html

  embed_templates "page_html/*"
end
