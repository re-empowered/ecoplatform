defmodule EcoplatformWeb.Router do
  use EcoplatformWeb, :router

  import EcoplatformWeb.APIAuth
  import EcoplatformWeb.UserAuth
  import EcoplatformWeb.Nav

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, html: {EcoplatformWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :require_authenticated_provider
  end

  scope "/", EcoplatformWeb do
    pipe_through [:browser, :set_current_path]

    get "/", PageController, :home
  end

  # Other scopes may use custom stacks.
  scope "/api/v1", EcoplatformWeb do
    pipe_through :api

    get "/datasets/:id/export", DatasetController, :export
    post "/datasets/:id/import", DatasetController, :import
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:ecoplatform, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: EcoplatformWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  scope "/admin/", EcoplatformWeb do
    pipe_through [:browser, :require_admin_user]

    live_session :require_admin_user,
      on_mount: [
        {EcoplatformWeb.UserAuth, :ensure_admin},
        {EcoplatformWeb.Nav, :set_current_path}
      ] do
      live "/users", UserLive.Index, :index
      live "/users/new", UserLive.Index, :new
      live "/users/:id/edit", UserLive.Index, :edit
      live "/users/:id", UserLive.Show, :show
      live "/users/:id/show/edit", UserLive.Show, :edit

      live "/providers", ProviderLive.Index, :index
      live "/providers/new", ProviderLive.Index, :new
      live "/providers/:id/edit", ProviderLive.Index, :edit
      live "/providers/:id", ProviderLive.Show, :show
      live "/providers/:id/show/edit", ProviderLive.Show, :edit
    end
  end

  ## Authentication routes

  scope "/", EcoplatformWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [
        {EcoplatformWeb.UserAuth, :redirect_if_user_is_authenticated},
        {EcoplatformWeb.Nav, :set_current_path}
      ] do
      live "/users/log_in", UserLoginLive, :new
      live "/users/reset_password", UserForgotPasswordLive, :new
      live "/users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", EcoplatformWeb do
    pipe_through [:browser, :require_authenticated_user]

    get "/datasets/:id/export", DatasetController, :export

    live_session :require_authenticated_user,
      on_mount: [
        {EcoplatformWeb.UserAuth, :ensure_authenticated},
        {EcoplatformWeb.Nav, :set_current_path}
      ] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email

      live "/datasets", DatasetLive.Index, :index
      live "/datasets/new", DatasetLive.Index, :new
      live "/datasets/:id/edit", DatasetLive.Index, :edit

      live "/datasets/:id", DatasetLive.Show, :show
      live "/datasets/:id/show/edit", DatasetLive.Show, :edit

      live "/datasets/:id/owners", DatasetLive.OwnerList, :index
      live "/datasets/:id/owners/new", DatasetLive.OwnerList, :new

      live "/datasets/:id/alarms", DatasetLive.Show.AlarmList, :index
      live "/datasets/:id/alarms/:alarm_summary_id", DatasetLive.Show.AlarmList, :show

      live "/datasets/:id/datastreams", DatasetLive.Show.DatastreamList, :index
      live "/datasets/:id/datastreams/new", DatasetLive.Show.DatastreamList, :new
      live "/datasets/:id/datastreams/export", DatasetLive.Show.DatastreamList, :export
      live "/datasets/:id/datastreams/:datastream_id", DatasetLive.Show.DatastreamList, :show
      live "/datasets/:id/datastreams/:datastream_id/edit", DatasetLive.Show.DatastreamList, :edit

      live "/datasets/:id/datastreams/:datastream_id/export",
           DatasetLive.Show.DatastreamList,
           :export
    end
  end

  scope "/", EcoplatformWeb do
    pipe_through [:browser]

    delete "/users/log_out", UserSessionController, :delete
  end
end
