defmodule EcoplatformWeb.Nav do
  @moduledoc """
  Plugs and live_session hooks for setting the current path
  """

  def on_mount(:set_current_path, _params, _session, socket) do
    {:cont,
     Phoenix.LiveView.attach_hook(
       socket,
       :set_current_path,
       :handle_params,
       &set_current_path/3
     )}
  end

  defp set_current_path(_params, url, socket) do
    current_path =
      URI.parse(url)
      |> Map.get(:path)

    {:cont, Phoenix.Component.assign(socket, :current_path, current_path)}
  end

  def set_current_path(conn, _opts) do
    Plug.Conn.assign(conn, :current_path, conn.request_path)
  end
end
