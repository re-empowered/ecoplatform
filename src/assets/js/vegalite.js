import vegaEmbed from "vega-embed";

const VegaLite = {
    mounted() {
        // This element is important so we can uniquely identify which element will be loaded
        this.props = { id: this.el.getAttribute("data-id") };
        // Handles the event of creating a graph and loads vegaEmbed targetting our main hook element
        this.handleEvent(`vega_lite:${this.props.id}:init`, ({ spec }) => {
            setTimeout(fn => {
              vegaEmbed(this.el, spec, {actions: false})
              .then((result) => result.view)
              .catch((error) => console.error(error));
            }, 0)
        });
    },
};

export default VegaLite;
