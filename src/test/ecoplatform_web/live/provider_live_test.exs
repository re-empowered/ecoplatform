defmodule EcoplatformWeb.ProviderLiveTest do
  use EcoplatformWeb.ConnCase

  import Phoenix.LiveViewTest
  import Ecoplatform.AccountsFixtures
  import Ecoplatform.DatasetsFixtures

  @create_attrs %{mqtt_namespace: "some-mqtt_namespace", name: "some name"}
  @update_attrs %{mqtt_namespace: "some-updated-mqtt_namespace", name: "some updated name"}
  @invalid_attrs %{mqtt_namespace: nil, name: nil}

  defp create_and_log_in_admin(%{conn: conn}) do
    user = user_fixture(admin: true)
    %{user: user, conn: log_in_user(conn, user)}
  end

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  describe "Authorization" do
    test "requires the user to be an administrator", %{conn: conn} do
      user = user_fixture()
      conn = log_in_user(conn, user)

      assert {:error, {:redirect, socket}} = live(conn, ~p"/admin/providers/")
      assert socket.flash["error"] == "You must be an administrator to access this page."
      assert socket.to == ~p"/"
    end
  end

  describe "Index" do
    setup [:create_and_log_in_admin, :create_provider]

    test "lists all providers", %{conn: conn, provider: provider} do
      {:ok, _index_live, html} = live(conn, ~p"/admin/providers")

      assert html =~ "Providers"
      assert html =~ provider.mqtt_namespace
    end

    test "saves new provider", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/providers")

      assert index_live |> element("a", "New Provider") |> render_click() =~
               "New Provider"

      assert_patch(index_live, ~p"/admin/providers/new")

      assert index_live
             |> form("#provider-form", provider: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#provider-form", provider: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/admin/providers")

      html = render(index_live)
      assert html =~ "Provider created successfully"
      assert html =~ "some-mqtt_namespace"
    end

    test "updates provider in listing", %{conn: conn, provider: provider} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/providers")

      assert index_live |> element("#providers-#{provider.id} a", "Edit") |> render_click() =~
               "Edit provider"

      assert_patch(index_live, ~p"/admin/providers/#{provider}/edit")

      assert index_live
             |> form("#provider-form", provider: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#provider-form", provider: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/admin/providers")

      html = render(index_live)
      assert html =~ "Provider updated successfully"
      assert html =~ "some-updated-mqtt_namespace"
    end

    test "deletes provider in listing", %{conn: conn, provider: provider} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/providers")

      assert index_live |> element("#providers-#{provider.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#providers-#{provider.id}")
    end
  end

  describe "Show" do
    setup [:create_and_log_in_admin, :create_provider]

    test "displays provider", %{conn: conn, provider: provider} do
      {:ok, _show_live, html} = live(conn, ~p"/admin/providers/#{provider}")

      assert html =~ "Show provider"
      assert html =~ provider.mqtt_namespace
    end

    test "updates provider within modal", %{conn: conn, provider: provider} do
      {:ok, show_live, _html} = live(conn, ~p"/admin/providers/#{provider}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit provider"

      assert_patch(show_live, ~p"/admin/providers/#{provider}/show/edit")

      assert show_live
             |> form("#provider-form", provider: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#provider-form", provider: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/admin/providers/#{provider}")

      html = render(show_live)
      assert html =~ "Provider updated successfully"
      assert html =~ "some-updated-mqtt_namespace"
    end
  end
end
