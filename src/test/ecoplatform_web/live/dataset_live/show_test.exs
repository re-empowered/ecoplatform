defmodule EcoplatformWeb.DatasetLive.ShowTest do
  use EcoplatformWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Ecoplatform.DatasetsFixtures
  import Ecoplatform.AccountsFixtures

  @update_attrs %{
    name: "some updated name",
    description: "some updated description",
    mqtt_subtopic: "some-updated-mqtt_subtopic"
  }
  @invalid_attrs %{name: nil, description: nil, mqtt_subtopic: nil}

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  defp create_dataset(context) do
    dataset = dataset_fixture(provider_id: context.provider.id)
    Ecoplatform.Datasets.add_dataset_owner(dataset, context.user.id)
    %{dataset: dataset}
  end

  defp create_and_log_in_user(%{conn: conn}) do
    user = user_fixture()
    %{user: user, conn: log_in_user(conn, user)}
  end

  setup [:create_and_log_in_user, :create_provider, :create_dataset]

  test "displays dataset", %{conn: conn, dataset: dataset} do
    {:ok, _show_live, html} = live(conn, ~p"/datasets/#{dataset}")

    assert html =~ "Show dataset"
    assert html =~ dataset.name
  end

  test "updates dataset within modal", %{conn: conn, dataset: dataset} do
    {:ok, show_live, _html} = live(conn, ~p"/datasets/#{dataset}")

    assert show_live |> element("a", "Edit") |> render_click() =~
             "Edit dataset"

    assert_patch(show_live, ~p"/datasets/#{dataset}/show/edit")

    assert show_live
           |> form("#dataset-form", dataset: @invalid_attrs)
           |> render_change() =~ "can&#39;t be blank"

    assert show_live
           |> form("#dataset-form", dataset: @update_attrs)
           |> render_submit()

    assert_patch(show_live, ~p"/datasets/#{dataset}")

    html = render(show_live)
    assert html =~ "Dataset updated successfully"
    assert html =~ "some updated name"
  end

  test "redirects if the user does not have access to dataset", %{conn: conn, provider: provider} do
    # Create a dataset that is not owned by the current user
    dataset = dataset_fixture(provider_id: provider.id)

    assert {:error, {:redirect, _}} = live(conn, ~p"/datasets/#{dataset}")
  end
end
