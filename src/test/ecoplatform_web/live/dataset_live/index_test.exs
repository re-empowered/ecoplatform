defmodule EcoplatformWeb.DatasetLive.IndexTest do
  use EcoplatformWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Ecoplatform.DatasetsFixtures
  import Ecoplatform.AccountsFixtures

  @create_attrs %{
    name: "some name",
    description: "some description",
    mqtt_subtopic: "some-mqtt_subtopic"
  }
  @update_attrs %{
    name: "some updated name",
    description: "some updated description",
    mqtt_subtopic: "some-updated-mqtt_subtopic"
  }
  @invalid_attrs %{name: nil, description: nil, mqtt_subtopic: nil}

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  defp create_dataset(context) do
    dataset = dataset_fixture(provider_id: context.provider.id)
    Ecoplatform.Datasets.add_dataset_owner(dataset, context.user.id)
    %{dataset: dataset}
  end

  defp create_and_log_in_user(%{conn: conn}) do
    user = user_fixture()
    %{user: user, conn: log_in_user(conn, user)}
  end

  defp create_and_log_in_admin(%{conn: conn}) do
    user = user_fixture(admin: true)
    %{user: user, conn: log_in_user(conn, user)}
  end

  describe "standard user" do
    setup [:create_and_log_in_user, :create_provider, :create_dataset]

    test "lists all datasets owned by the user", %{conn: conn, dataset: dataset} do
      {:ok, _index_live, html} = live(conn, ~p"/datasets")

      assert html =~ "Listing Datasets"
      assert html =~ dataset.name
    end

    test "does not list datasets owned by the another user", %{conn: conn, provider: provider} do
      # Create a dataset that is not owned by the current user
      dataset = dataset_fixture(provider_id: provider.id)

      {:ok, _index_live, html} = live(conn, ~p"/datasets")

      assert html =~ "Listing Datasets"
      refute html =~ dataset.name
    end

    test "can filter datasets", %{conn: conn, user: user, dataset: dataset} do
      extra_dataset = dataset_fixture(description: "another description")
      Ecoplatform.Datasets.add_dataset_owner(extra_dataset, user.id)

      {:ok, index_live, _html} = live(conn, ~p"/datasets")

      html =
        index_live
        |> form("#datasets-filter-form", %{"filters[1][value]" => dataset.name})
        |> render_change()

      assert html =~ dataset.description
      refute html =~ extra_dataset.description
    end

    test "cannot see button show all datasets", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets")
      toggle_button = element(index_live, ~s/button[phx-click="toggle-all"]/)

      refute has_element?(toggle_button)
    end

    test "saves new dataset", %{conn: conn, provider: provider} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets")

      assert index_live |> element("a", "New dataset") |> render_click() =~
               "New dataset"

      assert_patch(index_live, ~p"/datasets/new")

      assert index_live
             |> form("#dataset-form", dataset: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#dataset-form", dataset: Map.put(@create_attrs, :provider_id, provider.id))
             |> render_submit()

      assert_patch(index_live, ~p"/datasets")

      html = render(index_live)
      assert html =~ "Dataset created successfully"
      assert html =~ "some name"
    end

    test "updates dataset in listing", %{conn: conn, dataset: dataset} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets")

      assert index_live |> element("#flop_datasets-#{dataset.id} a", "Edit") |> render_click() =~
               "Edit dataset"

      assert_patch(index_live, ~p"/datasets/#{dataset}/edit")

      assert index_live
             |> form("#dataset-form", dataset: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#dataset-form", dataset: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/datasets")

      html = render(index_live)
      assert html =~ "Dataset updated successfully"
      assert html =~ "some updated name"
    end

    test "deletes dataset in listing", %{conn: conn, dataset: dataset} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets")

      assert index_live |> element("#flop_datasets-#{dataset.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#datasets-#{dataset.id}")
    end
  end

  describe "admin user" do
    setup [:create_and_log_in_admin, :create_provider, :create_dataset]

    test "can see button show all datasets", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets")
      toggle_button = element(index_live, ~s/button[phx-click="toggle-all"]/)

      assert has_element?(toggle_button)
    end

    test "lists datasets owned by other users if button is clicked", %{
      conn: conn,
      provider: provider
    } do
      # Create a dataset that is not owned by the current user
      dataset = dataset_fixture(provider_id: provider.id)

      {:ok, index_live, html} = live(conn, ~p"/datasets")
      toggle_button = element(index_live, ~s/button[phx-click="toggle-all"]/)
      refute html =~ dataset.name

      html = render_click(toggle_button)

      assert html =~ dataset.name
    end
  end
end
