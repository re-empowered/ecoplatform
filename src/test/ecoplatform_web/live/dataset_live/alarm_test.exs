defmodule EcoplatformWeb.DatasetLive.AlarmTest do
  use EcoplatformWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Ecoplatform.DatasetsFixtures
  import Ecoplatform.AccountsFixtures

  defp create_and_log_in_user(%{conn: conn}) do
    user = user_fixture()
    %{user: user, conn: log_in_user(conn, user)}
  end

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  defp create_dataset(context) do
    dataset = dataset_fixture(provider_id: context.provider.id)
    Ecoplatform.Datasets.add_dataset_owner(dataset, context.user.id)
    %{dataset: dataset}
  end

  defp create_datastream(context) do
    datastream = datastream_fixture(dataset_id: context.dataset.id)
    %{datastream: datastream}
  end

  defp create_value_alarms(context) do
    dt = DateTime.new!(Date.utc_today(), ~T[10:20:00.000000])

    value_alarms = [
      %{
        datastream_id: context.datastream.id,
        raised_at: dt,
        ts: DateTime.add(dt, -10, :second),
        value: context.datastream.max_value + 10
      },
      %{
        datastream_id: context.datastream.id,
        raised_at: DateTime.add(dt, -20),
        ts: DateTime.add(dt, -30, :second),
        value: context.datastream.min_value - 10
      }
    ]

    Ecoplatform.Datasets.insert_value_alarms(value_alarms)

    %{value_alarms: value_alarms}
  end

  setup [
    :create_and_log_in_user,
    :create_provider,
    :create_dataset,
    :create_datastream,
    :create_value_alarms
  ]

  test "lists value alarm summaries on dataset", %{conn: conn, dataset: dataset} = context do
    {:ok, _lv, html} = live(conn, ~p"/datasets/#{dataset}/alarms")
    assert html =~ context.datastream.name
  end

  test "can view value alarm details", %{conn: conn, dataset: dataset} = context do
    {:ok, lv, html} = live(conn, ~p"/datasets/#{dataset}/alarms")

    # Find the link that will be patched to when the user clicks an alarm summary row
    patch_to_details =
      Floki.parse_document!(html)
      |> Floki.find("tr [phx-click]")
      |> then(fn [{"td", [_, {"phx-click", when_clicked}], _} | _] ->
        [["patch", %{"href" => href}]] = Jason.decode!(when_clicked)
        href
      end)

    html = render_patch(lv, patch_to_details)
    assert html =~ "Value alarm details"
    assert html =~ context.datastream.name

    for value_alarm <- context.value_alarms do
      assert html =~ DateTime.to_iso8601(value_alarm.raised_at)
      assert html =~ DateTime.to_iso8601(value_alarm.ts)
    end
  end

  test "can subscribe to alarm notifications", %{conn: conn, dataset: dataset} do
    {:ok, lv, _html} = live(conn, ~p"/datasets/#{dataset}/alarms")

    lv
    |> element("button", "Subscribe to alarm notifications")
    |> render_click()

    flash = element(lv, "#flash", "Success!")
    assert has_element?(flash)

    unsubscribe_button = element(lv, "button", "Unsubscribe from alarm notifications")
    assert has_element?(unsubscribe_button)
  end

  test "can unsubscribe from alarm notifications", %{conn: conn, dataset: dataset, user: user} do
    Ecoplatform.Datasets.create_alarm_subscription(dataset, user)
    {:ok, lv, _html} = live(conn, ~p"/datasets/#{dataset}/alarms")

    lv
    |> element("button", "Unsubscribe from alarm notifications")
    |> render_click()

    flash = element(lv, "#flash", "Success!")
    assert has_element?(flash)

    subscribe_button = element(lv, "button", "Subscribe to alarm notifications")
    assert has_element?(subscribe_button)
  end
end
