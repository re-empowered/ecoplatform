defmodule EcoplatformWeb.DatasetLive.DatastreamTest do
  use EcoplatformWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import Ecoplatform.DatasetsFixtures
  import Ecoplatform.AccountsFixtures

  @create_attrs %{
    name: "some-name",
    description: "some description",
    min_value: 1.0,
    max_value: 2.0
  }
  @update_attrs %{
    name: "some-updated-name",
    description: "some updated description",
    min_value: 1.0,
    max_value: 2.0
  }
  @invalid_attrs %{
    name: nil,
    description: nil,
    min_value: nil,
    max_value: nil
  }

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  defp create_dataset(context) do
    dataset = dataset_fixture(provider_id: context.provider.id)
    Ecoplatform.Datasets.add_dataset_owner(dataset, context.user.id)
    %{dataset: dataset}
  end

  defp create_datastream(context) do
    datastream = datastream_fixture(dataset_id: context.dataset.id)
    %{datastream: datastream}
  end

  defp create_and_log_in_user(%{conn: conn}) do
    user = user_fixture()
    %{user: user, conn: log_in_user(conn, user)}
  end

  describe "standard user" do
    setup [:create_and_log_in_user, :create_provider, :create_dataset, :create_datastream]

    test "lists all datastreams on dataset", %{conn: conn, dataset: dataset} do
      {:ok, _index_live, html} = live(conn, ~p"/datasets/#{dataset}/datastreams")

      assert html =~ "Datastreams"
      assert html =~ dataset.name
    end

    test "does not list datastreams on other datasets", %{conn: conn, dataset: dataset} do
      # Create a datastream that is not on the current dataset
      datastream = datastream_fixture()

      {:ok, _index_live, html} = live(conn, ~p"/datasets/#{dataset}/datastreams")

      assert html =~ "Datastreams"
      refute html =~ datastream.name
    end

    test "can filter datastreams", %{conn: conn, datastream: datastream} do
      extra_datastream = datastream_fixture(description: "another description")

      {:ok, index_live, _html} = live(conn, ~p"/datasets/#{datastream.dataset_id}/datastreams")

      html =
        index_live
        |> form("#datastreams-filter-form", %{"filters[0][value]" => datastream.name})
        |> render_change()

      assert html =~ datastream.description
      refute html =~ extra_datastream.description
    end

    test "saves new datastream", %{conn: conn, dataset: dataset} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets/#{dataset}/datastreams")

      assert index_live |> element("a#new-datastream") |> render_click() =~
               "New datastream"

      assert_patch(index_live, ~p"/datasets/#{dataset}/datastreams/new")

      assert index_live
             |> form("#datastream-form", new_datastream: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#datastream-form", new_datastream: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/datasets/#{dataset}/datastreams")

      html = render(index_live)
      assert html =~ "Datastream created successfully"
      assert html =~ "some-name"
    end

    test "updates dataset in listing", %{conn: conn, datastream: datastream} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets/#{datastream.dataset_id}/datastreams")

      assert index_live
             |> element("#flop_datastreams-#{datastream.id} a", "Edit")
             |> render_click() =~ "Edit datastream"

      assert_patch(
        index_live,
        ~p"/datasets/#{datastream.dataset_id}/datastreams/#{datastream.id}/edit"
      )

      assert index_live
             |> form("#datastream-form", new_datastream: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#datastream-form", new_datastream: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/datasets/#{datastream.dataset_id}/datastreams")

      html = render(index_live)
      assert html =~ "Datastream updated successfully"
      assert html =~ "some-updated-name"
    end

    test "deletes dataset in listing", %{conn: conn, datastream: datastream} do
      {:ok, index_live, _html} = live(conn, ~p"/datasets/#{datastream.dataset_id}/datastreams")

      assert index_live
             |> element("#flop_datastreams-#{datastream.id} a", "Delete")
             |> render_click()

      refute has_element?(index_live, "#datastreams-#{datastream.id}")
    end
  end
end
