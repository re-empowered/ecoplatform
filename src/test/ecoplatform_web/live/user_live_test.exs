defmodule EcoplatformWeb.UserLiveTest do
  use EcoplatformWeb.ConnCase

  import Phoenix.LiveViewTest
  import Ecoplatform.AccountsFixtures

  @create_attrs %{email: "valid-email@example.com", name: "Valid Name", admin: false}
  @update_attrs %{email: "new-valid-email@example.com", name: "New Valid Name", admin: false}
  @invalid_attrs %{email: nil, name: nil}

  defp create_and_log_in_admin(%{conn: conn}) do
    user = user_fixture(admin: true)
    %{user: user, conn: log_in_user(conn, user)}
  end

  describe "Authorization" do
    test "requires the user to be an administrator", %{conn: conn} do
      user = user_fixture()
      conn = log_in_user(conn, user)

      assert {:error, {:redirect, socket}} = live(conn, ~p"/admin/users/")
      assert socket.flash["error"] == "You must be an administrator to access this page."
      assert socket.to == ~p"/"
    end
  end

  describe "Index" do
    setup [:create_and_log_in_admin]

    test "lists all users", %{conn: conn} do
      {:ok, _index_live, html} = live(conn, ~p"/admin/users")

      assert html =~ "Users"
    end

    test "saves new user and sends password reset email", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/users")

      assert index_live |> element("a", "New user") |> render_click() =~
               "New user"

      assert_patch(index_live, ~p"/admin/users/new")

      assert index_live
             |> form("#user-form", user: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#user-form", user: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/admin/users")

      html = render(index_live)
      assert html =~ "User created successfully"

      user = Ecoplatform.Accounts.get_user_by_email(@create_attrs.email)

      assert Ecoplatform.Repo.get_by!(Ecoplatform.Accounts.UserToken, user_id: user.id).context ==
               "reset_password"
    end

    test "updates user in listing", %{conn: conn, user: user} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/users")

      assert index_live |> element("#users-#{user.id} a", "Edit") |> render_click() =~
               "Edit user"

      assert_patch(index_live, ~p"/admin/users/#{user}/edit")

      assert index_live
             |> form("#user-form", user: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#user-form", user: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/admin/users")

      html = render(index_live)
      assert html =~ "User updated successfully"
    end

    test "deletes user in listing", %{conn: conn, user: user} do
      {:ok, index_live, _html} = live(conn, ~p"/admin/users")

      assert index_live |> element("#users-#{user.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#users-#{user.id}")
    end
  end

  describe "Show" do
    setup [:create_and_log_in_admin]

    test "displays user", %{conn: conn, user: user} do
      {:ok, _show_live, html} = live(conn, ~p"/admin/users/#{user}")

      assert html =~ "Show user"
    end

    test "updates user within modal", %{conn: conn, user: user} do
      {:ok, show_live, _html} = live(conn, ~p"/admin/users/#{user}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit user"

      assert_patch(show_live, ~p"/admin/users/#{user}/show/edit")

      assert show_live
             |> form("#user-form", user: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#user-form", user: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/admin/users/#{user}")

      html = render(show_live)
      assert html =~ "User updated successfully"
    end
  end
end
