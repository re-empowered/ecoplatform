defmodule EcoplatformWeb.DatasetControllerTest do
  use EcoplatformWeb.ConnCase, async: true

  import Ecoplatform.AccountsFixtures
  import Ecoplatform.DatasetsFixtures

  describe "auth" do
    setup [:create_provider, :create_dataset]

    test "import returns a 401 response when no credentials are provided", context do
      conn = post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import")
      assert json_response(conn, 401)
    end

    test "import returns a 403 response when credentials do not match dataset provider",
         context do
      provider = provider_fixture()
      api_credentials = api_credentials_fixture(provider_id: provider.id)

      conn =
        context.conn
        |> put_req_header("x-api-key", api_credentials.key)
        |> put_req_header("x-api-secret", api_credentials.secret)

      conn = post(conn, ~p"/api/v1/datasets/#{context.dataset}/import")
      assert json_response(conn, 403)
    end

    test "export returns a 401 response when no credentials or session cookie is provided",
         context do
      conn = get(context.conn, ~p"/api/v1/datasets/#{context.dataset}/export")
      assert json_response(conn, 401)
    end

    test "export checks session cookie for user if no credentials are provided", context do
      user = user_fixture()
      conn = log_in_user(context.conn, user)

      # NOTE: We test against the non-API path here so we can make the request
      # without API credentials
      conn = get(conn, ~p"/datasets/#{context.dataset}/export")
      assert redirected_to(conn) == ~p"/"
    end
  end

  describe "import" do
    setup [
      :create_provider,
      :create_api_credentials,
      :create_dataset,
      :create_datastream,
      :set_auth_headers
    ]

    test "publishes imported data to pipeline", context do
      import_file = Briefly.create!()

      data =
        "#{context.datastream.name},1686224097000,231.3423\r\n" <>
          "#{context.datastream.name},1686224098000,231.3423\r\n" <>
          "#{context.datastream.name},1686224099000,231.3423\r\n"

      File.write!(import_file, data)

      conn =
        post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import", %{
          "import" => %Plug.Upload{path: import_file}
        })

      assert json_response(conn, 200)
      assert_receive {:publish, payload}
      expected_datastream_id = context.datastream.id

      assert [%{"datastream_id" => ^expected_datastream_id} | _] =
               datapoints = Jason.decode!(payload)

      assert length(datapoints) == 3
    end

    test "publishes data in batches", context do
      import_file = Briefly.create!()

      data =
        Stream.unfold(1_686_224_097_000, fn ts ->
          value = :rand.uniform()
          line = "#{context.datastream.name},#{ts},#{value}\r\n"
          {line, ts + 1000}
        end)
        |> Enum.take(1550)

      File.write!(import_file, data)

      conn =
        post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import", %{
          "import" => %Plug.Upload{path: import_file}
        })

      assert json_response(conn, 200)
      assert_receive {:publish, payload}
      assert length(Jason.decode!(payload)) == 500
      assert_receive {:publish, payload}
      assert length(Jason.decode!(payload)) == 500
      assert_receive {:publish, payload}
      assert length(Jason.decode!(payload)) == 500
      assert_receive {:publish, payload}
      assert length(Jason.decode!(payload)) == 50
    end

    test "returns error when csv data has invalid data", context do
      import_file = Briefly.create!()

      data =
        "#{context.datastream.name},1686224097000,231.3423\r\n" <>
          "#{context.datastream.name},1686224098000,231.3423\r\n" <>
          "#{context.datastream.name},1686224099000,not-a-float\r\n"

      File.write!(import_file, data)

      conn =
        post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import", %{
          "import" => %Plug.Upload{path: import_file}
        })

      # Assert that the first two items were properly imported
      assert_receive {:publish, payload}
      assert length(Jason.decode!(payload)) == 2

      assert %{
               "errors" => %{
                 "import" => ["invalid line #3:" <> _],
                 "meta" => %{"line_number" => 3}
               }
             } = json_response(conn, 422)
    end

    test "returns error if datastream cannot be found from name", context do
      import_file = Briefly.create!()

      File.write!(
        import_file,
        "datastream-name-that-does-not-exist,1686224099000,231.3423"
      )

      conn =
        post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import", %{
          "import" => %Plug.Upload{path: import_file}
        })

      assert %{
               "errors" => %{
                 "import" => ["invalid line #1:" <> _],
                 "meta" => %{"line_number" => 1}
               }
             } = json_response(conn, 422)
    end

    test "returns error when no file is uploaded", context do
      conn = post(context.conn, ~p"/api/v1/datasets/#{context.dataset}/import")
      assert %{"errors" => %{"import" => ["must be a csv file"]}} = json_response(conn, 422)
    end
  end

  describe "export" do
    setup [
      :create_provider,
      :create_api_credentials,
      :create_dataset,
      :create_datastream,
      :set_auth_headers
    ]

    test "returns csv data", %{conn: conn, dataset: dataset, datastream: datastream} do
      other_datastream = datastream_fixture(dataset_id: dataset.id)

      Ecoplatform.Datasets.insert_datapoints([
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_098_000 * 1000, :microsecond),
          value: 230.0
        },
        %{
          datastream_id: other_datastream.id,
          ts: DateTime.from_unix!(1_686_224_098_000 * 1000, :microsecond),
          value: 100.0
        },
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_099_000 * 1000, :microsecond),
          value: 231.0
        },
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_100_000 * 1000, :microsecond),
          value: 232.0
        }
      ])

      params = %{start_ts: 1_686_224_098_000, end_ts: 1_686_224_100_000}

      conn = get(conn, ~p"/api/v1/datasets/#{dataset.id}/export?#{params}")

      assert get_resp_header(conn, "content-type") == ["application/csv; charset=utf-8"]

      assert get_resp_header(conn, "content-disposition") == [
               "attachment; filename=datapoints.csv"
             ]

      assert response(conn, 200) ==
               "#{datastream.name},1686224098000,230.0\r\n" <>
                 "#{datastream.name},1686224099000,231.0\r\n" <>
                 "#{datastream.name},1686224100000,232.0\r\n" <>
                 "#{other_datastream.name},1686224098000,100.0\r\n"
    end

    test "can filter by start_ts and end_ts", %{
      conn: conn,
      dataset: dataset,
      datastream: datastream
    } do
      Ecoplatform.Datasets.insert_datapoints([
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_098_000 * 1000, :microsecond),
          value: 230.0
        },
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_099_000 * 1000, :microsecond),
          value: 231.0
        },
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_100_000 * 1000, :microsecond),
          value: 232.0
        }
      ])

      params = %{start_ts: 1_686_224_099_000, end_ts: 1_686_224_100_000}

      conn = get(conn, ~p"/api/v1/datasets/#{dataset.id}/export?#{params}")

      assert get_resp_header(conn, "content-type") == ["application/csv; charset=utf-8"]

      assert get_resp_header(conn, "content-disposition") == [
               "attachment; filename=datapoints.csv"
             ]

      assert response(conn, 200) ==
               "#{datastream.name},1686224099000,231.0\r\n" <>
                 "#{datastream.name},1686224100000,232.0\r\n"
    end

    test "can filter by datastream_id", %{conn: conn, dataset: dataset, datastream: datastream} do
      other_datastream = datastream_fixture(dataset_id: dataset.id)

      Ecoplatform.Datasets.insert_datapoints([
        %{
          datastream_id: datastream.id,
          ts: DateTime.from_unix!(1_686_224_098_000 * 1000, :microsecond),
          value: 230.0
        },
        %{
          datastream_id: other_datastream.id,
          ts: DateTime.from_unix!(1_686_224_098_000 * 1000, :microsecond),
          value: 231.0
        }
      ])

      params = %{
        start_ts: 1_686_224_098_000,
        end_ts: 1_686_224_100_000,
        datastream_ids: "#{other_datastream.id}"
      }

      conn = get(conn, ~p"/api/v1/datasets/#{dataset.id}/export?#{params}")
      assert response(conn, 200) == "#{other_datastream.name},1686224098000,231.0\r\n"
      assert get_resp_header(conn, "content-type") == ["application/csv; charset=utf-8"]

      assert get_resp_header(conn, "content-disposition") == [
               "attachment; filename=datapoints.csv"
             ]
    end
  end

  defp create_provider(_) do
    provider = provider_fixture()
    %{provider: provider}
  end

  defp create_api_credentials(%{provider: provider}) do
    api_credentials = api_credentials_fixture(provider_id: provider.id)
    %{api_credentials: api_credentials}
  end

  defp create_dataset(context) do
    dataset = dataset_fixture(provider_id: context.provider.id)
    %{dataset: dataset}
  end

  defp create_datastream(context) do
    datastream = datastream_fixture(dataset_id: context.dataset.id)
    %{datastream: datastream}
  end

  defp set_auth_headers(context) do
    conn =
      context.conn
      |> put_req_header("x-api-key", context.api_credentials.key)
      |> put_req_header("x-api-secret", context.api_credentials.secret)

    %{conn: conn}
  end
end
