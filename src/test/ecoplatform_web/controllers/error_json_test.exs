defmodule EcoplatformWeb.ErrorJSONTest do
  use EcoplatformWeb.ConnCase, async: true

  test "renders 404" do
    assert EcoplatformWeb.ErrorJSON.render("404.json", %{}) == %{errors: %{detail: "Not Found"}}
  end

  test "renders 500" do
    assert EcoplatformWeb.ErrorJSON.render("500.json", %{}) ==
             %{errors: %{detail: "Internal Server Error"}}
  end
end
