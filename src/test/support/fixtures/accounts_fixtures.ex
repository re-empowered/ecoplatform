defmodule Ecoplatform.AccountsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Ecoplatform.Accounts` context.
  """

  def unique_user_email, do: "user#{System.unique_integer()}@example.com"
  def valid_user_name, do: "user#{System.unique_integer()}"
  def valid_user_password, do: "hello world!"
  def valid_api_credentials_name, do: "api-credentials#{System.unique_integer()}"

  def valid_user_attributes(attrs \\ %{}) do
    Enum.into(attrs, %{
      email: unique_user_email(),
      name: valid_user_name()
    })
  end

  def user_fixture(attrs \\ %{}) do
    email = attrs[:email] || unique_user_email()
    name = attrs[:name] || valid_user_name()
    admin = attrs[:admin] || false
    {:ok, user} = Ecoplatform.Accounts.create_user(email, name, admin)

    if attrs[:set_password?] != false do
      password = attrs[:password] || valid_user_password()

      {:ok, user} =
        Ecoplatform.Accounts.reset_user_password(user, %{
          password: password,
          password_confirmation: password
        })

      user
    else
      user
    end
  end

  def api_credentials_fixture(attrs \\ %{}) do
    provider_id = attrs[:provider_id] || raise "missing provider_id"
    name = attrs[:name] || valid_api_credentials_name()
    key_prefix = "P" <> String.pad_leading("#{provider_id}", 3, "0")

    {:ok, api_credentials} =
      Ecoplatform.Accounts.create_api_credentials(provider_id, name, key_prefix)

    api_credentials
  end

  def extract_user_token(fun) do
    {:ok, captured_email} = fun.(&"[TOKEN]#{&1}[TOKEN]")
    [_, token | _] = String.split(captured_email.text_body, "[TOKEN]")
    token
  end
end
