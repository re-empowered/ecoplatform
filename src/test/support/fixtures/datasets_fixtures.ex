defmodule Ecoplatform.DatasetsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Ecoplatform.Datasets` context.
  """

  def unique_provider_name, do: "provider#{System.unique_integer()}"
  def unique_mqtt_namespace, do: "mqtt-namespace#{System.unique_integer()}"
  def unique_dataset_name, do: "dataset#{System.unique_integer()}"
  def unique_mqtt_subtopic, do: "mqtt-subtopic#{System.unique_integer()}"
  def unique_datastream_name, do: "datastream#{System.unique_integer()}"

  @doc """
  Generate a provider.
  """
  def provider_fixture(attrs \\ %{}) do
    name = attrs[:name] || unique_provider_name()
    mqtt_namespace = attrs[:mqtt_namespace] || unique_mqtt_namespace()

    {:ok, provider} = Ecoplatform.Datasets.create_provider(name, mqtt_namespace)

    provider
  end

  @doc """
  Generate a dataset.
  """
  def dataset_fixture(attrs \\ %{}) do
    provider_id = attrs[:provider_id] || provider_fixture().id
    name = attrs[:name] || unique_dataset_name()
    description = attrs[:description] || "some description"
    mqtt_subtopic = attrs[:mqtt_subtopic] || unique_mqtt_namespace()

    {:ok, dataset} =
      Ecoplatform.Datasets.create_dataset(provider_id, name, description, mqtt_subtopic)

    dataset
  end

  @doc """
  Generate a datastream.
  """
  def datastream_fixture(attrs \\ %{}) do
    dataset_id = attrs[:dataset_id] || dataset_fixture().id
    name = attrs[:name] || unique_datastream_name()
    description = attrs[:description] || "some description"
    min_value = attrs[:min_value] || 1.0
    max_value = attrs[:max_value] || 2.0
    max_silence_seconds = attrs[:max_value] || 600

    {:ok, datastream} =
      Ecoplatform.Datasets.create_datastream(
        dataset_id,
        name,
        %{
          description: description,
          min_value: min_value,
          max_value: max_value,
          max_silence_seconds: max_silence_seconds
        }
      )

    datastream
  end
end
