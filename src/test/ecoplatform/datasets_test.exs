defmodule Ecoplatform.DatasetsTest do
  use Ecoplatform.DataCase, async: false

  import Ecoplatform.DatasetsFixtures

  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Dataset
  alias Ecoplatform.Datasets.Datastream
  alias Ecoplatform.Datasets.Datapoint
  alias Ecoplatform.Datasets.Provider

  describe "providers" do
    @invalid_attrs %{mqtt_namespace: nil, name: nil}

    test "list_providers/0 returns all providers" do
      provider = provider_fixture()
      assert Datasets.list_providers() == [provider]
    end

    test "get_provider!/1 returns the provider with given id" do
      provider = provider_fixture()
      assert Datasets.get_provider!(provider.id) == provider
    end

    test "create_provider/2 with valid data creates a provider" do
      valid_name = "some name"
      valid_mqtt_namespace = "some-mqtt_namespace"

      assert {:ok, %Provider{} = provider} =
               Datasets.create_provider(valid_name, valid_mqtt_namespace)

      assert provider.mqtt_namespace == "some-mqtt_namespace"
      assert provider.name == "some name"
    end

    test "create_provider/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Datasets.create_provider(@invalid_attrs.name, @invalid_attrs.mqtt_namespace)
    end

    test "update_provider/2 with valid data updates the provider" do
      provider = provider_fixture()
      updated_mqtt_namespace = "some-updated-mqtt_namespace"
      updated_name = "some updated name"

      assert {:ok, %Provider{} = provider} =
               Datasets.update_provider(provider, updated_name, updated_mqtt_namespace)

      assert provider.name == updated_name
      assert provider.mqtt_namespace == updated_mqtt_namespace
    end

    test "update_provider/2 with invalid data returns error changeset" do
      provider = provider_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Datasets.update_provider(
                 provider,
                 @invalid_attrs.name,
                 @invalid_attrs.mqtt_namespace
               )

      assert provider == Datasets.get_provider!(provider.id)
    end

    test "delete_provider/1 deletes the provider" do
      provider = provider_fixture()
      assert {:ok, %Provider{}} = Datasets.delete_provider(provider)
      assert_raise Ecto.NoResultsError, fn -> Datasets.get_provider!(provider.id) end
    end
  end

  describe "datasets" do
    @invalid_attrs %{provider_id: nil, name: nil, description: nil, mqtt_subtopic: nil}

    test "list_datasets/0 returns all datasets" do
      dataset = dataset_fixture()
      dataset_id = dataset.id
      assert [%Dataset{id: ^dataset_id}] = Datasets.list_datasets()
    end

    test "get_dataset!/1 returns the dataset with given id" do
      expected_dataset = dataset_fixture()
      dataset = Datasets.get_dataset!(expected_dataset.id)
      assert dataset.id == dataset.id
      assert dataset.name == dataset.name
      assert dataset.description == dataset.description
    end

    test "create_dataset/1 with valid data creates a dataset" do
      provider = provider_fixture()

      assert {:ok, %Dataset{} = dataset} =
               Datasets.create_dataset(
                 provider.id,
                 "some name",
                 "some description",
                 "some-mqtt_subtopic"
               )

      assert dataset.description == "some description"
      assert dataset.mqtt_subtopic == "some-mqtt_subtopic"
      assert dataset.name == "some name"
    end

    test "create_dataset/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Datasets.create_dataset(
                 @invalid_attrs.provider_id,
                 @invalid_attrs.name,
                 @invalid_attrs.description,
                 @invalid_attrs.mqtt_subtopic
               )
    end

    test "update_dataset/2 with valid data updates the dataset" do
      dataset = dataset_fixture()

      assert {:ok, %Dataset{} = dataset} =
               Datasets.update_dataset(
                 dataset,
                 dataset.provider_id,
                 "some updated name",
                 "some updated description",
                 "some-updated-mqtt_subtopic"
               )

      assert dataset.description == "some updated description"
      assert dataset.mqtt_subtopic == "some-updated-mqtt_subtopic"
      assert dataset.name == "some updated name"
    end

    test "update_dataset/2 with invalid data returns error changeset" do
      dataset = dataset_fixture()

      assert {:error, %Ecto.Changeset{}} =
               Datasets.update_dataset(
                 dataset,
                 @invalid_attrs.provider_id,
                 @invalid_attrs.name,
                 @invalid_attrs.description,
                 @invalid_attrs.mqtt_subtopic
               )

      dataset_id = dataset.id
      assert %Dataset{id: ^dataset_id} = Datasets.get_dataset!(dataset.id)
    end

    test "delete_dataset/1 deletes the dataset" do
      dataset = dataset_fixture()
      assert {:ok, %Dataset{}} = Datasets.delete_dataset(dataset)
      assert_raise Ecto.NoResultsError, fn -> Datasets.get_dataset!(dataset.id) end
    end
  end

  describe "dataset ownership" do
    import Ecoplatform.AccountsFixtures

    test "can add a user as owner" do
      dataset = dataset_fixture()
      user = user_fixture()
      assert {:ok, _} = Datasets.add_dataset_owner(dataset, user.id)
    end

    test "cannot add owner if user does not exist" do
      dataset = dataset_fixture()
      assert {:error, %Ecto.Changeset{}} = Datasets.add_dataset_owner(dataset, -1)
    end

    test "can list datasets owned by user" do
      dataset = dataset_fixture()
      user = user_fixture()
      {:ok, _} = Datasets.add_dataset_owner(dataset, user.id)

      assert [dataset] == Datasets.list_own_datasets(user.id)
    end

    test "can remove an owner" do
      dataset = dataset_fixture()
      user = user_fixture()
      {:ok, _} = Datasets.add_dataset_owner(dataset, user.id)

      assert :ok = Datasets.remove_dataset_owner(dataset, user.id)
      assert [] == Datasets.list_own_datasets(user.id)
    end

    test "can check if user has access to dataset" do
      dataset = dataset_fixture()
      not_owner = user_fixture()
      owner = user_fixture()
      admin = user_fixture(admin: true)
      {:ok, _} = Datasets.add_dataset_owner(dataset, owner.id)

      refute Datasets.user_can_access?(dataset, not_owner)
      assert Datasets.user_can_access?(dataset, owner)
      assert Datasets.user_can_access?(dataset, admin)
    end
  end

  describe "datastreams" do
    @invalid_attrs %{
      dataset_id: nil,
      name: nil,
      description: nil,
      min_value: nil,
      max_value: nil,
      max_silence_seconds: nil
    }

    test "list_datastreams/0 returns all datastreams" do
      datastream = datastream_fixture()
      datastream_id = datastream.id
      assert [%Datastream{id: ^datastream_id}] = Datasets.list_datastreams()
    end

    test "get_datastream!/1 returns the datastream with given id" do
      expected_datastream = datastream_fixture()
      datastream = Datasets.get_datastream!(expected_datastream.id)
      assert expected_datastream.id == datastream.id
      assert expected_datastream.name == datastream.name
      assert expected_datastream.description == datastream.description
    end

    test "create_datastream/1 with valid data creates a datastream" do
      dataset = dataset_fixture()

      assert {:ok, %Datastream{} = datastream} =
               Datasets.create_datastream(
                 dataset.id,
                 "some-name",
                 %{
                   description: "some description",
                   min_value: 1.0,
                   max_value: 2.0,
                   max_silence_seconds: 600
                 }
               )

      assert datastream.name == "some-name"
      assert datastream.description == "some description"
    end

    test "create_datastream/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} =
               Datasets.create_datastream(
                 @invalid_attrs.dataset_id,
                 @invalid_attrs.name,
                 %{
                   description: @invalid_attrs.description,
                   min_value: @invalid_attrs.min_value,
                   max_value: @invalid_attrs.max_value,
                   max_silence_seconds: @invalid_attrs.max_silence_seconds
                 }
               )
    end

    test "update_datastream/2 with valid data updates the dataset" do
      datastream = datastream_fixture()

      assert {:ok, %Datastream{} = datastream} =
               Datasets.update_datastream(
                 datastream,
                 datastream.dataset_id,
                 "some-updated-name",
                 %{
                   description: "some updated description",
                   min_value: 1.0,
                   max_value: 2.0,
                   max_silence_seconds: 300
                 }
               )

      assert datastream.name == "some-updated-name"
      assert datastream.description == "some updated description"
    end

    test "datastream names must be unique within dataset" do
      dataset = dataset_fixture()

      params = %{
        description: "description1",
        min_value: 1.0,
        max_value: 2.0,
        max_silence_seconds: 600
      }

      {:ok, _datastream} = Datasets.create_datastream(dataset.id, "same-name", params)

      assert {:error, %Ecto.Changeset{}} =
               Datasets.create_datastream(dataset.id, "same-name", params)
    end

    test "delete_datastream/1 deletes the datastream" do
      datastream = datastream_fixture()
      assert {:ok, %Datastream{}} = Datasets.delete_datastream(datastream)
      assert_raise Ecto.NoResultsError, fn -> Datasets.get_datastream!(datastream.id) end
    end
  end

  describe "datapoints" do
    test "insert_datapoints/1 inserts a batch of datapoints" do
      datastream = datastream_fixture()

      max_datapoints_per_insert = trunc(65_535 / 3)

      datapoints =
        Stream.repeatedly(fn ->
          %{datastream_id: datastream.id, ts: ~U[2020-01-01 13:00:00.000000Z], value: 13.0}
        end)
        |> Stream.with_index()
        |> Stream.map(fn {d, i} -> %{d | ts: DateTime.add(d.ts, i)} end)
        |> Enum.take(max_datapoints_per_insert)

      assert :ok = Datasets.insert_datapoints(datapoints)
      assert Repo.aggregate(Datapoint, :count) == max_datapoints_per_insert
    end

    test "stream_datapoints/3 returns a stream with the matching datapoints" do
      datastream = datastream_fixture()
      other_datastream = datastream_fixture()

      expected_datapoints = [
        %{datastream_id: datastream.id, ts: ~U[2020-01-01 13:00:00.000000Z], value: 13.0},
        %{datastream_id: datastream.id, ts: ~U[2020-01-01 14:00:00.000000Z], value: 14.0},
        %{datastream_id: datastream.id, ts: ~U[2020-01-01 15:00:00.000000Z], value: 15.0},
        %{datastream_id: datastream.id, ts: ~U[2020-01-01 16:00:00.000000Z], value: 16.0}
      ]

      other_datastream_datapoints =
        Enum.map(expected_datapoints, fn d -> %{d | datastream_id: other_datastream.id} end)

      datapoints_outside_time_range =
        Enum.map(expected_datapoints, fn d -> %{d | ts: DateTime.add(d.ts, 30, :day)} end)

      :ok =
        Datasets.insert_datapoints(
          expected_datapoints ++ other_datastream_datapoints ++ datapoints_outside_time_range
        )

      assert {:ok, actual_datapoints} =
               Repo.transaction(fn ->
                 Datasets.stream_datapoints(
                   datastream.id,
                   ~U[2020-01-01 13:00:00.000000Z],
                   ~U[2020-01-01 16:00:00.000000Z]
                 )
                 |> Enum.to_list()
               end)

      comparable_actual_datapoints =
        Enum.map(actual_datapoints, fn d -> Map.take(d, [:datastream_id, :ts, :value]) end)

      assert comparable_actual_datapoints == expected_datapoints
    end
  end
end
