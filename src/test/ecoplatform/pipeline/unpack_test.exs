defmodule Ecoplatform.Pipeline.UnpackTest do
  use Ecoplatform.DataCase, async: true

  setup context do
    broadway_name = String.to_atom("#{__MODULE__}.#{context.test}")

    test_pid = self()

    {Ecoplatform.Pipeline.Unpack,
     name: broadway_name,
     producer_module: Broadway.DummyProducer,
     dataset_id_fn: &dataset_id/1,
     datastream_id_fn: &datastream_id/2,
     publish_fn: fn payload -> send(test_pid, {:publish, payload}) end}
    |> Supervisor.child_spec(id: broadway_name)
    |> start_supervised!()

    %{broadway: broadway_name}
  end

  defp dataset_id(routing_key) do
    case routing_key do
      "provider1.dataset1" -> {:ok, 1}
      _ -> {:ok, nil}
    end
  end

  defp datastream_id(dataset_id, datastream_name) do
    case {dataset_id, datastream_name} do
      {1, "datastream.1"} -> {:ok, 1}
      _ -> {:ok, nil}
    end
  end

  test "forwards valid data", %{broadway: broadway} do
    ts = DateTime.to_unix(~U[2020-01-01 13:00:00.000000Z])

    valid_data = """
    {
      "ts": #{ts},
      "values": {
        "datastream.1": 14.42
      }
    }
    """

    ref =
      Broadway.test_message(
        broadway,
        valid_data,
        metadata: %{routing_key: "provider1.dataset1"}
      )

    assert_receive {:ack, ^ref, [%{}], []}, 5000
    assert_receive {:publish, received_data}

    assert [%{datastream_id: 1, ts: ts, value: 14.42}] ==
             received_data
  end

  test "rejects messages with unknown routing keys", %{broadway: broadway} do
    ts = DateTime.to_unix(~U[2020-01-01 13:00:00.000000Z])

    valid_data = """
    {
      "ts": #{ts},
      "values": {
        "datastream.1": 14.42
      }
    }
    """

    ref =
      Broadway.test_message(
        broadway,
        valid_data,
        metadata: %{routing_key: "unknown-provider1.unknown-dataset1"}
      )

    assert_receive {:ack, ^ref, [], [%{}]}, 5000
  end

  test "filters values with unknown datastream names", %{broadway: broadway} do
    ts = DateTime.to_unix(~U[2020-01-01 13:00:00.000000Z])

    data = """
    {
      "ts": #{ts},
      "values": {
          "datastream.1": 14.42,
          "datastream.unknown": 2000.0
      }
    }
    """

    ref =
      Broadway.test_message(
        broadway,
        data,
        metadata: %{routing_key: "provider1.dataset1"}
      )

    assert_receive {:ack, ^ref, [%{}], []}, 5000
    assert_receive {:publish, received_data}
    assert [%{datastream_id: 1, ts: ts, value: 14.42}] == received_data
  end

  test "rejects invalid data", %{broadway: broadway} do
    ts = DateTime.to_unix(~U[2020-01-01 13:00:00.000000Z], :millisecond)

    invalid_datas = [
      """
      {
        "ts": -1,
        "values": {
            "datastream.1": 14.42
        }
      }
      """,
      """
      {
        "ts": #{ts},
        "values": {
            "datastream.1": "not a float"
        }
      }
      """,
      """
      straigt-up invalid format
      """
    ]

    for invalid_data <- invalid_datas do
      ref =
        Broadway.test_message(
          broadway,
          invalid_data,
          metadata: %{routing_key: "provider1.dataset1"}
        )

      assert_receive {:ack, ^ref, [], [%{}]}, 5000
    end
  end
end
