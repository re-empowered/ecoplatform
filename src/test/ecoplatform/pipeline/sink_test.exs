defmodule Ecoplatform.Pipeline.SinkTest do
  use Ecoplatform.DataCase, async: false

  alias Ecoplatform.BroadwayEctoSandbox
  alias Ecoplatform.Datasets
  alias Ecoplatform.Datasets.Datapoint
  alias Ecoplatform.Datasets.Datastream

  import Ecoplatform.DatasetsFixtures

  setup context do
    BroadwayEctoSandbox.attach(Repo)

    broadway_name = String.to_atom("#{__MODULE__}.#{context.test}")

    {Ecoplatform.Pipeline.Sink, name: broadway_name, producer_module: Broadway.DummyProducer}
    |> Supervisor.child_spec(id: broadway_name)
    |> start_supervised!()

    %{broadway: broadway_name}
  end

  test "stores valid incoming data in datapoints table", %{broadway: broadway} do
    datastream_id = datastream_fixture().id
    ts = ~U[2020-01-01 13:00:00.000000Z]

    valid_data = """
    [{
      "datastream_id": #{datastream_id},
      "ts": #{DateTime.to_unix(ts, :millisecond)},
      "value": 14.42
    }]
    """

    ref = Broadway.test_message(broadway, valid_data, metadata: %{ecto_sandbox: self()})

    assert_receive {:ack, ^ref, [%{}], []}, 5000

    assert [%Datapoint{datastream_id: ^datastream_id, ts: ^ts, value: 14.42}] =
             Repo.all(Datapoint)
  end

  test "updates the latest_datapoint_ts in datastreams table", %{broadway: broadway} do
    first_datastream_id = datastream_fixture().id
    second_datastream_id = datastream_fixture().id

    first_datastream_latest_ts = ~U[2020-01-01 13:00:00.000000Z]
    second_datastream_latest_ts = ~U[2020-01-01 13:00:30.000000Z]

    valid_data_1 = """
    [{
      "datastream_id": #{first_datastream_id},
      "ts": #{DateTime.to_unix(first_datastream_latest_ts, :millisecond)},
      "value": 14.42
    },
    {
      "datastream_id": #{second_datastream_id},
      "ts": #{DateTime.add(second_datastream_latest_ts, -10) |> DateTime.to_unix(:millisecond)},
      "value": 24.42
    }]
    """

    valid_data_2 = """
    [{
      "datastream_id": #{first_datastream_id},
      "ts": #{DateTime.add(first_datastream_latest_ts, -10) |> DateTime.to_unix(:millisecond)},
      "value": 14.82
    },
    {
      "datastream_id": #{second_datastream_id},
      "ts": #{DateTime.to_unix(second_datastream_latest_ts, :millisecond)},
      "value": 24.82
    }]
    """

    ref =
      Broadway.test_batch(broadway, [valid_data_1, valid_data_2],
        metadata: %{ecto_sandbox: self()}
      )

    assert_receive {:ack, ^ref, [%{}, %{}], []}, 5000

    assert %Datastream{id: ^first_datastream_id, latest_datapoint_ts: ^first_datastream_latest_ts} =
             Datasets.get_datastream!(first_datastream_id)

    assert %Datastream{
             id: ^second_datastream_id,
             latest_datapoint_ts: ^second_datastream_latest_ts
           } = Datasets.get_datastream!(second_datastream_id)
  end

  test "rejects invalid data", %{broadway: broadway} do
    ts = ~U[2020-01-01 13:00:00.000000Z]

    invalid_datas = [
      """
      [{
        "datastream_id": -1,
        "ts": #{DateTime.to_unix(ts, :millisecond)},
        "value": 14.42
      }]
      """,
      """
      [{
        "datastream_id": 1,
        "ts": -1,
        "value": 14.42
      }]
      """,
      """
      [{
        "datastream_id": 1,
        "ts": #{DateTime.to_unix(ts, :millisecond)},
        "value": "not a float"
      }]
      """
    ]

    for invalid_data <- invalid_datas do
      ref = Broadway.test_message(broadway, invalid_data, metadata: %{ecto_sandbox: self()})
      assert_receive {:ack, ^ref, [], [%{}]}, 5000
      assert Repo.all(Datapoint) == []
    end
  end
end
