defmodule Ecoplatform.Pipeline.ValidateTest do
  use Ecoplatform.DataCase, async: true

  alias Ecoplatform.BroadwayEctoSandbox
  alias Ecoplatform.Datasets

  import Ecoplatform.DatasetsFixtures

  setup context do
    BroadwayEctoSandbox.attach(Repo)

    broadway_name = String.to_atom("#{__MODULE__}.#{context.test}")

    test_pid = self()

    {Ecoplatform.Pipeline.Validate,
     name: broadway_name,
     producer_module: Broadway.DummyProducer,
     datastream_range_fn: &datastream_range/1,
     publish_fn: fn payload -> send(test_pid, {:publish, payload}) end}
    |> Supervisor.child_spec(id: broadway_name)
    |> start_supervised!()

    %{broadway: broadway_name}
  end

  defp datastream_range(datastream_id) do
    case datastream_id do
      -1 -> {:ok, nil}
      _ -> {:ok, {0, 20}}
    end
  end

  test "forwards valid data", %{broadway: broadway} do
    datastream = dataset_fixture()
    ts = ~U[2020-01-01 13:00:00.000000Z]

    valid_data = [
      %{
        "datastream_id" => datastream.id,
        "ts" => DateTime.to_unix(ts, :millisecond),
        "value" => 14.42
      }
    ]

    ref = Broadway.test_message(broadway, Jason.encode!(valid_data))

    assert_receive {:ack, ^ref, [%{}], []}, 5000
    assert_receive {:publish, received_data}
    assert valid_data == received_data
  end

  test "filters data outside the minimum and maximum range", %{broadway: broadway} do
    datastream = dataset_fixture()
    ts = ~U[2020-01-01 13:00:00.000000Z]

    valid_data = [
      %{
        "datastream_id" => datastream.id,
        "ts" => DateTime.to_unix(ts, :millisecond),
        "value" => 14.42
      }
    ]

    all_data =
      valid_data ++
        [
          %{
            "datastream_id" => datastream.id,
            "ts" => DateTime.to_unix(ts, :millisecond),
            # -10 is below the minimum value of 0
            "value" => -10
          },
          %{
            "datastream_id" => datastream.id,
            "ts" => DateTime.to_unix(ts, :millisecond),
            # 30 is above the maximum value of 20
            "value" => 30
          }
        ]

    ref =
      Broadway.test_message(broadway, Jason.encode!(all_data), metadata: %{ecto_sandbox: self()})

    assert_receive {:ack, ^ref, [%{}], []}, 5000
    assert_receive {:publish, received_data}
    assert valid_data == received_data
  end

  test "rejects invalid data", %{broadway: broadway} do
    datastream = dataset_fixture()
    ts = DateTime.to_unix(~U[2020-01-01 13:00:00.000000Z], :millisecond)

    invalid_datas = [
      """
      [{
        "datastream_id": -1,
        "ts": #{ts},
        "value": 14.42
      }]
      """,
      """
      [{
        "datastream_id": #{datastream.id},
        "ts": -1,
        "value": 14.42
      }]
      """,
      """
      [{
        "datastream_id": #{datastream.id},
        "ts": #{ts},
        "value": "not a float"
      }]
      """
    ]

    for invalid_data <- invalid_datas do
      ref = Broadway.test_message(broadway, invalid_data)
      assert_receive {:ack, ^ref, [], [%{}]}, 5000
    end
  end

  test "creates value alarms", %{broadway: broadway} do
    datastream = datastream_fixture()
    ts = ~U[2020-01-01 13:00:00.000000Z]

    data = [
      %{
        "datastream_id" => datastream.id,
        "ts" => DateTime.to_unix(ts, :millisecond),
        # -10 is below the minimum value of 0
        "value" => -10
      },
      %{
        "datastream_id" => datastream.id,
        "ts" => DateTime.to_unix(ts, :millisecond),
        # 30 is above the maximum value of 20
        "value" => 30
      }
    ]

    ref = Broadway.test_message(broadway, Jason.encode!(data), metadata: %{ecto_sandbox: self()})

    assert_receive {:ack, ^ref, [%{}], []}, 5000
    assert_receive {:publish, []}
    assert length(Datasets.get_value_alarms(datastream.id, DateTime.utc_now())) == 2
  end
end
