defmodule Ecoplatform.Accounts.UserTest do
  use Ecoplatform.DataCase, async: true

  alias Ecoplatform.Accounts.User

  describe "inspect/2 for the User module" do
    test "does not include password" do
      refute inspect(%User{password: "123456"}) =~ "password: \"123456\""
    end
  end
end
