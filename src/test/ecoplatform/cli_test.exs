defmodule Ecoplatform.CLITest do
  use Ecoplatform.DataCase, async: true

  import ExUnit.CaptureIO
  import Ecoplatform.DatasetsFixtures

  alias Ecoplatform.CLI
  alias Ecoplatform.Accounts

  test "can create API credentials" do
    provider = provider_fixture()

    io =
      capture_io(fn ->
        CLI.create_api_credentials(provider.mqtt_namespace, "my-cli-creds")
      end)

    assert ["SUCCESS", key, secret] = String.split(io, ":")

    key = String.trim(key)
    secret = String.trim(secret)

    assert %Accounts.APICredentials{} = Accounts.get_api_credential_by_key_and_secret(key, secret)
  end

  test "can delete API credentials" do
    provider = provider_fixture()

    {:ok, api_credentials} =
      Accounts.create_api_credentials(provider.id, "to-be-deleted", "prefix")

    io =
      capture_io(fn ->
        assert :ok = CLI.delete_api_credentials(api_credentials.key)
      end)

    assert String.trim(io) == "SUCCESS: Credentials were deleted"
    refute Repo.get(Accounts.APICredentials, api_credentials.id)
  end

  test "can create admin user" do
    io =
      capture_io(fn ->
        assert :ok = CLI.create_admin_user("new-admin@example.com", "New Admin")
      end)

    assert String.trim(io) == "SUCCESS: User created"
    assert %Accounts.User{admin: true} = Accounts.get_user_by_email("new-admin@example.com")
    assert_receive {:email, %Swoosh.Email{subject: "New ecoPlatform user"}}
  end
end
