# System administration

This guide describes a few common tasks for those who are system administrators
of an ecoPlatform instance.

All commands listed on this page must be executed from the root of the
installation directory (e.g.  `/opt/ecoplatform`).

## Stopping, starting and restarting platform services

After installation, the services will be started automatically when the server
is booted. If it is necessary to manually stop and start the services, this can
be done with the following commands:

- Stop all services: `docker compose --profile prod stop`
- Start all services: `docker compose --profile prod start`

The services can also be restarted:

- Restart all services: `docker compose --profile prod restart`

## Checking platform health

As all services are run with Docker, the status and logs of these services can
be inspected from the command line.

- Check status of all services: `docker compose ps`

    All services should list their state as healthy.

- Check logs of a service: `docker compose logs -f --tail 100 <service-name>`

    You can replace `<service-name>` with the name of any service from the
    `docker-compose.yml` file, e.g. `docker compose logs -f --tail 100 nginx`.
    The options `-f` and `--tail 100` make this command behave like
    `tail -f -n 100 <some-file>`.

    This can also be used to check the status of the periodic TLS certificate
    renewal: `docker compose logs -f cert-renew`.

## Database backup and restore

The ecoPlatform database is a PostgreSQL database with the TimescaleDB extension
installed. This means there are several well-known processes and tools that can
be used to backup and restore the ecoPlatform database. Perhaps your
organization has already picked a process and tool which is utilized for other
PostgreSQL databases your organization runs - in that case, you are advised to
apply these to your ecoPlatform instance as well.

However, it should be noted that the TimescaleDB extension places some
constraints on the use of these tools. For example, if you use `pg_dump` to
backup the database, you must take care if backing up individual hypertables.
This is explained in greater detail in the
[TimescaleDB documentation on using pg_dump and pg_restore](https://docs.timescale.com/use-timescale/latest/migration/pg-dump-and-restore/).
Please consider the
[full TimescaleDB documentation on backups](https://docs.timescale.com/self-hosted/latest/backup-and-restore/)
as you decide on a process and tool that fits you use case.

For the sake of illustration, you can create a logical backup of your
ecoPlatform database instance by running the commands below. The first command
opens a shell inside the TimescaleDB container, where the full suite of Postgres
tools are available. The next command actually performs the backup:

```sh
docker compose exec timescale /bin/bash
pg_dump -U postgres -Fc -f ecoplatform.bak ecoplatform
```

Note that the commands must be executed from your installation directory
(typically `/opt/ecoplatform`).

Please see the TimescaleDB documentation for how to restore such a backup.

## Creating admin accounts

Accounts for instance administrators can be created by system administrators by
running the script [scripts/create_admin_account.sh](../scripts/create_admin_account.sh):

```sh
bash ./scripts/create_admin_account.sh "new-admin@example.com" "New Admin"
```

The first argument is the email address of the instance administrator, and the
second argument is their name.

When the account has been created, an email is sent to the specified
email address. This email contains a link for choosing a password.

## Managing API credentials

API credentials are used by data providers and consumers to authenticate and
authorize their requests against the HTTP and MQTT APIs.

API credentials are managed by system administrators with scripts from the
[scripts/](../scripts/) directory. Thus, system administrators must handle
API credential requests from the providers.

When providers requiest API credentials, the request must state the providers
MQTT topic namespace, as well as a name they want to associate with the given
set of API credentials.

To create API credentials, execute the following command:
```sh
bash ./scripts/create_api_credentials.sh "<topic-namespace>" "<credentials-name>"
```

`<topic-namespace>` must be replaced with the MQTT topic namespace of the
provider. `<credentials-name>` can be any string of characters, and is only used
for reference. On success, the command will print the API key and API secret that is
to be used for authentication.

API credentials can be deleted by executing:

```sh
bash ./scripts/delete_api_credentials.sh "<api-key>"
```

Here, `<api-key>` is the key that was printed when generating the API credentials.
