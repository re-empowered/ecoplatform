# ecoPlatform API client examples

This directory contains Python scripts which illustrate how to interact with the
ecoPlatform APIs.

The examples are tested with Python 3.11. You will need to install the
paho-mqtt and requests packages from PyPI. It is recommended that you create a
virtual environment first. To setup your virtual environment and install the
packages, run:

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

The scripts contain a small snippet of configuration at the top of the file - a
block of uppercase variables. These need to be adjusted to match your
environment.  If you are running the ecoPlatform services locally (as described
in [the top-level README](../../README.md)), you can simply uncomment the
variables below the `## Local environment` block.

Once the configuration is in place, you can run the scripts:
- `python importer.py` to import data using the HTTP API.
- `python exporter.py` to export data using the HTTP API.
- `python subscriber.py` to connect to the MQTT broker and subscribe to new
  messages.
- `python publisher.py` to connect to the MQTT broker and publish datapoints as
  MQTT messages.
