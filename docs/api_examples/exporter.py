#! /usr/bin/env python

import requests

## Test environment
BASE_URL = 'https://ecoplatform.example.com'
API_KEY = 'REPLACE-WITH-YOUR-API-KEY'
API_SECRET = 'REPLACE-WITH-YOUR-API-SECRET'
DATASTREAM_IDS='1,2,3'

## Local environment
# BASE_URL = 'http://localhost:4000'
# API_KEY = 'provider1'
# API_SECRET = 'secret'
# DATASTREAM_IDS='1,2,3'

headers = {
    'x-api-key': API_KEY,
    'x-api-secret': API_SECRET
}

params = {
    'start_ts': 0,
    'end_ts': 32503679999999,
    'datastream_ids': DATASTREAM_IDS
}

r = requests.get(BASE_URL + '/api/v1/datasets/1/export', params=params, headers=headers, stream=True)
print('status: ' + str(r.status_code))
if r.status_code != 200:
    print('error: ' + str(r.text))
else:
    print('exported:')
    print(r.text)
