#! /usr/bin/env python

import time
import random
import requests

## Test environment
BASE_URL = 'https://ecoplatform.example.com'
API_KEY = 'REPLACE-WITH-YOUR-API-KEY'
API_SECRET = 'REPLACE-WITH-YOUR-API-SECRET'
DATASTREAM_NAME = 'voltage'

## Local environment
# BASE_URL = 'http://localhost:4000'
# API_KEY = 'provider1'
# API_SECRET = 'secret'
# DATASTREAM_NAME = 'datastream-1'

headers = {
    'x-api-key': API_KEY,
    'x-api-secret': API_SECRET
}

timestamp = int(time.time() * 1000)

datapoints = [
    DATASTREAM_NAME + ',' + str(timestamp + 1) + ',' + str(230 + random.uniform(-5, 5)),
    DATASTREAM_NAME + ',' + str(timestamp + 2) + ',' + str(230 + random.uniform(-5, 5)),
    DATASTREAM_NAME + ',' + str(timestamp + 3) + ',' + str(230 + random.uniform(-5, 5))
]

files = {'import': ('datapoints.csv', '\r\n'.join(datapoints))}

r = requests.post(BASE_URL + '/api/v1/datasets/1/import', files=files, headers=headers)

print('status: ' + str(r.status_code))
if r.status_code != 200:
    print('error: ' + str(r.text))
