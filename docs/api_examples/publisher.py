#! /usr/bin/env python

import time
import json
import random
import paho.mqtt.client as mqtt

## Test environment
BROKER_HOST = 'ecoplatform.example.com'
BROKER_PORT = 8883
BROKER_TLS = True
API_KEY = 'REPLACE-WITH-YOUR-API-KEY'
API_SECRET = 'REPLACE-WITJ-YOUR-API-SECRET'
TOPIC = 'ecodr/emeter/1001'
DATASTREAM_NAME = 'voltage'

## Local environment
# BROKER_HOST = 'localhost'
# BROKER_PORT = 1883
# BROKER_TLS = False
# API_KEY = 'provider1'
# API_SECRET = 'secret'
# TOPIC = 'provider1/dataset1'
# DATASTREAM_NAME = 'datastream-2'

def connect_logger(client, userdata, flags, rc):
    print('Connected with rc {}'.format(rc))

client = mqtt.Client(client_id='ecoplatform-demo-publisher')
client.username_pw_set(API_KEY, API_SECRET)
client.on_connect = connect_logger
if BROKER_TLS:
    client.tls_set()

client.connect(BROKER_HOST, BROKER_PORT, 60)

print('Connecting')
client.loop_start()

print('Publishing')
for i in range(0, 10):
    time.sleep(1)

    timestamp = int(time.time() * 1000)
    voltage = 230 + random.uniform(-5, 5)

    message = json.dumps({
        'ts': timestamp,
        'values': {
            DATASTREAM_NAME: voltage
        }
    })

    # Publish with QoS1 (`qos=1`) and wait to receive QoS acknowledgements from broker
    message_info = client.publish(TOPIC, message, qos=1)
    message_info.wait_for_publish()
