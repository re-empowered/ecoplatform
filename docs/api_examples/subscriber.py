#! /usr/bin/env python

import paho.mqtt.client as mqtt

## Test environment
BROKER_HOST = 'ecoplatform.example.com'
BROKER_PORT = 8883
BROKER_TLS = True
API_KEY = 'REPLACE-WITH-YOUR-API-KEY'
API_SECRET = 'REPLACE-WITJ-YOUR-API-SECRET'

## Local environment
# BROKER_HOST = 'localhost'
# BROKER_PORT = 1883
# BROKER_TLS = False
# API_KEY = 'provider1'
# API_SECRET = 'secret'

def connect_logger(client, userdata, flags, rc):
    print('Connected with rc {}'.format(rc))
    print('Subscribing')
    client.subscribe("#")

def subscribe_logger(client, userdata, mid, granted_qos):
    print('Subscribed')

def message_logger(client, userdata, message):
    print(f'Topic: {message.topic} | Message: {message.payload}')

client = mqtt.Client(client_id='ecoplatform-demo-subscriber')
client.username_pw_set(API_KEY, API_SECRET)
client.on_connect = connect_logger
client.on_subscribe = subscribe_logger
client.on_message = message_logger
if BROKER_TLS:
    client.tls_set()

client.connect(BROKER_HOST, BROKER_PORT, 60)
print('Connecting')
client.loop_forever()
