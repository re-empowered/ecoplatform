# Pipeline

This document describes the data pipeline that underpins ecoPlatforms real-time
data processing. The intended audience is developers who need to understand the
inner workings of the pipeline in order to modify or extend it.

Since the pipeline is built around RabbitMQ, it is expected that the reader is
familiar with RabbitMQ topology such as exchanges, queues and queue bindings.

## Purpose

The purpose of the data pipeline is to process data received from external
tools. These tools submit data to ecoPlatform through either an MQTT or HTTP
API. The main tasks of the data pipeline is to sanitize the incoming data,
filter out invalid data, and store the valid data in a time series database.

## Dataflow

The default dataflow is illustrated in below diagram:

```mermaid
flowchart TD
  mqtt((mqtt)) -- # --o unpack[Unpack]
  unpack -.-> mqtt-dlx((mqtt-dlx))
  unpack -- unpacked --> pipeline((pipeline))
  http(http) --x import[Import]
  import -- unpacked --> pipeline
  pipeline -- unpacked --o validate[Validate]
  validate -- validated --> pipeline
  validate -.-> pipeline-dlx((pipeline-dlx))
  pipeline -- validated --o sink[Sink]
  sink -.-> pipeline-dlx
  sink --x db(Database)
```

In the above diagram:
- A circle is a RabbitMQ exchange.
- A rectangle with sharp corners is a data processing step.
- A rectangle with rounded corners is something else.
- Lines that end in a dot are RabbitMQ queue bindings. The text on the line is
  the routing key of the queue binding.
- Lines that end in an arrow indicate a message flow to a RabbitMQ exchange. The
  text on the line is the routing key of the message.
- Lines that end in an X indicate a different kind of dataflow.

The `Unpack` and `Import` processing steps both serve the same purpose:
translating from an external message format (MQTT and HTTP, respectively) to
a common internal format (called [`unpacked`](#the-unpacked-data-format)). The
messages in `unpacked` format are published to the RabbitMQ pipeline exchange.

Before performing the translation, the processing steps validate that the
incoming data conforms to the corresponding format for the given interface.
For rules regarding these formats, please see the [API documentation](./api.md).

In case the `Unpack` processor receives a message that does not conform to its
format, the message is published to the dead letter exchange `mqtt-dlx` with its
original routing key. See the section on
[dead letter exchanges](#dead-letter-exchanges) for more information.

The next processing step is `Validate`. It receives all `unpacked` messages and
validates that the value of a datapoint is within the minimum and maximum bounds
of its datastream (if the datastream has such bounds defined). If it determines
that a datapoint is out of bounds, it raises a value alarm, and the datapoint is
discarded. Valid datapoints are published to the RabbitMQ pipeline exchange with
the `validated` routing key, still using the `unpacked` format.

Messages which `Validate` cannot process (for example, if they do not conform to
the `unpacked` format), are published to the dead letter exchange `mqtt-dlx`
with their original routing key.

The last processing step is `Sink`. It receives all `validated` messages and
writes the contained datapoints to the time series database.

## Translation of MQTT topics to RabbitMQ routing keys

MQTT uses slashes ("/") for topic segment separators while RabbitMQ routing keys
uses dots. RabbitMQ does automatic translation of MQTT topics to RabbitMQ
routing keys, so that the MQTT topic `ecodr/emeter/123432` becomes the routing
key `ecodr.emeter.123432`.

## Pipeline cache

To reduce the load on the database, the pipeline keeps a cache of all the
information it needs to validate the contents of messages. The cache is used to
look up the following terms:

- which dataset ID corresponds to a given routing key (MQTT topic).
- which datastream ID corresponds to a given a datastream name.
- which minimum and maximum values correspond to a given datastream ID.

The cache is automatically refreshed every 10 minutes. This means it may at
worst be 10 minutes out-of-date, and changes made to the above terms can take
up to 10 minutes to propagate to the pipeline.

## The `unpacked` data format

The common internal format is a serialized as JSON. It consists of an array of
datapoints. Each datapoint is an object that has the following keys:

- `datastream_id`: The ID of the datastream the datapoint belongs to. Integer.
- `ts`: The timestamp of the datapoint. Integer (unix epoch in milliseconds).
- `value`: The value of the datapoint. Double.

The common internal format is validated according to the following JSON schema:

```
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "datastream_id": {"type": "integer", "minimum": 1},
      "ts": {"type": "integer", "minimum": 0, "exclusiveMaximum": 32503680000000},
      "value": {"type": "number"}
    },
    "required": ["ts", "datastream_id", "value"]
  }
}
```

## Configuring the pipeline

The default dataflow is as illustrated in the above figure. But it is possible
to change the following details through configuration:

- The names of the unpack, validate and sink queues
- The queue bindings for the unpack, validate and sink queues
- The publish target (exchange and routing key) that should be used by:
  - the unpack processor when published unpacked messages
  - the value processor when published validated messages
- The names of the dead letter exchanges for the unpack, validate and sink
  processors.

These items are configured by system administrators - see
[Configuration](./installation.md#configuration).

## Extending the pipeline

The pipeline can be extended in several ways.

For example, if you have an extra source of data that you want to run through
the pipeline, you can do this by publishing the data to RabbitMQ. You can then
choose to use the existing pipeline exchange, and publish with the `unpacked` or
`validated` routing key depending on where you want to inject the data into the
pipeline. Alternatively, you can publish it to a separate exchange with a custom
routing key, and add a matching queue binding to the configuration for the
`Validate` or `Sink` processor. The only important detail is that the data must
be formatted according to the [`unpacked`](#the-unpacked-data-format) format.

It is also possible to insert extra processing steps. If you want to insert a
processing step between the `Unpack` and `Validate` step, you can do this by
changing the queue binding for the `Validate` step, e.g. you could bind it to
the `unpacked-and-ready` routing key. Then add your own script that creates a
queue witj a queue binding to the `unpacked` routing key, process the data, and
publish it back to RabbitMQ using the `unpacked-and-ready` routing key.

## Dead letter exchanges

As mentioned above, messages which cannot be processed are published back to
RabbitMQ on a dead letter exchange. A dead letter exchange is just a normal
RabbitMQ exchange, but messages are published to it for the purpose of handling
errors.

You should note that there is no default handling the messages published to dead
letter exchanges. This lies in the hands of those responsible for managing the
instance.

A simple way to handle these messages is to create a queue with a queue binding
to all routing keys on the dead letter exchanges. This will at least store the
messages, so they can be inspected manually. Creating and configuring the queue
and inspecting the messages can be done through RabbitMQ's admin panel.
