# Installation

This guide describes how you can install and run an ecoPlatform instance on a
server. The guide assumes that your server is running Debian Linux 12.

An ecoPlatform instance consists of the following core services:

- `timescale`
- `rabbitmq`
- `engine`

All platform services are run with `docker`, and are defined in
[docker-compose.yml](./../docker-compose.yml). In addition to the core services
listed above, there are also supporting services (`nginx`, `cert-acquire` and
`cert-renew`). The additional services are optional and are included to manage
TLS certificates and handle TLS termination.

The guide assumes that you have experience with Linux system administration, and are familiar with the following (non-exhaustive) list of topics:

- DNS
- Email and SMTP
- Web servers and reverse proxies
- TLS certificates
- Docker and docker-compose

## Prerequisites

Your server will need to fulfill the following preconditions before you can
install and run an ecoPlatform instance:

- Your server should fulfill the [hardware requirements](#hardware-requirements).
- Your server must be [publicly accessible](#publicly-accessible) for HTTP
  and MQTT traffic.
- Your server must have [Docker](https://docs.docker.com/engine/install/debian/)
  and git installed.
- You must have [DNS](#dns) configured for your ecoPlatform instance.
- You must have access to an [SMTP](#smtp) server for ecoPlatform to send emails
  through.

### Hardware requirements

Exact hardware requirements depend on the specific usage scenario. It depends
mainly on the expected number of datapoints per second and the duration for
which data must be stored.

Given 500 expected datapoints per second and a storage duration of 2 years, the
following specifications are recommended:

- CPU: 8 cores
- RAM: 32 GB
- Disk:
  - 50 GB SSD disk space for the operating system
  - 50 GB SSD disk space for the message broker
  - 1 TB SSD disk space for the database

It is recommended that the operating system, message broker and database use
separate disks. If you do this, you will need to override the definitions for
the `timescale` and `rabbitmq` services, in order to specify where the data must
be stored. For information on how to do this, see the documentation on
[overriding service definitions](#overriding-service-definitions).

### Publicly accessible

ecoPlatform exposes MQTT and HTTP APIs and an HTTP web interface to clients.
This means you must make these services accessible to the clients of your
ecoPlatform instance. Typically, this means allocating a public IP to your
server and allowing incoming traffic on the service ports.

The ecoPlatform traffic is by default proxied through an NGINX instance which
handles TLS termination. In this case you must ensure the following ports are
accessible through the firewall on your server:

- HTTP: 443
- MQTT: 8883

If you are using an external proxy to handle TLS termination, you must forward
the traffic to these ports:

- HTTP: 4001
- MQTT: 1883

### DNS

In order to expose the ecoPlatform services, you must create a DNS record that
points to your server. The record can be an A or CNAME record.

For example, if your instance uses `ecoplatform.example.com` as its DNS name,
and runs on a server with an IP address of `192.0.2.10`, you could have a DNS A
record pointing `ecoplatform.example.com` to `192.0.2.10`.

This changes if you use an external proxy. In that situation, you must point
your DNS name to the proxy, and configure the proxy to forward traffic to your
server.

The DNS name you select will be used later when you configure your ecoPlatform
instance.

### SMTP

An SMTP server is required for ecoPlatform to send emails, which are used for
user registration, password resets and alarm notifications.

Configuration options such as SMTP host, port, username and password must be
entered into the .env file, see the [configuration section](#configuration)
below.

## Configuration

The platform services read configuration from a file named .env in the src
directory. This file is not checked in to the repository, so you must first
create it. You can do this by copying the file file .env.example:
`cp src/.env.example src/.env`.

All configuration options are documented in the file. The default options work
for local developer setups, but must be changed when installing on a server.
Some options can be left unchanged, but you must consider changing the value
of at least the following options:

- EP_DOMAIN_NAME
- EP_CERTBOT_EMAIL
- EP_WEB_SECRET_KEY_BASE
- POSTGRES_PASSWORD
- EP_DB_PASSWORD
- EP_SMTP_HOST
- EP_SMTP_USERNAME
- EP_SMTP_PASSWORD
- EP_SMTP_MAIL_FROM_EMAIL
- EP_RABBITMQ_ADMIN_PASSWORD
- EP_RABBITMQ_HOST
- EP_RABBITMQ_PASSWORD

The configuration options specified in the file are used across the different
containers, in configuration file templates, and in scripts. Changing the value
of an option may require restarting containers and/or re-running scripts.

### Configuration file format

The format of configuration options in the file must be `OPTION=value`. Lines
in the file which start with `#` are treated as comments and are not used by the
platform.

Examples of invalid formatted configuration options:

- Do not include spaces around the equals sign: `OPTION = value`
- Do not use quotes around the value: `OPTION="value"`
- Do not write multi-line variables:
    ```
    OPTION=a multi
    line value
    ```

## TLS certificates

It is highly encouraged that you ensure all traffic is encrypted with TLS while
it is in transit.

By default, ecoPlatform will use Certbot to acquire and renew certificates from
Let's Encrypt. NGINX is used for TLS termination. However, it is of course
possible for you to enable TLS in any way you please. But you must then adjust
the installation process accordingly yourself.

## Overriding service definitions

It is possible to override the default definitions by adding a
`docker-compose.override.yml` file to the installation directory. This can be
useful in at least two situations:

- You need to specify non-default volumes for Timescale and/or RabbitMQ.
- You want to disable NGINX (you are handling TLS termination yourself).

Below is an example of the contents of a `docker-compose.override.yml` file that
specifies volumes for the `timescale` and `rabbitmq` services and disables the
`nginx` service. Adjust it to your needs.

```
services:
  nginx:
    entrypoint: /bin/true
    restart: no

volumes:
  timescale-db:
    driver: local
    driver_opts:
      type: 'none'
      o: 'bind'
      device: '/mnt/timescale'
  rabbitmq-db:
    driver: local
    driver_opts:
      type: 'none'
      o: 'bind'
      device: '/mnt/rabbitmq'
```

## Installation steps

The following steps need to be performed in order in order to install the platform:

1. Clone the git repository into an installation directory of your choice.

    ```sh
    git clone https://gitlab.com/re-empowered/ecoplatform.git /opt/ecoplatform
    ```

    Here the installation directory is assumed to be `/opt/ecoplatform`.

1. Change into the installation directory:

    ```sh
    cd /opt/ecoplatform
    ```

1. Then, copy .env.example to .env and modify variables as described in [configuration](#configuration):

    ```sh
    cp src/.env.example src/.env
    ```

1. Acquire an SSL certificate for the domain name of you ecoPlatform instance:

    ```sh
    docker compose up cert-acquire
    ```

    This command uses [certbot](https://certbot.eff.org/) to acquire a
    certificate from [Let's Encrypt](https://letsencrypt.org/). Before running
    this command, you should make sure your domain name points to the correct IP
    and that port 80 is accessible to the Let's Encrypt service.

    This step is optional if you already have a TLS certificate, or if you have
    an external load balancer to handle TLS termination.

1. Build the Docker image for the `engine` service:

    ```sh
    docker compose --profile prod build
    ```

1. Run the `timescale` and `rabbitmq` services:

    ```sh
    docker compose up -d
    ```

1. Wait for services to be healthy
   (`docker compose ps` should list status as `healthy`).

1. Run the bootstrap scripts:

    ```sh
    bash ./scripts/timescale_bootstrap.sh
    bash ./scripts/rabbitmq_bootstrap.sh
    ```

1. Run the `engine` and `nginx` services:

    ```sh
    docker compose --profile prod up -d
    ```

    Note: If you do not wish to use NGINX for TLS termination, you should
    disable this service - see the documentation on
    [overriding service definitions](#overriding-service-definitions).

1. Add a daily cronjob to the server to automatically renew your certificate.

    For example, you can use `crontab -e` to open the cron editor and enter the following:

    ```
    12 4 * * * cd /opt/ecoplatform/ && /bin/bash ./scripts/renew_certificate.sh
    ```

    Replace `/opt/ecoplatform/` with your installation directory.

    This step is optional if you already have a TLS certificate, or if you have
    an external load balancer to handle TLS termination.

At this point all platform services should be running and available for use.
You may wish to continue by creating accounts for instance administrators, see
[Creating admin accounts](./system_administration.md#creating-admin-accounts).

## How to upgrade

1. Change into the installation directory:

    ```sh
    cd /opt/ecoplatform
    ```

1. Pull the latest changes from the repository:

    ```sh
    git pull
    ```

1. Make changes to the configuration if necessary.

    The changelog for the new ecoPlatform version should mention any changes you
    need to make to the configuration.

1. Pull Docker images for the `prod` services:

    ```sh
    docker compose --profile prod pull
    ```

1. Build the Docker image for the `engine` service:

    ```sh
    docker compose --profile prod build
    ```

1. Bring up all `prod` services.

    ```sh
    docker compose --profile prod up -d
    ```

    This will recreate any containers whose image was updated, by either the
    above pull or build command. Other services will continue running
    unaffected.

## How to uninstall

1. Change into the installation directory:

    ```sh
    cd /opt/ecoplatform
    ```

1. Stop and delete the docker compose services and their volumes:

    ```sh
    docker compose --profile prod --profile cert-acquire --profile cert-renew down -v
    ```

    This command will delete all data managed by Docker. If you are using
    non-default volumes for Timescale and/or RabbitMQ, you will need to delete
    this data manually.
