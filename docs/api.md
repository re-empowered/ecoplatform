# API

This document describes the MQTT and HTTP APIs that allow data providers and
consumers to interact with ecoPlatform. The intended audience is software
developers who need to build clients that interact with these APIs. Readers are
expected to be familiar with the MQTT and HTTP protocols.

You can find examples of how to use the APIs with Python in the
[api_examples](./api_examples/) directory.

## API credentials

API credentials are used by data providers and consumers to authenticate and
authorize their requests against the HTTP and MQTT APIs.

In order to create a set of API credentials, a matching provider must first be
created through the web UI. Providers are created by instance administrators.
When the provider is created, it is assigned an MQTT topic namespace. If the
provider publishes data via. MQTT, this MQTT topic namespace must be used as the
first topic element.

To obtain API credentials, the provider must request these from a system
administrator, see
[the corresponding documentation](./system_administration.md#managing-api-credentials).
As part of the request for API credentials, providers must state their MQTT
topic namespace, as well as a name they want to associate with the given set of
API credentials. The MQTT topic namespace must match the value that has been
entered into the system by the instance administrator.

The API credentials consist of two parts: An `API key` and an `API secret`.

The authorization system is quite simple. Anyone with a valid set of API
credentials have read access to all data submitted to the system. However, a
given set of API credentials can only submit data to datasets that belongs to
the same provider that the API credentials belong to.

## MQTT API

ecoPlatform exposes an MQTT API. The use case for the MQTT API is real-time
exchange of messages. Subscribers receive data immediately after it has been
published. This makes it useful for tools which need to coordinate in real-time.

Note: subscribers receive messages exactly as published. This means subscribers
should be able to handle messages in an invalid format, and containing invalid
data.

The MQTT API is compatible with MQTT protocol version 3.1.1, with the exception
that Quality of Service 2 for PUBLISH and SUBSCRIBE packets is not supported.

### Connection and authentication

To connect to the MQTT API, you must know the hostname (or IP) and port number
of the ecoPlatform instance. Ask you system administrator to provide this
information.

When connecting to the MQTT API, you must provide a username and password.
The username is your `API key`, and the password is your `API secret`.

### Message topic

The MQTT topic of a message identifies which dataset the data in the message
payload belongs to.

All MQTT topics consist of two parts: `<Provider namespace>/<Dataset subtopic>`.

The provider namespace is always a single topic segment. The dataset subtopic
may be multiple segments. A few examples:

- `ecomonitor/salination`:
  - Namespace: `ecomonitor`
  - Subtopic: `salination`
- `ecodr/emeter/FN1G739D`
  - Namespace: `ecodr`
  - Subtopic: `emeter/FN1G739D`

### Message payload

Every message contains a single timestamp and one or more datapoint values. The
format of the message payload is a JSON object. It must have the following keys:

- `ts`: The timestamp of the datapoint. Integer (unix epoch in milliseconds).
- `values`: A JSON object mapping datastream names to values. The datastream name
  must be the name of a datastream belonging to the dataset identified by the
  topic of the message. The value must be a float.

Example message payload:

```
{
  "ts": 1694691139351,
  "values": {
    "voltage": 230.1,
    "ampere": 0.23,
    "frequency": 49.989
  }
}
```

The payload must conform to the following JSON schema:

```json
{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "type": "object",
  "properties": {
    "ts": {"type": "integer", "minimum": 0, "exclusiveMaximum": 32503680000000},
    "values": {
      "type": "object",
      "additionalProperties": {"type": "number"}
    }
  },
  "required": ["ts", "values"]
}
```

## HTTP API

ecoPlatform exposes an HTTP API. The use case for the HTTP API is batch import
and export of datapoints. This makes it useful for tools which periodically
generate data, or which periodically need to pull data to perform calculations.

When a client imports data using the HTTP API, the data is submitted in chunks to the
pipeline. When all the data has been forwarded to the pipeline, the client receives
a `200 OK` response. This only means that data has been submitted to the pipeline -
it may not immediately available for export, as it must first be validated and
inserted into the database.

When exporting datapoints from the HTTP API, clients receive data from the
database. This means clients only receive data that has been validated.

Note: subscribers on the MQTT API do not receive messages for data imported via.
the HTTP API.

### Connection and authentication

To connect to the HTTP API, you must know the domain name of the ecoPlatform
instance. Ask you system administrator to provide this information.

The base URL for requests is: `https://<domain-name>/api/v1`.

Authentication is is done by adding two HTTP headers to every request:
- `X-API-KEY`: This is your `API key`
- `X-API-SECRET`: This is your `API secret`

### Data format

Whether you are importing or exporting data, the data format is the same:
RFC4180 compatible CSV. Rows are delimited by `\r\n` (`<CR><LF>`). Columns are
delimited by commas. Each row holds a single datapoint, and has the following
columns:

1. The name of the datastream.
2. The timestamp of the datapoint. Integer (unix epoch in milliseconds).
3. The value of the datapoint. Float.

Example:

```
voltage,1694506639820,231.71202374934785
voltage,1694506639821,231.61112087845828
voltage,1694679490949,229.93035678165728
ampere,1694506639820,0.23
ampere,1694506639821,0.42
ampere,1694679490949,0.39
frequency,1694506639820,50.01
frequency,1694506639821,50.02
frequency,1694679490949,50.0
```

### Import

To import datapoints for a dataset, you must send a POST request to
`<base-url>/datasets/<dataset-id>/import`. The request body must be multipart-encoded,
and it must have a field named `import`. The contents of this field must be
datapoints in CSV format as described above.

Example using cURL:

```sh
curl -X POST \
  -H 'X-API-KEY: P001-LFEVpj0NiWo0VMBj1RT65LhptMz' \
  -H 'X-API-SECRET: UxW1lOBvcU4jIh785fMZ8lc3JiNpouKP' \
  -F import=@testfile.csv \
  https://ecoplatform.example.com/api/v1/datasets/1/import
```

It is worth nothing that the datastream names in the file must match datastreams
that belong to the dataset identified by `<dataset-id>` in the URL.

If the imported file contains a row with invalid data, every preceding row will
be imported, while the invalid row and all of the following rows will be discarded.
The client will receive a `422 Unprocessable Content` response, with a JSON object
that describes the offending row.

Files uploaded through the API must have a maximum size of 20 MB.

### Export

To export datapoints from a dataset, you must send a GET request to
`<base-url>/datasets/<dataset-id>/export`. The request must contain
the following two query string parameters:

- `start_ts`: The earliest timestamp to include datapoints for. Inclusive.
   Integer (unix epoch in milliseconds).
- `end_ts`: The latest timestamp to include datapoints for. Inclusive.
   Integer (unix epoch in milliseconds).

The request may also contain the following parameter:

- `datastream_ids`: A list of datastream IDs to include in the export,
  in the format of a comma-separated values, e.g. `1,2,3`.
  If the parameter is not present, all datastreams are exported.

Example using cURL:

```sh
curl --get \
  -H 'X-API-KEY: P001-LFEVpj0NiWo0VMBj1RT65LhptMz' \
  -H 'X-API-SECRET: UxW1lOBvcU4jIh785fMZ8lc3JiNpouKP' \
  -d 'start_ts=1694506639820' \
  -d 'end_ts=1694679490949' \
  -d 'datastream_ids=1,2,3' \
  https://ecoplatform.example.com/api/v1/datasets/1/export
```
