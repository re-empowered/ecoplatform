# Introduction

ecoPlatform is built to exchange, process and store time series data. It
provides MQTT and HTTP APIs for data exchange, and a web UI for data
administration.

The data processing is built on top of RabbitMQ, and is designed to be very
flexible. This allows custom scripts and applications to hook into the pipeline
and perform custom processing. RabbitMQ also hosts the MQTT API.

All persistent data is stored in a PostgreSQL database with the TimescaleDB
plugin. This provides a solid foundation for data storage, and equips
administrators with a well-known interface for managing the data.

These services are tied together by a custom service called Engine. This is a
custom application built for ecoPlatform, and its source code is part of this
repository. Engine both runs the default processing steps, serves up a web UI
for management, and hosts the HTTP API.

ecoPlatform ships with a scripts and documentation for setting up an instance
with a simple deployment configuration on a single Linux server.

This document is meant to give you an overview of the central concepts at play in ecoPlatform.

For more in-depth knowledge, please see the following documents:
- [API](./api.md)
- [Pipeline](./pipeline.md)
- [Installation](./installation.md)
- [System Administration](./system_administration.md)

## Time series

ecoPlatform is all about time series data. A time series is a series of
time-value pairs. For example, you might record a series of outdoor temperature
measurements near your house. Every hour you write down the temperature at the
given point in time: At 08:00 it's 20.2 °C, at 09:00 it's 20.8 °C, and so forth.

In ecoPlatform terms, we call the time series a `datastream`. And time-value
pairs are called `datapoints`. And datastreams which belong together are grouped
in a `dataset`.

This is the fundamental hierarchy that is used to provide context and metadata
to the time series data that is stored in ecoPlatform.

It is worth noting that datapoint values must all be floating point numbers.
Datapoint timestamps can be specified to a precision of one millisecond.

## Providers

In order to submit data into ecoPlatform, it must be sent to through
ecoPlatforms MQTT or HTTP APIs. This is the job of a `provider`. Every dataset
belongs to a provider, and it is only the provider who is authorized to submit
data for their datasets. Whoever the provider actually is - it may represent an
organization, team or just a single person - the provider is expected to build
and operate the tools that interface with the ecoPlatform APIs.

While a provider can only submit data for their own datasets, they have
read-only access to all datasets which belongs to other providers.

## Dataset owners

Prior to submitting time series data to ecoPlatform, the corresponding datasets
and datastreams must first be created. This is managed through the web UI. All
users of the web UI can create datasets, and they automatically become owners of
those datasets. As an owner of a dataset, they can then manage the datastreams
of the dataset, and they can add other users to the list of owners for their
datasets.

Datasets are only visible to those users who are owners of the dataset.

## Instance administrators

Instance administrators are users of the web UI with special privileges - they
are responsible for adding and removing users and providers. They also have
access to all datasets in the given instance as if they were owners of the
datasets.

## System administrators

On the fringes of ecoPlatform reside the mythical system administrators. They
are responsible for installing an instance of the platform and managing the
server it runs on. These special creatures are tasked with creating the initial
accounts of instance administrators and also issue API credentials to providers.

## APIs

The MQTT and HTTP APIs provide different interfaces for different purposes.

The MQTT API allows providers to publish and subscribe to data. This API is to
be used by providers in scenarios where subscribers must be immediately notified
when new data is published by the provider.

The HTTP API allows providers to import and export data in a CSV format. This
API is to be used by providers in scenarios where batch import and export of
data is expedient.

The details of the API is described in the [API](./api.md) document.

Both APIs provide entrypoints to the data pipeline.

## Data pipeline

RabbitMQ is the backbone of the ecoPlatform data pipeline. It acts as a message
broker, and hosts exchanges and queues for routing messages between different
data processors.

The data processors validate the received data. Any data that is considered
invalid is discarded. Valid datapoints are sinked into the database.

It is possible to hook into the pipeline at various stages, to perform custom
data processing, inject additional data, or filter some data out. This is
described in the [Pipeline](./pipeline.md) document.

## Alarms

Dataset owners are able to specify a minimum and maximum value bound for the
datapoints in a datastream. If the data processors receive datapoints from a
datastream which is outside of these bounds, the data is considered invalid and
is discarded. This will generate a value alarm.

Dataset owners may also specify a maximum silence period for a datastream. If
the data processors have not received any valid datapoints from a datastream for
a period longer than the maximum silence period, a silence alarm is generated.

Dataset owners can subscribe to alarms from individual datasets. If subscribed,
the owner will receive an email notification whenever an alarm is generated. The
system only sends 1 email notification per hour per data owner, to avoid spam.
