#! /bin/sh

mkdir -p /etc/nginx/ssl/$EP_DOMAIN_NAME
cp /etc/letsencrypt/live/$EP_DOMAIN_NAME/* /etc/nginx/ssl/$EP_DOMAIN_NAME/
chown -R 101 /etc/nginx/ssl/$EP_DOMAIN_NAME
